package fr.soleil.comete.swt;

import java.util.ArrayList;
import java.util.EventObject;
import java.util.List;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;

import fr.soleil.comete.definition.data.target.scalar.INumberComponent;
import fr.soleil.comete.definition.listener.INumberListener;
import fr.soleil.comete.swt.util.SWTUtils;

public abstract class AbstractNumberRangeControl<T extends Control> extends CometeComposite<T> implements INumberComponent {

    List<INumberListener> numberListeners = new ArrayList<INumberListener>();

    public AbstractNumberRangeControl(Composite parent, int childStyle) {
        super(parent, childStyle);
    }

    protected abstract Number getControlValue();

    protected abstract void setControlValue(Number value);

    protected abstract Number getControlMinimum();

    protected abstract void setControlMinimum(Number min);

    protected abstract Number getControlMaximum();

    protected abstract void setControlMaximum(Number max);

    public void addNumberListener(INumberListener listener) {
        if (!numberListeners.contains(listener)) {
            numberListeners.add(listener);
        }
    }

    public void removeNumberListener(INumberListener listener) {
        if (numberListeners.contains(listener)) {
            numberListeners.remove(listener);
        }
    }

    public void fireValueChanged(EventObject event) {
        for (INumberListener listener : numberListeners) {
            listener.valueChanged(event);
        }
    }

    /*
     *  Number *****************
     */

    @Override
    public Number getNumberValue() {
        return getControlValue();
    }

    @Override
    public void setNumberValue(final Number value) {
        if (getControl() != null) {
            SWTUtils.asyncExec(new Runnable() {
                @Override
                public void run() {
                    if (!getControl().isDisposed()) {
                        setControlValue(value);
                        EventObject event = new EventObject(this);
                        fireValueChanged(event);
                    }
                }
            });
        }
    }


    public void setMinimumValue(final Number min) {
        if (getControl() != null) {
            SWTUtils.asyncExec(new Runnable() {
                @Override
                public void run() {
                    if (!getControl().isDisposed()) {
                        setControlMinimum(min);
                        EventObject event = new EventObject(this);
                        fireValueChanged(event);
                    }
                }
            });
        }
    }

    public void setMaximumValue(final Number max) {
        if (getControl() != null) {
            SWTUtils.asyncExec(new Runnable() {
                @Override
                public void run() {
                    if (!getControl().isDisposed()) {
                        setControlMaximum(max);
                        EventObject event = new EventObject(this);
                        fireValueChanged(event);
                    }
                }
            });
        }
    }

    /*
     *  Integer *****************
     */

    public int getValue() {
        int value = 0;
        Number numberValue = getControlValue();
        if (numberValue != null) {
            value = numberValue.intValue();
        }
        return value;
    }

    public void setValue(int value) {
        setNumberValue(value);
    }

    public void setMinimum(int min) {
        setMinimumValue(min);

    }

    public void setMaximum(int max) {
        setMaximumValue(max);
    }

    public int getMinimumInteger() {
        int min = 0;
        Number numberValue = getControlMinimum();
        if (numberValue != null) {
            min = numberValue.intValue();
        }
        return min;
    }

    public int getMaximumInteger() {
        int max = 0;
        Number numberValue = getControlMaximum();
        if (numberValue != null) {
            max = numberValue.intValue();
        }
        return max;
    }

    /*
     *  Double *****************
     */

    public double getDoubleValue() {
        double value = 0;
        Number numberValue = getControlValue();
        if (numberValue != null) {
            value = numberValue.doubleValue();
        }
        return value;
    }

    public void setDoubleValue(double value) {
        setNumberValue(value);
    }

    public void setMinimumDouble(double min) {
        setMinimumValue(min);
    }

    public void setMaximumDouble(double max) {
        setMaximumValue(max);
    }

    public double getMinimumDouble() {
        double min = 0;
        Number numberValue = getControlMinimum();
        if (numberValue != null) {
            min = numberValue.doubleValue();
        }
        return min;
    }

    public double getMaximumDouble() {
        double max = 0;
        Number numberValue = getControlMaximum();
        if (numberValue != null) {
            max = numberValue.doubleValue();
        }
        return max;
    }

    @Override
    public void setHorizontalAlignment(int halign) {
        // TODO Auto-generated method stub
    }

    @Override
    public int getHorizontalAlignment() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public String getFormat() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setFormat(String format) {
        // TODO Auto-generated method stub
    }

}
