package fr.soleil.comete.swt;

import java.util.ArrayList;
import java.util.EventObject;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

import fr.soleil.comete.definition.listener.IButtonListener;
import fr.soleil.comete.definition.widget.IButton;
import fr.soleil.comete.definition.widget.util.CometeImage;
import fr.soleil.comete.swt.util.ImageTool;
import fr.soleil.comete.swt.util.SWTUtils;
import fr.soleil.comete.swt.util.SwtImage;

public class Button extends CometeComposite<org.eclipse.swt.widgets.Button> implements IButton, MouseListener {

    String actionName = null;
    List<IButtonListener> buttonListener = new ArrayList<IButtonListener>();

    public Button(Composite parent, int childStyle) {
        super(parent, childStyle);
    }

    @Override
    protected org.eclipse.swt.widgets.Button initControl(int childStyle) {
        org.eclipse.swt.widgets.Button control = new org.eclipse.swt.widgets.Button(this, childStyle);
        control.addMouseListener(this);
        return control;
    }

    @Override
    public boolean isSelected() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void setSelected(boolean bool) {
        // TODO Auto-generated method stub

    }

    @Override
    public void setHorizontalAlignment(int halign) {
        // TODO Auto-generated method stub

    }

    @Override
    public int getHorizontalAlignment() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public String getText() {
        String result = null;
        if (getControl() != null) {
            final String[] threadResult = { null };
            SWTUtils.syncExec(new Runnable() {
                @Override
                public void run() {
                    if (!getControl().isDisposed()) {
                        threadResult[0] = getControl().getText();
                    }
                }
            });
            result = threadResult[0];
        }
        return result;
    }

    @Override
    public void setText(String text) {
        if (getControl() != null) {
            final String theText = (text == null ? "" : text);
            SWTUtils.asyncExec(new Runnable() {
                @Override
                public void run() {
                    if (!getControl().isDisposed()) {
                        getControl().setText(theText);
                    }
                }
            });
        }
    }

    @Override
    public void setEditable(boolean b) {
        // Nothing to do
    }

    @Override
    public boolean isEditable() {
        return false;
    }

    @Override
    public void setCometeImage(CometeImage image) {
        if (getControl() != null) {
            final SwtImage swtImage = ImageTool.getImage(image);
            if ((swtImage != null) && (swtImage.getImage() != null)) {
                SWTUtils.asyncExec(new Runnable() {
                    @Override
                    public void run() {
                        if (!getControl().isDisposed()) {
                            getControl().setImage(swtImage.getImage());
                        }
                    }
                });
            }
        }

    }

    @Override
    public CometeImage getCometeImage() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setBorderPainted(boolean painted) {
        // TODO Auto-generated method stub

    }

    @Override
    public boolean isBorderPainted() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void setButtonLook(boolean look) {
        // TODO Auto-generated method stub

    }

    @Override
    public boolean isButtonLook() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void addButtonListener(IButtonListener listener) {
        if (!buttonListener.contains(listener)) {
            buttonListener.add(listener);
        }
    }

    @Override
    public void removeButtonListener(IButtonListener listener) {
        if (buttonListener.contains(listener)) {
            buttonListener.remove(listener);
        }
    }

    @Override
    public void fireActionPerformed(EventObject event) {
        for (IButtonListener listener : buttonListener) {
            listener.actionPerformed(event);
        }
    }

    @Override
    public void execute() {
        // TODO Auto-generated method stub

    }

    @Override
    public String getActionName() {
        String tmpActionName = getText();
        if (actionName != null) {
            tmpActionName = actionName;
        }
        return tmpActionName;
    }

    @Override
    public void setActionName(String actionName) {
        this.actionName = actionName;
    }

    @Override
    public void mouseDoubleClick(MouseEvent arg0) {
        fireActionPerformed(new EventObject(this));
    }

    @Override
    public void mouseDown(MouseEvent arg0) {
        fireActionPerformed(new EventObject(this));
    }

    @Override
    public void mouseUp(MouseEvent arg0) {
        // TODO Auto-generated method stub

    }

    public static void main(String[] args) {
        final Display display = new Display();
        final Shell shell = new Shell(display);

        shell.setBounds(0, 0, 400, 200);
        shell.setLayout(new FillLayout());
        shell.setText("Button");

        Button button = new Button(shell, SWT.NONE);
        button.setText("Button SWT Test");
        button.setToolTipText("Tooltype test");

        button.addButtonListener(new IButtonListener() {

            @Override
            public void actionPerformed(EventObject event) {
                //System.out.println("Action performed");
            }
        });

        // shell.pack();
        shell.open();

        while (!shell.isDisposed()) {
            if (!display.readAndDispatch()) {
                display.sleep();
            }
        }

        display.dispose();
        System.exit(0);
    }



}
