package fr.soleil.comete.swt.viewers;

import java.awt.Frame;
import java.awt.event.MouseWheelEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JPanel;

import org.eclipse.swt.SWT;
import org.eclipse.swt.awt.SWT_AWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseWheelListener;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.jfree.chart.ChartMouseEvent;
import org.jfree.chart.ChartMouseListener;
import org.jfree.chart.JFreeChart;
import org.jfree.data.general.Dataset;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import fr.soleil.comete.definition.listener.IChartViewerListener;
import fr.soleil.comete.definition.listener.IDataTransferListener;
import fr.soleil.comete.definition.listener.ISnapshotListener;
import fr.soleil.comete.definition.widget.IChartViewer;
import fr.soleil.comete.definition.widget.properties.AxisProperties;
import fr.soleil.comete.definition.widget.properties.BarProperties;
import fr.soleil.comete.definition.widget.properties.ChartProperties;
import fr.soleil.comete.definition.widget.properties.CurveProperties;
import fr.soleil.comete.definition.widget.properties.ErrorProperties;
import fr.soleil.comete.definition.widget.properties.InterpolationProperties;
import fr.soleil.comete.definition.widget.properties.MarkerProperties;
import fr.soleil.comete.definition.widget.properties.MathProperties;
import fr.soleil.comete.definition.widget.properties.PlotProperties;
import fr.soleil.comete.definition.widget.properties.SamplingProperties;
import fr.soleil.comete.definition.widget.properties.SmoothingProperties;
import fr.soleil.comete.definition.widget.properties.TransformationProperties;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.comete.definition.widget.util.CometeFont;
import fr.soleil.comete.swt.CometeComposite;
import fr.soleil.comete.swt.viewers.utils.IPlot;
import fr.soleil.comete.swt.viewers.utils.PlotFactory;

public class Chart extends CometeComposite<Control> implements IChartViewer {

    private final List<IChartViewerListener> listeners = new ArrayList<IChartViewerListener>();;

    private Map<String, Object> data;

    private IPlot plot;
    private Frame frame;
    private MouseWheelListener mouseWheelListener;
    private KeyListener keyListener;
    private ChartMouseListener chartMouseListener;

    public Chart(Composite parent, int style) {
        super(parent, SWT.EMBEDDED | style);

        frame = SWT_AWT.new_Frame(this);
    }


    protected void embedPlot(IPlot plot) {
        if (plot instanceof JPanel) {
            if (frame != null) {
                frame.add((JPanel) plot);
            }
        }
        else {
            throw new IllegalArgumentException("must be a chart plot panel");
        }
    }

    public void setPlot(final IPlot plot) {
        final IPlot oldPlot = this.plot;
        if (oldPlot != null) {
            oldPlot.cleanUp();
            frame.remove((JPanel) oldPlot);
        }
        this.plot = plot;
        final Composite composite = this;
        Display display = Display.getCurrent();
        if (display == null) {
            display = Display.getDefault();
        }
        display.asyncExec(new Runnable() {

            @Override
            public void run() {
                embedPlot(plot);
                removeListeners(oldPlot);
                addListeners();
                composite.pack();
                composite.getParent().layout(true, true);
            }
        });
    }

    private void removeListeners(IPlot plot) {
        if (plot != null) {
            removeMouseWheelListener(mouseWheelListener);
            removeKeyListener(keyListener);
            plot.removeChartMouseListener(chartMouseListener);
        }
    }

    private void addListeners() {
        if (plot != null) {
            mouseWheelListener = new MouseWheelListener() {

                @Override
                public void mouseScrolled(MouseEvent event) {
                    JPanel panel = null;
                    if (plot instanceof JPanel) {
                        panel = (JPanel) plot;
                    }
                    MouseWheelEvent awtEvent = org.gumtree.vis.listener.SWT_AWT.toMouseWheelEvent(event, panel);
                    plot.processMouseWheelEvent(awtEvent);
                }
            };
            addMouseWheelListener(mouseWheelListener);

            keyListener = new KeyListener() {

                boolean keyPressed = false;

                @Override
                public void keyReleased(KeyEvent event) {
                    switch (event.keyCode) {
                        case SWT.DEL:
                            plot.removeSelectedMask();
                            break;
                        default:
                            break;
                    }
                    switch (event.character) {
                        default:
                            break;
                    }
                    keyPressed = false;
                }

                @Override
                public void keyPressed(KeyEvent event) {
                    switch (event.keyCode) {
                        case SWT.ARROW_UP:
                            plot.moveSelectedMask(event.keyCode);
                            break;
                        case SWT.ARROW_LEFT:
                            plot.moveSelectedMask(event.keyCode);
                            break;
                        case SWT.ARROW_RIGHT:
                            plot.moveSelectedMask(event.keyCode);
                            break;
                        case SWT.ARROW_DOWN:
                            plot.moveSelectedMask(event.keyCode);
                            break;
                        default:
                            break;
                    }
                    switch (event.stateMask) {
                        case SWT.CTRL:
                            if ((event.keyCode == 'c') || (event.keyCode == 'C')) {
                                if (!keyPressed) {
                                    plot.doCopy();
                                }
                            }
                            else if ((event.keyCode == 'z') || (event.keyCode == 'Z') || (event.keyCode == 'r')
                                    || (event.keyCode == 'R')) {
                                if (!keyPressed) {
                                    plot.restoreAutoBounds();
                                }
                            }
                            else if ((event.keyCode == 'p') || (event.keyCode == 'P')) {
                                System.out.println("p pressed");
                                if (!keyPressed) {
                                    Thread newThread = new Thread(new Runnable() {

                                        @Override
                                        public void run() {
                                            plot.createChartPrintJob();
                                        }
                                    });
                                    newThread.start();
                                }
                            }
                            else if ((event.keyCode == 'e') || (event.keyCode == 'E')) {
                                System.out.println("s pressed");
                                if (!keyPressed) {
                                    Thread newThread = new Thread(new Runnable() {

                                        @Override
                                        public void run() {
                                            try {
                                                plot.doSaveAs();
                                            }
                                            catch (IOException e) {
                                                handleException(e);
                                            }
                                        }
                                    });
                                    newThread.start();
                                }
                            }
                            keyPressed = true;
                            break;
                        default:
                            break;
                    }
                }
            };
            addKeyListener(keyListener);

            final Composite composite = this;

            chartMouseListener = new ChartMouseListener() {

                @Override
                public void chartMouseMoved(ChartMouseEvent event) {

                }

                @Override
                public void chartMouseClicked(ChartMouseEvent event) {
                    Display.getDefault().asyncExec(new Runnable() {

                        @Override
                        public void run() {
                            if (!composite.isFocusControl()) {
                                composite.setFocus();
                            }
                        }
                    });
                }
            };
            plot.addChartMouseListener(chartMouseListener);
        }
    }

    public JFreeChart getChart() {
        if (plot != null) {
            return plot.getChart();
        }
        else {
            return null;
        }
    }

    @Override
    public void setVisible(boolean visible) {
        if (plot != null) {
            plot.setVisible(visible);
        }
        super.setVisible(visible);
    }

    public void setDataset(Dataset dataset) {
        try {
            boolean createNewPlot = true;

            if (!createNewPlot) {
                plot.setDataset(dataset);
            }
            else {
                if (plot != null) {
                    JPanel panel = (JPanel) plot;
                    frame.remove(panel);
                }
                IPlot newPlot = null;
                if (dataset instanceof XYDataset) {
                    newPlot = PlotFactory.createPlot1DPanel((XYDataset) dataset);
                }
                setPlot(newPlot);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Dataset getDataset() {
        return getPlot().getDataset();
    }

    public IPlot getPlot() {
        return plot;
    }

    @Override
    public void redraw() {
        plot.repaint();
        super.redraw();
    }

    public void handleException(final Exception e) {
        Display display = Display.getCurrent();
        if (display == null) {
            display = Display.getDefault();
        }
        display.asyncExec(new Runnable() {
            @Override
            public void run() {
                if (getShell() != null) {
                    MessageBox messageBox = new MessageBox(getShell(), SWT.ICON_WARNING | SWT.OK);
                    messageBox.setText("Failed to Save");
                    messageBox.setMessage("failed : " + e.getMessage());
                    messageBox.open();
                    // MessageDialog.openError(getShell(), "Failed to Save", "failed to save " +
                    // "the image: " + e.getMessage());

                }
            }
        });
    }

    @Override
    public void dispose() {
        if (!isDisposed()) {
            if (mouseWheelListener != null) {
                removeMouseWheelListener(mouseWheelListener);
                mouseWheelListener = null;
            }
            if (keyListener != null) {
                removeKeyListener(keyListener);
                keyListener = null;
            }
            if (plot != null) {
                plot.cleanUp();
                if (chartMouseListener != null) {
                    plot.removeChartMouseListener(chartMouseListener);
                    chartMouseListener = null;
                }
                frame.remove((JPanel) plot);
                frame.dispose();
                plot = null;
            }
        }
        frame = null;
        super.dispose();
    }

    @Override
    public void addChartViewerListener(IChartViewerListener listener) {
        if (!listeners.contains(listener)) {
            listeners.add(listener);
        }
    }

    @Override
    public void removeChartViewerListener(IChartViewerListener listener) {
        if (listeners.contains(listener)) {
            synchronized (listeners) {
                listeners.remove(listener);
            }
        }
    }

    @Override
    public void setAxisName(String name, int axis) {
        // TODO
        // if (plot != null) {
        // switch (axis) {
        // case IChartViewer.X:
        // ((XYPlot) plot).getRangeAxis().setLabel(name);
        // break;
        // default:
        // ((XYPlot) plot).getDomainAxis().setLabel(name);
        // break;
        // }
        // }
    }

    @Override
    public void setDataViewAxis(String id, int axis) {
        // TODO

    }

    @Override
    public void setHeader(String s) {
        // TODO

    }

    @Override
    public Map<String, Object> getData() {
        return data;
    }

    @Override
    public void setData(Map<String, Object> data) {
        this.data = data;
        XYDataset dataset = convertToDataset(data);
        setDataset(dataset);
    }

    @Override
    public void addData(Map<String, Object> dataToAdd) {
        if (data == null) {
            data = new LinkedHashMap<String, Object>();
        }
        if (dataToAdd != null) {
            data.putAll(dataToAdd);
        }
        setData(data);
    }

    @Override
    public void removeData(Collection<String> idsToRemove) {
        if (data != null) {
            for (String id : idsToRemove) {
                data.remove(id);
            }
        }
        setData(data);
    }

    private XYDataset convertToDataset(Map<String, Object> data) {

        XYSeriesCollection dataset = null;

        if ((data != null) && (data.size() > 0)) {
            dataset = new XYSeriesCollection();

            for (Map.Entry<String, Object> entry : data.entrySet()) {
                String key = entry.getKey();
                Object value = entry.getValue();

                if (value != null) {
                    double[] xValue = null;
                    double[] yValue = null;

                    if (value instanceof double[]) {
                        double[] doubleValues = (double[]) value;
                        if ((doubleValues.length > 0) && (doubleValues.length % 2 == 0)) {
                            int halfLenght = doubleValues.length / 2;
                            xValue = new double[halfLenght];
                            yValue = new double[halfLenght];
                            for (int i = 0; i < halfLenght; i++) {
                                xValue[i] = doubleValues[2 * i];
                                yValue[i] = doubleValues[2 * i + 1];
                            }
                        }
                    }
                    else if (value instanceof double[][]) {
                        double[][] doubleValues = (double[][]) value;
                        if ((doubleValues.length == 2) && (doubleValues[0] != null) && (doubleValues[1] != null)
                                && (doubleValues[0].length == doubleValues[1].length)) {
                            xValue = doubleValues[0];
                            yValue = doubleValues[1];
                        }
                    }

                    if ((xValue != null) && (yValue != null)) {
                        final XYSeries series = new XYSeries(key);
                        // System.out.println("xValue = " + Arrays.toString(xValue));
                        // System.out.println("yValue = " + Arrays.toString(yValue));
                        for (int i = 0; i < xValue.length; i++) {
                            series.add(xValue[i], yValue[i]);
                        }
                        dataset.addSeries(series);
                    }
                }
            }
        }

        return dataset;
    }

    @Override
    public void setHorizontalAlignment(int halign) {
        // TODO Auto-generated method stub

    }

    @Override
    public int getHorizontalAlignment() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public void setChartCometeBackground(CometeColor c) {
        // TODO Auto-generated method stub

    }

    @Override
    public CometeColor getChartCometeBackground() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void fireDataChanged(Map<String, Object> data) {
        // TODO Auto-generated method stub

    }

    @Override
    public void fireConfigurationChanged(String id) {
        // TODO Auto-generated method stub

    }

    @Override
    public void setEditable(boolean editable) {
        // TODO Auto-generated method stub

    }

    @Override
    public boolean isEditable() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public int getAxisTitleAlignment(int axis) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public void setAxisTitleAlignment(int align, int axis) {
        // TODO Auto-generated method stub

    }

    @Override
    public void setAxisCometeColor(CometeColor color, int axis) {
        // TODO Auto-generated method stub

    }

    @Override
    public CometeColor getAxisCometeColor(int axis) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setGridStyle(int style, int axis) {
        // TODO Auto-generated method stub

    }

    @Override
    public int getGridStyle(int axis) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public void setAxisDateFormat(String format, int axis) {
        // TODO Auto-generated method stub

    }

    @Override
    public String getAxisDateFormat(int axis) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setSubGridVisible(boolean visible, int axis) {
        // TODO Auto-generated method stub

    }

    @Override
    public boolean isSubGridVisible(int axis) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void setGridVisible(boolean visible, int axis) {
        // TODO Auto-generated method stub

    }

    @Override
    public boolean isGridVisible(int axis) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void setXAxisOnBottom(boolean b) {
        // TODO Auto-generated method stub

    }

    @Override
    public boolean isManagementPanelVisible() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void setManagementPanelVisible(boolean visible) {
        // TODO Auto-generated method stub

    }

    @Override
    public boolean isMathExpressionEnabled() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void setMathExpressionEnabled(boolean enable) {
        // TODO Auto-generated method stub

    }

    @Override
    public void setDisplayDuration(double duration) {
        // TODO Auto-generated method stub

    }

    @Override
    public double getDisplayDuration() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public String getHeader() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public CometeColor getHeaderCometeColor() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public CometeFont getHeaderCometeFont() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean isHeaderVisible() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public int getTimePrecision() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public void setTimePrecision(int precision) {
        // TODO Auto-generated method stub

    }

    @Override
    public boolean isFreezePanelVisible() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public String getDateColumnName() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean isLegendVisible() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void setLegendVisible(boolean b) {
        // TODO Auto-generated method stub

    }

    @Override
    public boolean isXAxisOnBottom() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void loadDataFile(String fileName) {
        // TODO Auto-generated method stub

    }

    @Override
    public void setAutoScale(boolean autoscale, int axis) {
        // TODO Auto-generated method stub

    }

    @Override
    public boolean isAutoScale(int axis) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void setDataViewStyle(String id, int style) {
        // TODO Auto-generated method stub

    }

    @Override
    public int getDataViewStyle(String id) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public void setDataViewFillStyle(String id, int style) {
        // TODO Auto-generated method stub

    }

    @Override
    public void setDataViewLineStyle(String id, int style) {
        // TODO Auto-generated method stub

    }

    @Override
    public void setDataViewLineWidth(String id, int lineWidth) {
        // TODO Auto-generated method stub

    }

    @Override
    public void setDataViewBarWidth(String id, int barWidth) {
        // TODO Auto-generated method stub

    }

    @Override
    public void setDataViewCometeColor(String id, CometeColor color) {
        // TODO Auto-generated method stub

    }

    @Override
    public void setDataViewMarkerStyle(String id, int marker) {
        // TODO Auto-generated method stub

    }

    @Override
    public void setDataViewMarkerCometeColor(String id, CometeColor color) {
        // TODO Auto-generated method stub

    }

    @Override
    public void setDataViewMarkerSize(String id, int size) {
        // TODO Auto-generated method stub

    }

    @Override
    public void setDataViewDisplayName(String id, String displayName) {
        // TODO Auto-generated method stub

    }

    @Override
    public void setDateColumnName(String dateColumnName) {
        // TODO Auto-generated method stub

    }

    @Override
    public void setFreezePanelVisible(boolean freezePanelVisible) {
        // TODO Auto-generated method stub

    }

    @Override
    public void setHeaderCometeColor(CometeColor c) {
        // TODO Auto-generated method stub

    }

    @Override
    public void setHeaderCometeFont(CometeFont f) {
        // TODO Auto-generated method stub

    }

    @Override
    public void setHeaderVisible(boolean b) {
        // TODO Auto-generated method stub

    }

    @Override
    public void setIndexColumnName(String name) {
        // TODO Auto-generated method stub

    }

    @Override
    public void setLabelCometeFont(CometeFont f) {
        // TODO Auto-generated method stub

    }

    @Override
    public CometeFont getLabelCometeFont() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setScale(int scale, int axis) {
        // TODO Auto-generated method stub

    }

    @Override
    public int getScale(int axis) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public int getDataViewFillStyle(String id) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public int getDataViewLineStyle(String id) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public int getDataViewLineWidth(String id) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public int getDataViewBarWidth(String id) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public CometeColor getDataViewCometeColor(String id) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public int getDataViewMarkerStyle(String id) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public CometeColor getDataViewMarkerCometeColor(String id) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public int getDataViewMarkerSize(String id) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public int getDataViewAxis(String id) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public String getDataViewDisplayName(String id) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean containsDataView(String name) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public String getIndexColumnName() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setPreferDialogForTable(boolean preferDialog, boolean modal) {
        // TODO Auto-generated method stub

    }

    @Override
    public void setLabelPlacement(int p) {
        // TODO Auto-generated method stub

    }

    @Override
    public int getLabelPlacement() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public void setNoValueString(String noValueString) {
        // TODO Auto-generated method stub

    }

    @Override
    public String getNoValueString() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setAxisMaximum(double maximum, int axis) {
        // TODO Auto-generated method stub

    }

    @Override
    public double getAxisMaximum(int axis) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public void setAxisMinimum(double minimum, int axis) {
        // TODO Auto-generated method stub

    }

    @Override
    public double getAxisMinimum(int axis) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public void setDataViewFillMethod(String id, int m) {
        // TODO Auto-generated method stub

    }

    @Override
    public int getDataViewFillMethod(String id) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public void setAxisDrawOpposite(boolean opposite, int axis) {
        // TODO Auto-generated method stub

    }

    @Override
    public boolean isAxisDrawOpposite(int axis) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void setAxisLabelFormat(int format, int axis) {
        // TODO Auto-generated method stub

    }

    @Override
    public int getAxisLabelFormat(int axis) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public void setAxisPosition(int position, int axis) {
        // TODO Auto-generated method stub

    }

    @Override
    public int getAxisPosition(int axis) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public void setAxisVisible(boolean visible, int axis) {
        // TODO Auto-generated method stub

    }

    @Override
    public boolean isAxisVisible(int axis) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void setCustomCometeColor(CometeColor[] colorList) {
        // TODO Auto-generated method stub

    }

    @Override
    public CometeColor[] getCustomCometeColor() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setCustomFillCometeColor(CometeColor[] colorList) {
        // TODO Auto-generated method stub

    }

    @Override
    public CometeColor[] getCustomFillCometeColor() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setCustomMarkerCometeColor(CometeColor[] colorList) {
        // TODO Auto-generated method stub

    }

    @Override
    public CometeColor[] getCustomMarkerCometeColor() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setCustomLineStyle(int[] lineStyleList) {
        // TODO Auto-generated method stub

    }

    @Override
    public int[] getCustomLineStyle() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setCustomAxis(int[] axisList) {
        // TODO Auto-generated method stub

    }

    @Override
    public int[] getCustomAxis() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setCustomMarkerStyle(int[] styleList) {
        // TODO Auto-generated method stub

    }

    @Override
    public int[] getCustomMarkerStyle() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setCustomMarkerSize(int[] sizeList) {
        // TODO Auto-generated method stub

    }

    @Override
    public int[] getCustomMarkerSize() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setCustomLineWidth(int[] widthList) {
        // TODO Auto-generated method stub

    }

    @Override
    public int[] getCustomLineWidth() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setCustomBarWidth(int[] widthList) {
        // TODO Auto-generated method stub

    }

    @Override
    public int[] getCustomBarWidth() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setCustomDataViewStyle(int[] styleList) {
        // TODO Auto-generated method stub

    }

    @Override
    public int[] getCustomDataViewStyle() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setCustomFillStyle(int[] styleList) {
        // TODO Auto-generated method stub

    }

    @Override
    public int[] getCustomFillStyle() {
        // TODO Auto-generated method stub
        return null;
    }


    @Override
    public double getDataViewTransformA0(String name) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public void setDataViewTransformA0(String name, double transform) {
        // TODO Auto-generated method stub

    }

    @Override
    public double getDataViewTransformA1(String name) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public void setDataViewTransformA1(String name, double transform) {
        // TODO Auto-generated method stub

    }

    @Override
    public double getDataViewTransformA2(String name) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public void setDataViewTransformA2(String name, double transform) {
        // TODO Auto-generated method stub

    }

    @Override
    public boolean isDataViewClickable(String name) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void setDataViewClickable(String name, boolean clickable) {
        // TODO Auto-generated method stub

    }

    @Override
    public boolean isDataViewLabelVisible(String visible) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public int getDataViewInterpolationMethod(String name) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public void setDataViewInterpolationMethod(String name, int method) {
        // TODO Auto-generated method stub

    }

    @Override
    public int getDataViewInterpolationStep(String name) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public void setDataViewInterpolationStep(String name, int step) {
        // TODO Auto-generated method stub

    }

    @Override
    public double getDataViewHermiteBias(String name) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public void setDataViewHermiteBias(String name, double bias) {
        // TODO Auto-generated method stub

    }

    @Override
    public double getDataViewHermiteTension(String name) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public void setDataViewHermiteTension(String name, double tension) {
        // TODO Auto-generated method stub

    }

    @Override
    public int getDataViewSmoothingMethod(String name) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public void setDataViewSmoothingMethod(String name, int method) {
        // TODO Auto-generated method stub

    }

    @Override
    public int getDataViewSmoothingNeighbors(String name) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public void setDataViewSmoothingNeighbors(String name, int neighbors) {
        // TODO Auto-generated method stub

    }

    @Override
    public double getDataViewSmoothingGaussSigma(String name) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public void setDataViewSmoothingGaussSigma(String name, double sigma) {
        // TODO Auto-generated method stub

    }

    @Override
    public int getDataViewSmoothingExtrapolation(String name) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public void setDataViewSmoothingExtrapolation(String name, int extrapolation) {
        // TODO Auto-generated method stub

    }

    @Override
    public int getDataViewMathFunction(String name) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public void setDataViewMathFunction(String name, int function) {
        // TODO Auto-generated method stub

    }

    @Override
    public void setDataViewLabelVisible(String name, boolean visible) {
        // TODO Auto-generated method stub

    }

    @Override
    public int getDataViewHighlightMethod(String name) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public void setDataViewHighlightMethod(String name, int method) {
        // TODO Auto-generated method stub

    }

    @Override
    public double getDataViewHighlightCoefficient(String name) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public void setDataViewHighlightCoefficient(String name, double coef) {
        // TODO Auto-generated method stub

    }

    @Override
    public boolean isDataViewHighlighted(String name) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void setDataViewHighlighted(String name, boolean highlighted) {
        // TODO Auto-generated method stub

    }

    @Override
    public void setAutoHighlightOnLegend(boolean autoHighlightOnLegend) {
        // TODO Auto-generated method stub

    }

    @Override
    public boolean isAutoHighlightOnLegend() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean isDataViewRemovingEnabled() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void setDataViewRemovingEnabled(boolean curveRemovingEnabled) {
        // TODO Auto-generated method stub

    }

    @Override
    public boolean isCleanDataViewConfigurationOnRemoving() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void setCleanDataViewConfigurationOnRemoving(boolean cleanCurveConfigurationOnRemoving) {
        // TODO Auto-generated method stub

    }

    @Override
    public boolean hasDataViewProperties(String name) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void cleanDataViewProperties(String name) {
        // TODO Auto-generated method stub

    }

    @Override
    public PlotProperties getDataViewPlotProperties(String name) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setDataViewPlotProperties(String name, PlotProperties properties) {
        // TODO Auto-generated method stub

    }

    @Override
    public BarProperties getDataViewBarProperties(String name) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setDataViewBarProperties(String name, BarProperties properties) {
        // TODO Auto-generated method stub

    }

    @Override
    public CurveProperties getDataViewCurveProperties(String name) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setDataViewCurveProperties(String name, CurveProperties properties) {
        // TODO Auto-generated method stub

    }

    @Override
    public MarkerProperties getDataViewMarkerProperties(String name) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setDataViewMarkerProperties(String name, MarkerProperties properties) {
        // TODO Auto-generated method stub

    }

    @Override
    public TransformationProperties getDataViewTransformationProperties(String name) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setDataViewTransformationProperties(String name, TransformationProperties properties) {
        // TODO Auto-generated method stub

    }

    @Override
    public InterpolationProperties getDataViewInterpolationProperties(String name) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setDataViewInterpolationProperties(String name, InterpolationProperties properties) {
        // TODO Auto-generated method stub

    }

    @Override
    public SmoothingProperties getDataViewSmoothingProperties(String name) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setDataViewSmoothingProperties(String name, SmoothingProperties properties) {
        // TODO Auto-generated method stub

    }

    @Override
    public MathProperties getDataViewMathProperties(String name) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setDataViewMathProperties(String name, MathProperties properties) {
        // TODO Auto-generated method stub

    }

    @Override
    public ChartProperties getChartProperties() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setChartProperties(ChartProperties properties) {
        // TODO Auto-generated method stub

    }

    @Override
    public AxisProperties getAxisProperties(int axisChoice) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setAxisProperties(AxisProperties properties, int axisChoice) {
        // TODO Auto-generated method stub

    }

    @Override
    public void setCustomUserFormat(String[] formatList) {
        // TODO Auto-generated method stub

    }

    @Override
    public String[] getCustomUserFormat() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setCyclingCustomMap(boolean cyclingCustomMap) {
        // TODO Auto-generated method stub

    }

    @Override
    public boolean isCyclingCustomMap() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean isAxisSelectionVisible() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void setAxisSelectionVisible(boolean axisSelectionVisible) {
        // TODO Auto-generated method stub

    }

    @Override
    public void setAxisScaleEditionEnabled(boolean enabled, int axis) {
        // TODO Auto-generated method stub

    }

    @Override
    public void resetAll() {
        // TODO Auto-generated method stub

    }

    @Override
    public void resetAll(boolean clearConfiguration) {
        // TODO Auto-generated method stub

    }

    @Override
    public void addExpression(String name, String expression, int axis, String[] variables, boolean x) {
        // TODO Auto-generated method stub

    }

    @Override
    public void addExpression(String id, String name, String expression, int axis, String[] variables, boolean x) {
        // TODO Auto-generated method stub

    }

    @Override
    public void setDataViewEditable(String name, boolean editable) {
        // TODO Auto-generated method stub

    }



    @Override
    public boolean isLimitDrawingZone() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void setLimitDrawingZone(boolean limitDrawingZone) {
        // TODO Auto-generated method stub

    }

    @Override
    public boolean isAutoHideViews() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void setAutoHideViews(boolean hide) {
        // TODO Auto-generated method stub

    }

    /**
     * @param args
     */
    public static void main(String[] args) {
        final Display display = new Display();
        final Shell shell = new Shell(display);

        shell.setBounds(0, 0, 990, 510);
        shell.setLayout(new FillLayout());
        shell.setText("ChartViewer");

        Chart chartViewer = new Chart(shell, SWT.NONE);

        // mixed x and y values
        double[] t1 = new double[] { 1, 5, 2, 9, 3, 12, 4, 0, 5, 23, 6, 15, 7, 10, 8, 19 };
        double[] t2 = new double[] { 1, 6, 2, -3, 3, 0, 4, 7, 5, 54, 6, 12, 7, 34, 8, 32, 9, 31, 10, 23 };
        Map<String, Object> data = new HashMap<String, Object>();
        data.put("first", t1);
        data.put("second", t2);
        chartViewer.setData(data);

        chartViewer.setAxisName("tesssst", IChartViewer.X);

        // shell.pack();
        shell.open();

        while (!shell.isDisposed()) {
            if (!display.readAndDispatch()) {
                display.sleep();
            }
        }

        display.dispose();
        System.exit(0);
    }

    @Override
    public String getSnapshotDirectory() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setSnapshotDirectory(String snapshotDirectory) {
        // TODO Auto-generated method stub

    }

    @Override
    public String getSnapshotFile() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void addSnapshotListener(ISnapshotListener listener) {
        // TODO Auto-generated method stub

    }

    @Override
    public void removeSnapshotListener(ISnapshotListener listener) {
        // TODO Auto-generated method stub

    }

    @Override
    public void saveDataFile(String path) {
        // TODO Auto-generated method stub

    }

    @Override
    public String getDataDirectory() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setDataDirectory(String path) {
        // TODO Auto-generated method stub

    }

    @Override
    public String getDataFile() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void addDataTransferListener(IDataTransferListener listener) {
        // TODO Auto-generated method stub

    }

    @Override
    public void removeDataTransferListener(IDataTransferListener listener) {
        // TODO Auto-generated method stub

    }

    @Override
    public String getAxisName(int axis) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setDataViewFillCometeColor(String name, CometeColor color) {
        // TODO Auto-generated method stub

    }

    @Override
    public CometeColor getDataViewFillCometeColor(String id) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setDataViewErrorCometeColor(String id, CometeColor color) {
        // TODO Auto-generated method stub

    }

    @Override
    public CometeColor getDataViewErrorCometeColor(String id) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean isDataViewSamplingAllowed(String name) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void setDataViewSamplingAllowed(String name, boolean allowed) {
        // TODO Auto-generated method stub

    }

    @Override
    public ErrorProperties getDataViewErrorProperties(String name) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setDataViewErrorProperties(String name, ErrorProperties properties) {
        // TODO Auto-generated method stub

    }

    @Override
    public boolean isDataViewErrorVisible(String id) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void setDataViewErrorVisible(String id, boolean visible) {
        // TODO Auto-generated method stub

    }

    @Override
    public SamplingProperties getSamplingProperties() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setSamplingProperties(SamplingProperties properties) {
        // TODO Auto-generated method stub

    }

    @Override
    public String[][] getAllViewsIdsAndNames() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setLabels(int axis, String[] labels, double[] labelPositions) {
        // TODO Auto-generated method stub

    }

    @Override
    public void clearLabels(int axis) {
        // TODO Auto-generated method stub

    }

    @Override
    protected Control initControl(int childStyle) {
        // TODO Auto-generated method stub
        return null;
    }

}
