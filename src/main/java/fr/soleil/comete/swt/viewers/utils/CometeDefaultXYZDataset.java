package fr.soleil.comete.swt.viewers.utils;

import org.gumtree.vis.interfaces.IXYZDataset;
import org.jfree.data.xy.DefaultXYZDataset;

public class CometeDefaultXYZDataset extends DefaultXYZDataset implements IXYZDataset {

    /**
     *
     */
    private static final long serialVersionUID = -6340560734547843687L;

    private final String title = "Title";

    public int getXSize(int series) {
        return 1;
    }

    public int getYSize(int series) {
        return super.getItemCount(series);
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public String getXTitle() {
        return null;
    }

    @Override
    public String getXUnits() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String getYTitle() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String getYUnits() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setTitle(String arg0) {
        // TODO Auto-generated method stub

    }

    @Override
    public void setXTitle(String arg0) {
        // TODO Auto-generated method stub

    }

    @Override
    public void setXUnits(String arg0) {
        // TODO Auto-generated method stub

    }

    @Override
    public void setYTitle(String arg0) {
        // TODO Auto-generated method stub

    }

    @Override
    public void setYUnits(String arg0) {
        // TODO Auto-generated method stub

    }

    @Override
    public double getXBlockSize() {
        // TODO Auto-generated method stub
        return 1;
    }

    @Override
    public double getXMax() {
        // TODO Auto-generated method stub
        return 1;
    }

    @Override
    public double getXMin() {
        // TODO Auto-generated method stub
        return 1;
    }

    @Override
    public double getYBlockSize() {
        // TODO Auto-generated method stub
        return 1;
    }

    @Override
    public double getYMax() {
        // TODO Auto-generated method stub
        return 1;
    }

    @Override
    public double getYMin() {
        // TODO Auto-generated method stub
        return 1;
    }

    @Override
    public double getZMax() {
        // TODO Auto-generated method stub
        return 1;
    }

    @Override
    public double getZMin() {
        // TODO Auto-generated method stub
        return 1;
    }

    @Override
    public String getZTitle() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String getZUnits() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public double getZofXY(int arg0, int arg1) {
        // TODO Auto-generated method stub
        return 1;
    }

    @Override
    public void setZTitle(String arg0) {
        // TODO Auto-generated method stub

    }

    @Override
    public void setZUnits(String arg0) {
        // TODO Auto-generated method stub

    }

    @Override
    public void update() {
        // TODO Auto-generated method stub

    }

}
