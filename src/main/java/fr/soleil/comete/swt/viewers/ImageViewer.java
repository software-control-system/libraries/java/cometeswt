package fr.soleil.comete.swt.viewers;

import java.awt.Frame;
import java.awt.event.MouseWheelEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;

import org.eclipse.swt.SWT;
import org.eclipse.swt.awt.SWT_AWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseWheelListener;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.jfree.chart.ChartMouseEvent;
import org.jfree.chart.ChartMouseListener;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.title.TextTitle;
import org.jfree.data.general.Dataset;
import org.jfree.data.xy.DefaultXYZDataset;
import org.jfree.data.xy.XYZDataset;

import fr.soleil.comete.definition.event.ImageViewerEvent;
import fr.soleil.comete.definition.event.ImageViewerEvent.Reason;
import fr.soleil.comete.definition.listener.IDataTransferListener;
import fr.soleil.comete.definition.listener.IImageViewerListener;
import fr.soleil.comete.definition.listener.ISnapshotListener;
import fr.soleil.comete.definition.listener.MaskListener;
import fr.soleil.comete.definition.util.IValueConvertor;
import fr.soleil.comete.definition.widget.IComponent;
import fr.soleil.comete.definition.widget.IImageViewer;
import fr.soleil.comete.definition.widget.properties.ImageProperties;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.comete.definition.widget.util.Gradient;
import fr.soleil.comete.definition.widget.util.IRoi;
import fr.soleil.comete.definition.widget.util.Mask;
import fr.soleil.comete.definition.widget.util.MathematicSector;
import fr.soleil.comete.swt.CometeComposite;
import fr.soleil.comete.swt.viewers.utils.IPlot;
import fr.soleil.comete.swt.viewers.utils.PlotFactory;
import fr.soleil.lib.project.math.ArrayUtils;

public class ImageViewer extends CometeComposite<Control> implements IImageViewer {

    private final List<IImageViewerListener> listeners = new ArrayList<IImageViewerListener>();;

    private Object flatNumberMatrix = null;
    private int matrixWidth = 0;
    private int matrixHeight = 0;
    private final int horizontalAlignment = IComponent.CENTER;
    private String title = null;

    private IPlot plot;
    private Frame frame;
    private MouseWheelListener mouseWheelListener;
    private KeyListener keyListener;
    private ChartMouseListener chartMouseListener;

    public ImageViewer(Composite parent, int style) {
        super(parent, SWT.EMBEDDED | style);

        frame = SWT_AWT.new_Frame(this);
    }

    protected void embedPlot(IPlot plot) {
        if (plot instanceof JPanel) {
            if (frame != null) {
                frame.add((JPanel) plot);
            }
        }
        else {
            throw new IllegalArgumentException("must be a chart plot panel");
        }
    }

    public void setPlot(final IPlot plot) {
        final IPlot oldPlot = this.plot;
        if (oldPlot != null) {
            oldPlot.cleanUp();
            frame.remove((JPanel) oldPlot);
        }
        this.plot = plot;
        title = getText();
        final Composite composite = this;
        Display display = Display.getCurrent();
        if (display == null) {
            display = Display.getDefault();
        }
        display.asyncExec(new Runnable() {

            @Override
            public void run() {
                embedPlot(plot);
                removeListeners(oldPlot);
                addListeners();
                composite.pack();
                composite.getParent().layout(true, true);
            }
        });
    }

    private void removeListeners(IPlot plot) {
        if (plot != null) {
            removeMouseWheelListener(mouseWheelListener);
            removeKeyListener(keyListener);
            plot.removeChartMouseListener(chartMouseListener);
        }
    }

    private void addListeners() {
        if (plot != null) {
            mouseWheelListener = new MouseWheelListener() {

                @Override
                public void mouseScrolled(MouseEvent event) {
                    JPanel panel = null;
                    if (plot instanceof JPanel) {
                        panel = (JPanel) plot;
                    }
                    MouseWheelEvent awtEvent = org.gumtree.vis.listener.SWT_AWT.toMouseWheelEvent(
                            event, panel);
                    plot.processMouseWheelEvent(awtEvent);
                }
            };
            addMouseWheelListener(mouseWheelListener);

            keyListener = new KeyListener() {

                boolean keyPressed = false;

                @Override
                public void keyReleased(KeyEvent event) {
                    switch (event.keyCode) {
                        case SWT.DEL:
                            plot.removeSelectedMask();
                            break;
                        default:
                            break;
                    }
                    switch (event.character) {
                        default:
                            break;
                    }
                    keyPressed = false;
                }

                @Override
                public void keyPressed(KeyEvent event) {
                    switch (event.keyCode) {
                        case SWT.ARROW_UP:
                            plot.moveSelectedMask(event.keyCode);
                            break;
                        case SWT.ARROW_LEFT:
                            plot.moveSelectedMask(event.keyCode);
                            break;
                        case SWT.ARROW_RIGHT:
                            plot.moveSelectedMask(event.keyCode);
                            break;
                        case SWT.ARROW_DOWN:
                            plot.moveSelectedMask(event.keyCode);
                            break;
                        default:
                            break;
                    }
                    switch (event.stateMask) {
                        case SWT.CTRL:
                            if ((event.keyCode == 'c') || (event.keyCode == 'C')) {
                                if (!keyPressed) {
                                    plot.doCopy();
                                }
                            }
                            else if ((event.keyCode == 'z') || (event.keyCode == 'Z')
                                    || (event.keyCode == 'r') || (event.keyCode == 'R')) {
                                if (!keyPressed) {
                                    plot.restoreAutoBounds();
                                }
                            }
                            else if ((event.keyCode == 'p') || (event.keyCode == 'P')) {
                                System.out.println("p pressed");
                                if (!keyPressed) {
                                    Thread newThread = new Thread(new Runnable() {

                                        @Override
                                        public void run() {
                                            plot.createChartPrintJob();
                                        }
                                    });
                                    newThread.start();
                                }
                            }
                            else if ((event.keyCode == 'e') || (event.keyCode == 'E')) {
                                System.out.println("s pressed");
                                if (!keyPressed) {
                                    Thread newThread = new Thread(new Runnable() {

                                        @Override
                                        public void run() {
                                            try {
                                                plot.doSaveAs();
                                            }
                                            catch (IOException e) {
                                                handleException(e);
                                            }
                                        }
                                    });
                                    newThread.start();
                                }
                            }
                            keyPressed = true;
                            break;
                        default:
                            break;
                    }
                }
            };
            addKeyListener(keyListener);

            final Composite composite = this;

            chartMouseListener = new ChartMouseListener() {

                @Override
                public void chartMouseMoved(ChartMouseEvent event) {

                }

                @Override
                public void chartMouseClicked(ChartMouseEvent event) {
                    Display.getDefault().asyncExec(new Runnable() {

                        @Override
                        public void run() {
                            if (!composite.isFocusControl()) {
                                composite.setFocus();
                            }
                        }
                    });
                }
            };
            plot.addChartMouseListener(chartMouseListener);
        }
    }

    public JFreeChart getChart() {
        if (plot != null) {
            return plot.getChart();
        }
        else {
            return null;
        }
    }

    @Override
    public void setVisible(boolean visible) {
        if (plot != null) {
            plot.setVisible(visible);
        }
        super.setVisible(visible);
    }

    public void setDataset(Dataset dataset) {
        try {
            boolean createNewPlot = true;

            if (!createNewPlot) {
                plot.setDataset(dataset);
            }
            else {
                if (plot != null) {
                    JPanel panel = (JPanel) plot;
                    frame.remove(panel);
                }
                IPlot newPlot = null;
                if (dataset instanceof XYZDataset) {
                    newPlot = PlotFactory.createHist2DPanel(((XYZDataset) dataset), title);
                }
                setPlot(newPlot);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Dataset getDataset() {
        return getPlot().getDataset();
    }

    public IPlot getPlot() {
        return plot;
    }

    @Override
    public void redraw() {
        plot.repaint();
        super.redraw();
    }

    public void handleException(final Exception e) {
        Display display = Display.getCurrent();
        if (display == null) {
            display = Display.getDefault();
        }
        display.asyncExec(new Runnable() {
            @Override
            public void run() {
                if (getShell() != null) {
                    MessageBox messageBox = new MessageBox(getShell(), SWT.ICON_WARNING | SWT.OK);
                    messageBox.setText("Failed to Save");
                    messageBox.setMessage("failed : " + e.getMessage());
                    messageBox.open();
                    // MessageDialog.openError(getShell(), "Failed to Save", "failed to save " +
                    // "the image: " + e.getMessage());

                }
            }
        });
    }

    @Override
    public void dispose() {
        if (!isDisposed()) {
            if (mouseWheelListener != null) {
                removeMouseWheelListener(mouseWheelListener);
                mouseWheelListener = null;
            }
            if (keyListener != null) {
                removeKeyListener(keyListener);
                keyListener = null;
            }
            if (plot != null) {
                plot.cleanUp();
                if (chartMouseListener != null) {
                    plot.removeChartMouseListener(chartMouseListener);
                    chartMouseListener = null;
                }
                frame.remove((JPanel) plot);
                frame.dispose();
                plot = null;
            }
        }
        frame = null;
        super.dispose();
    }

    @Override
    public String getText() {
        String result = null;
        if (plot != null) {
            JFreeChart chart = plot.getChart();
            if (chart != null) {
                TextTitle title = chart.getTitle();
                if (title != null) {
                    result = title.getText();
                }
            }
        }
        return result;
    }

    @Override
    public void setText(String value) {
        if (plot != null) {
            JFreeChart chart = plot.getChart();
            if (chart != null) {
                chart.setTitle(value);
                title = value;
            }
        }
    }

    @Override
    public Object getFlatNumberMatrix() {
        return flatNumberMatrix;
    }

    @Override
    public Object[] getNumberMatrix() {
        return (Object[]) ArrayUtils.convertArrayDimensionFrom1ToN(flatNumberMatrix, matrixHeight,
                matrixWidth);
    }

    @Override
    public void setFlatNumberMatrix(Object value, int width, int height) {
        if ((value instanceof Number[]) || (value instanceof double[])) {

            Double[] temp = null;

            if (value instanceof double[]) {

                double[] doubleValue = (double[]) value;

                temp = new Double[doubleValue.length];
                for (int i = 0; i < doubleValue.length; i++) {
                    temp[i] = doubleValue[i];
                }
            }
            else if (value instanceof Double[]) {
                temp = (Double[]) value;
            }
            else if (value instanceof Number[]) {
                Number[] doubleValue = (Number[]) value;

                temp = new Double[doubleValue.length];
                for (int i = 0; i < doubleValue.length; i++) {
                    temp[i] = (Double) doubleValue[i];
                }

            }

            flatNumberMatrix = temp;
            matrixWidth = width;
            matrixHeight = height;

            XYZDataset dataset = convertToDataset(temp, width, height);
            setDataset(dataset);
        }
    }

    @Override
    public void setNumberMatrix(Object[] value) {
        Object flatValue = null;
        int width = 0, height = 0;
        if (value != null) {
            height = value.length;
            int[] shape = ArrayUtils.recoverShape(value);
            if ((shape != null) && (shape.length == 2)) {
                height = shape[0];
                width = shape[1];
                flatValue = ArrayUtils.convertArrayDimensionFromNTo1(value);
            }
        }
        setFlatNumberMatrix(flatValue, width, height);
    }

    private XYZDataset convertToDataset(Object value, int width, int height) {

        DefaultXYZDataset dataset = null;
        // DefaultXYZDataset dataset = new DefaultXYZDataset();

        if (value != null) {

            double[] x = new double[height * width];
            double[] y = new double[height * width];
            double[] z = new double[height * width];

            if (value instanceof Double[]) {
                Double[] flatNumberMatrix = (Double[]) value;

                for (int l = 0; l < height; l++) {
                    for (int c = 0; c < width; c++) {
                        x[l * width + c] = c;
                        y[l * width + c] = l;
                        z[l * width + c] = flatNumberMatrix[l * width + c];
                    }
                }
            }
            else if (value instanceof double[]) {
                double[] flatNumberMatrix = (double[]) value;

                for (int l = 0; l < height; l++) {
                    for (int c = 0; c < width; c++) {
                        x[l * width + c] = c;
                        y[l * width + c] = l;
                        z[l * width + c] = flatNumberMatrix[l * width + c];
                    }
                }
            }

            dataset = new DefaultXYZDataset();
            dataset.addSeries("image", new double[][] { x, y, z });
        }
        else {
            // dataset.addSeries("null", new double[][] { {}, {}, {} });
        }

        return dataset;
    }

    @Override
    public int getMatrixDataHeight(Class<?> arg0) {
        return matrixHeight;
    }

    @Override
    public int getMatrixDataWidth(Class<?> arg0) {
        return matrixWidth;
    }

    @Override
    public void addImageViewerListener(IImageViewerListener listener) {
        if (!listeners.contains(listener)) {
            listeners.add(listener);
        }
    }

    @Override
    public void removeImageViewerListener(IImageViewerListener listener) {
        if (listeners.contains(listener)) {
            synchronized (listeners) {
                listeners.remove(listener);
            }
        }
    }

    public void fireValueChanged() {
        ImageViewerEvent imageViewerEvent = new ImageViewerEvent(this, null, flatNumberMatrix,
                Reason.VALUE);
        for (IImageViewerListener listener : listeners) {
            if (listener != null) {
                listener.imageViewerChanged(imageViewerEvent);
            }
        }
    }

    @Override
    public void setEditable(boolean b) {
        // there is no table to edit
    }

    @Override
    public boolean isEditable() {
        return false;
    }

    @Override
    public void setHorizontalAlignment(int halign) {
        // nop
    }

    @Override
    public int getHorizontalAlignment() {
        return horizontalAlignment;
    }

    @Override
    public boolean isPreferFlatValues(Class<?> arg0) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void addMaskListener(MaskListener listener) {
        // TODO Auto-generated method stub

    }

    @Override
    public void removeMaskListener(MaskListener listener) {
        // TODO Auto-generated method stub

    }

    @Override
    public void removeAllMaskListeners() {
        // TODO Auto-generated method stub

    }

    @Override
    public boolean setMask(Mask mask) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public Mask getMask() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public int getImageWidth() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public int getImageHeight() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public String getImageName() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setImageName(String name) {
        // TODO Auto-generated method stub

    }

    @Override
    public Gradient getGradient() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setGradient(Gradient gradient) {
        // TODO Auto-generated method stub

    }

    @Override
    public boolean isUseMaskManagement() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void setUseMaskManagement(boolean useMask) {
        // TODO Auto-generated method stub

    }

    @Override
    public void loadMask(String path) {
        // TODO Auto-generated method stub

    }

    @Override
    public void saveMask(String path) {
        // TODO Auto-generated method stub

    }

    @Override
    public boolean isAlwaysFitMaxSize() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void setAlwaysFitMaxSize(boolean alwaysFitMaxSize) {
        // TODO Auto-generated method stub

    }

    @Override
    public void setShowRoiInformationTable(boolean showTable) {
        // TODO Auto-generated method stub

    }

    @Override
    public boolean getShowRoiInformationTable() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean isDrawBeamPosition() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void setDrawBeamPosition(boolean drawBeam) {
        // TODO Auto-generated method stub

    }

    @Override
    public double[] getBeamPoint() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setBeamPoint(double... theBeamPoint) {
        // TODO Auto-generated method stub

    }

    @Override
    public void clearBeamPoint() {
        // TODO Auto-generated method stub

    }

    @Override
    public void registerSectorClass(Class<?> sectorClass) {
        // TODO Auto-generated method stub

    }

    @Override
    public boolean isUseSectorManagement() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void setUseSectorManagement(boolean useSector) {
        // TODO Auto-generated method stub

    }

    @Override
    public MathematicSector getSector() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setSector(MathematicSector sector) {
        // TODO Auto-generated method stub

    }

    @Override
    public IValueConvertor getXAxisConvertor() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setXAxisConvertor(IValueConvertor xAxisConvertor) {
        // TODO Auto-generated method stub

    }

    @Override
    public IValueConvertor getYAxisConvertor() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setYAxisConvertor(IValueConvertor yAxisConvertor) {
        // TODO Auto-generated method stub

    }

    @Override
    public boolean isSingleRoiMode() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void setSingleRoiMode(boolean singleRoiMode) {
        // TODO Auto-generated method stub

    }

    @Override
    public boolean isCleanOnDataSetting() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void setCleanOnDataSetting(boolean clean) {
        // TODO Auto-generated method stub

    }

    @Override
    public boolean isDualProfilingEnabled() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void setDualProfilingEnabled(boolean dualProfilingEnabled) {
        // TODO Auto-generated method stub

    }

    @Override
    public void addRoi(IRoi roi) {
        // TODO Auto-generated method stub

    }

    @Override
    public void addRoi(IRoi roi, boolean centered) {
        // TODO Auto-generated method stub

    }

    public static void main(String[] args) {
        final Display display = new Display();
        final Shell shell = new Shell(display);

        shell.setBounds(0, 0, 990, 510);
        shell.setLayout(new FillLayout());
        shell.setText("ImageViewer");

        ImageViewer imageViewer = new ImageViewer(shell, SWT.NONE);

        // Number[] t0 = new Number[] { 1., 2., 3., 4., 5., 6., 7., 8., 9., 10., 11., 12., 13., 14.
        // };
        // imageViewer.setFlatNumberMatrix(t0, 2, 7);
        //
        // Double[] t1 = new Double[] { 1., 2., 3., 4., 5., 6., 7., 8., 9., 10., 11., 12., 13., 14.,
        // 15. };
        // imageViewer.setFlatNumberMatrix(t1, 3, 5);

        // TODO voir pourquoi l'�chelle ne se met pas � jour lors du changement de dataset
        // le min et le max ne sont pas mis � jour
        double[][] t2 = new double[][] { { 2, 4, 6 }, { 8, 10, 12 }, { 14, 16, 18 }, { 20, 22, 24 } };
        imageViewer.setNumberMatrix(t2);

        // shell.pack();
        shell.open();

        while (!shell.isDisposed()) {
            if (!display.readAndDispatch()) {
                display.sleep();
            }
        }

        display.dispose();
        System.exit(0);
    }

    @Override
    public CometeColor getNanColor() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setNanColor(CometeColor nanColor) {
        // TODO Auto-generated method stub

    }

    @Override
    public CometeColor getPositiveInfinityColor() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setPositiveInfinityColor(CometeColor positiveInfinityColor) {
        // TODO Auto-generated method stub

    }

    @Override
    public CometeColor getNegativeInfinityColor() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setNegativeInfinityColor(CometeColor negativeInfinityColor) {
        // TODO Auto-generated method stub

    }

    @Override
    public String getFormat() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setFormat(String format) {
        // TODO Auto-generated method stub

    }

    @Override
    public String getSnapshotDirectory() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setSnapshotDirectory(String snapshotDirectory) {
        // TODO Auto-generated method stub

    }

    @Override
    public String getSnapshotFile() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void addSnapshotListener(ISnapshotListener listener) {
        // TODO Auto-generated method stub

    }

    @Override
    public void removeSnapshotListener(ISnapshotListener listener) {
        // TODO Auto-generated method stub

    }

    @Override
    public void loadDataFile(String fileName) {
        // TODO Auto-generated method stub

    }

    @Override
    public void saveDataFile(String path) {
        // TODO Auto-generated method stub

    }

    @Override
    public String getDataDirectory() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setDataDirectory(String path) {
        // TODO Auto-generated method stub

    }

    @Override
    public String getDataFile() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void addDataTransferListener(IDataTransferListener listener) {
        // TODO Auto-generated method stub

    }

    @Override
    public void removeDataTransferListener(IDataTransferListener listener) {
        // TODO Auto-generated method stub

    }

    @Override
    public boolean isShowRoiInformationTable() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public ImageProperties getImageProperties() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setImageProperties(ImageProperties properties) {
        // TODO Auto-generated method stub

    }

    @Override
    protected Control initControl(int childStyle) {
        // TODO Auto-generated method stub
        return null;
    }

}
