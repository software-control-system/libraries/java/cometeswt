package fr.soleil.comete.swt.viewers.utils;

import java.io.File;
import java.io.IOException;

import org.jfree.data.general.Dataset;

public interface IExporter {

    public void export(File file, Dataset dataset) throws IOException;

	public String getExtensionName();
}
