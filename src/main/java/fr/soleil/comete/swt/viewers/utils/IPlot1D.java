/*******************************************************************************
 * Copyright (c) 2010 Australian Nuclear Science and Technology Organisation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Norman Xiong (nxi@Bragg Institute) - initial API and implementation
 ******************************************************************************/
package fr.soleil.comete.swt.viewers.utils;

import java.awt.Color;
import java.awt.Shape;
import java.awt.Stroke;

import org.gumtree.vis.plot1d.MarkerShape;
import org.jfree.data.xy.YIntervalSeries;


/**
 * @author nxi
 *
 */
public interface IPlot1D extends IPlot{

    public Color getCurveColor(YIntervalSeries pattern);

    public Shape getCurveMarkerShape(YIntervalSeries pattern);

    public Stroke getCurveStroke(YIntervalSeries pattern);

	public Stroke getErrorBarStroke();

    public boolean isCurveMarkerFilled(YIntervalSeries pattern);

    public boolean isCurveVisible(YIntervalSeries pattern);

	public boolean isErrorBarEnabled();

	public boolean isLogarithmXEnabled();

	public boolean isLogarithmYEnabled();

	public boolean isMarkerEnabled();

    public void setCurveColor(YIntervalSeries pattern, Color color);

    public void setCurveMarkerFilled(YIntervalSeries pattern, boolean filled);

    public void setCurveMarkerShape(YIntervalSeries pattern, MarkerShape shape);

    public void setCurveStroke(YIntervalSeries pattern, float stroke);

    public void setCurveVisible(YIntervalSeries pattern, boolean visible);

	public void setErrorBarEnabled(boolean enabled);

	public void setErrorBarStroke(float stroke);

	public void setLogarithmXEnabled(boolean enabled);

	public void setLogarithmYEnabled(boolean enabled);

	public void setMarkerEnabled(boolean enabled);

	public void setSelectedSeries(int seriesIndex);
}
