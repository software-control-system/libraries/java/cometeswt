package fr.soleil.comete.swt;

import java.util.EventObject;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

import fr.soleil.comete.definition.listener.ISpinnerListener;
import fr.soleil.comete.definition.widget.ISpinner;
import fr.soleil.comete.swt.util.SWTUtils;

public class Spinner extends AbstractNumberRangeControl<org.eclipse.swt.widgets.Spinner> implements ISpinner,
        SelectionListener {

    public Spinner(Composite parent, int childStyle) {
        super(parent, childStyle);
    }

    @Override
    protected org.eclipse.swt.widgets.Spinner initControl(int childStyle) {
        org.eclipse.swt.widgets.Spinner spinner = new org.eclipse.swt.widgets.Spinner(this, childStyle);
        spinner.addSelectionListener(this);
        return spinner;
    }

    @Override
    protected Number getControlValue() {
        Number value = null;
        if (getControl() != null) {
            value = getControl().getSelection();
        }
        return value;
    }

    @Override
    protected void setControlValue(Number value) {
        if ((getControl() != null) && (value != null)) {
            getControl().setSelection(value.intValue());
        }
    }

    @Override
    protected Number getControlMinimum() {
        Number min = null;
        if (getControl() != null) {
            min = getControl().getMinimum();
        }
        return min;
    }

    @Override
    protected void setControlMinimum(Number min) {
        if ((min != null) && (getControl() != null)) {
            getControl().setMinimum(min.intValue());
        }
    }

    @Override
    protected Number getControlMaximum() {
        Number max = null;
        if (getControl() != null) {
            max = getControl().getMaximum();
        }
        return max;
    }

    @Override
    protected void setControlMaximum(Number max) {
        if ((max != null) && (getControl() != null)) {
            getControl().setMaximum(max.intValue());
        }
    }

    @Override
    public Number getMinimum() {
        return getControlMinimum();
    }

    @Override
    public void setMinimum(Number minimum) {
        setMinimumValue(minimum);
    }

    @Override
    public Number getMaximum() {
        return getControlMaximum();
    }

    @Override
    public void setMaximum(Number maximum) {
        setMaximumValue(maximum);
    }

    @Override
    public Number getStep() {
        int step = 0;
        if ((getControl() != null) && !getControl().isDisposed()) {
            step = getControl().getIncrement();
        }
        return step;
    }

    @Override
    public void setStep(final Number step) {
        if ((getControl() != null) && (step != null)) {
            SWTUtils.asyncExec(new Runnable() {
                @Override
                public void run() {
                    if (!getControl().isDisposed()) {
                        getControl().setIncrement(step.intValue());
                    }
                }
            });
        }
    }


    @Override
    public void setEditable(boolean b) {
        setEnabled(b);
    }

    @Override
    public boolean isEditable() {
        return isEnabled();
    }


    @Override
    public void addSpinnerListener(ISpinnerListener listener) {
        addNumberListener(listener);
    }

    @Override
    public void removeSpinnerListener(ISpinnerListener listener) {
        removeNumberListener(listener);
    }

    @Override
    public void widgetDefaultSelected(SelectionEvent arg0) {
    }

    @Override
    public void widgetSelected(SelectionEvent arg0) {
        EventObject event = new EventObject(this);
        fireValueChanged(event);
    }

    public static void main(String[] args) {
        final Display display = new Display();
        final Shell shell = new Shell(display);

        shell.setBounds(0, 0, 400, 200);
        shell.setLayout(new FillLayout());
        shell.setText("Spinner");

        final Spinner spinner = new Spinner(shell, SWT.NONE);
        spinner.setMaximum(1000);
        spinner.setToolTipText("Tooltype test");

        spinner.addSpinnerListener(new ISpinnerListener() {

            @Override
            public void valueChanged(EventObject event) {
                System.out.println("value=" + spinner.getValue());
            }
        });

        // shell.pack();
        shell.open();

        while (!shell.isDisposed()) {
            if (!display.readAndDispatch()) {
                display.sleep();
            }
        }

        display.dispose();
        System.exit(0);
    }




}
