package fr.soleil.comete.swt;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.widgets.Composite;

import fr.soleil.comete.definition.widget.IComponent;
import fr.soleil.comete.definition.widget.IScrollPane;

public class ScrollPane extends CometeComposite<ScrolledComposite> implements IScrollPane {

    public ScrollPane(Composite parent, int childStyle) {
        super(parent, childStyle);
    }

    @Override
    protected ScrolledComposite initControl(int childStyle) {
        return new ScrolledComposite(this, SWT.H_SCROLL | SWT.V_SCROLL);
    }

    @Override
    public void setHorizontalAlignment(int halign) {
        // Nothing to do

    }

    @Override
    public int getHorizontalAlignment() {
        return 0;
    }

    @Override
    public void setView(IComponent component) {
        // Nothing to do in SWT composite constructor always need parent

    }


}
