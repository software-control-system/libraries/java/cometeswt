package fr.soleil.comete.swt;

import javax.activation.UnsupportedDataTypeException;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

import fr.soleil.data.container.matrix.BooleanMatrix;
import fr.soleil.data.target.matrix.IBooleanMatrixTarget;

public class BooleanMatrixTable extends AbstractMatrixTable<Boolean> implements IBooleanMatrixTarget {


    public BooleanMatrixTable(Composite parent, int childStyle) {
        super(parent, childStyle);
    }

    @Override
    public boolean[][] getBooleanMatrix() {
        boolean[][] result = null;
        if (matrix != null) {
            Object[] value = matrix.getValue();
            if (value instanceof boolean[][]) {
                result = (boolean[][]) value;
            }
            else if (value instanceof Boolean[][]) {
                Boolean[][] bValue = (Boolean[][]) value;
                result = new boolean[matrix.getHeight()][matrix.getWidth()];
                for (int row = 0; row < result.length; row++) {
                    for (int col = 0; col < result[row].length; col++) {
                        result[row][col] = (bValue[row][col] == null ? false : bValue[row][col]);
                    }
                }
            }
        }
        return result;
    }

    @Override
    public void setBooleanMatrix(boolean[][] value) {
        BooleanMatrix bMatrix = new BooleanMatrix(Boolean.TYPE);
        try {
            bMatrix.setValue(value);
            setData(bMatrix);
        }
        catch (UnsupportedDataTypeException e) {
            // Should not happen
            e.printStackTrace();
        }
    }

    @Override
    public boolean[] getFlatBooleanMatrix() {
        boolean[] result = null;
        if (matrix != null) {
            Object flatValue = matrix.getFlatValue();
            if (flatValue instanceof boolean[]) {
                result = (boolean[]) flatValue;
            }
            else if (flatValue instanceof Boolean[]) {
                Boolean[] bValue = (Boolean[]) flatValue;
                result = new boolean[bValue.length];
                for (int i = 0; i < bValue.length; i++) {
                    result[i] = (bValue[i] == null ? false : bValue[i]);
                }
            }
        }
        return result;
    }

    @Override
    public void setFlatBooleanMatrix(boolean[] value, int width, int height) {
        BooleanMatrix bMatrix = new BooleanMatrix(Boolean.TYPE);
        try {
            bMatrix.setFlatValue(value, height, width);
            setData(bMatrix);
        }
        catch (UnsupportedDataTypeException e) {
            // Should not happen
            e.printStackTrace();
        }
    }

    /**
     * @param args
     */
    public static void main(String[] args) {
        final Display display = new Display();
        final Shell shell = new Shell(display);

        shell.setBounds(0, 0, 990, 510);
        shell.setLayout(new FillLayout());
        shell.setText("BooleanMatrixTable");

        BooleanMatrixTable booleanTable = new BooleanMatrixTable(shell, SWT.NONE);

        boolean[][] value = new boolean[][] { { true, false, true, false }, { false, false, true, false },
                { true, true, false, true } };
        booleanTable.setBooleanMatrix(value);

        // shell.pack();
        shell.open();

        while (!shell.isDisposed()) {
            if (!display.readAndDispatch()) {
                display.sleep();
            }
        }

        display.dispose();
        System.exit(0);
    }

}
