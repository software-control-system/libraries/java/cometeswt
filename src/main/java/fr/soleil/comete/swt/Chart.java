package fr.soleil.comete.swt;

import java.awt.Color;
import java.awt.Frame;
import java.awt.event.MouseWheelEvent;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JPanel;

import org.eclipse.swt.SWT;
import org.eclipse.swt.awt.SWT_AWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseWheelListener;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.jfree.chart.ChartMouseEvent;
import org.jfree.chart.ChartMouseListener;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.entity.ChartEntity;
import org.jfree.chart.entity.XYItemEntity;
import org.jfree.data.general.Dataset;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import fr.soleil.comete.definition.data.target.complex.IDataArrayTarget;
import fr.soleil.comete.definition.event.ChartViewerEvent;
import fr.soleil.comete.definition.listener.IChartViewerListener;
import fr.soleil.comete.definition.widget.IChartViewer;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.comete.swt.util.ColorTool;
import fr.soleil.comete.swt.viewers.utils.IPlot;
import fr.soleil.comete.swt.viewers.utils.PlotFactory;

public class Chart extends CometeComposite<Control> implements IDataArrayTarget {

    private final List<WeakReference<IChartViewerListener>> chartListeners = new ArrayList<WeakReference<IChartViewerListener>>();

    private Map<String, Object> data;

    private IPlot plot;
    private Frame frame;
    private MouseWheelListener mouseWheelListener;
    private KeyListener keyListener;
    private ChartMouseListener chartMouseListener;

    public Chart(Composite parent, int childStyle) {
        super(parent, SWT.EMBEDDED, childStyle);

        frame = SWT_AWT.new_Frame(this);
    }

    @Override
    protected Control initControl(int childStyle) {
        return null;
    }

    protected void embedPlot(IPlot plot) {
        if (plot instanceof JPanel) {
            if (frame != null) {
                frame.add((JPanel) plot);
            }
        }
        else {
            throw new IllegalArgumentException("must be a chart plot panel");
        }
    }

    public void setPlot(final IPlot plot) {
        final IPlot oldPlot = this.plot;
        // if (oldPlot != null) {
        // oldPlot.cleanUp();
        // frame.remove((JPanel) oldPlot);
        // }
        this.plot = plot;
        final Composite composite = this;
        Display display = Display.getCurrent();
        if (display == null) {
            display = Display.getDefault();
        }
        if (!isDisposed()) {
            embedPlot(plot);
            removeListeners(oldPlot);
            addListeners();
            composite.pack();
            composite.getParent().layout(true, true);
        }
    }

    private void removeListeners(IPlot plot) {
        if (plot != null) {
            removeMouseWheelListener(mouseWheelListener);
            removeKeyListener(keyListener);
            plot.removeChartMouseListener(chartMouseListener);
        }
    }

    private void addListeners() {
        if (plot != null) {
            mouseWheelListener = new MouseWheelListener() {

                @Override
                public void mouseScrolled(MouseEvent event) {
                    JPanel panel = null;
                    if (plot instanceof JPanel) {
                        panel = (JPanel) plot;
                    }
                    MouseWheelEvent awtEvent = org.gumtree.vis.listener.SWT_AWT.toMouseWheelEvent(
                            event, panel);
                    plot.processMouseWheelEvent(awtEvent);
                }
            };
            addMouseWheelListener(mouseWheelListener);

            keyListener = new KeyListener() {

                boolean keyPressed = false;

                @Override
                public void keyReleased(KeyEvent event) {
                    switch (event.keyCode) {
                        case SWT.DEL:
                            plot.removeSelectedMask();
                            break;
                        default:
                            break;
                    }
                    switch (event.character) {
                        default:
                            break;
                    }
                    keyPressed = false;
                }

                @Override
                public void keyPressed(KeyEvent event) {
                    switch (event.keyCode) {
                        case SWT.ARROW_UP:
                            plot.moveSelectedMask(event.keyCode);
                            break;
                        case SWT.ARROW_LEFT:
                            plot.moveSelectedMask(event.keyCode);
                            break;
                        case SWT.ARROW_RIGHT:
                            plot.moveSelectedMask(event.keyCode);
                            break;
                        case SWT.ARROW_DOWN:
                            plot.moveSelectedMask(event.keyCode);
                            break;
                        default:
                            break;
                    }
                    switch (event.stateMask) {
                        case SWT.CTRL:
                            if ((event.keyCode == 'c') || (event.keyCode == 'C')) {
                                if (!keyPressed) {
                                    plot.doCopy();
                                }
                            }
                            else if ((event.keyCode == 'z') || (event.keyCode == 'Z')
                                    || (event.keyCode == 'r') || (event.keyCode == 'R')) {
                                if (!keyPressed) {
                                    plot.restoreAutoBounds();
                                }
                            }
                            else if ((event.keyCode == 'p') || (event.keyCode == 'P')) {
                                System.out.println("p pressed");
                                if (!keyPressed) {
                                    Thread newThread = new Thread(new Runnable() {

                                        @Override
                                        public void run() {
                                            plot.createChartPrintJob();
                                        }
                                    });
                                    newThread.start();
                                }
                            }
                            else if ((event.keyCode == 'e') || (event.keyCode == 'E')) {
                                System.out.println("s pressed");
                                if (!keyPressed) {
                                    Thread newThread = new Thread(new Runnable() {

                                        @Override
                                        public void run() {
                                            try {
                                                plot.doSaveAs();
                                            }
                                            catch (IOException e) {
                                                handleException(e);
                                            }
                                        }
                                    });
                                    newThread.start();
                                }
                            }
                            keyPressed = true;
                            break;
                        default:
                            break;
                    }
                }
            };
            addKeyListener(keyListener);

            final Composite composite = this;

            chartMouseListener = new ChartMouseListener() {

                @Override
                public void chartMouseMoved(ChartMouseEvent event) {

                }

                @Override
                public void chartMouseClicked(ChartMouseEvent event) {
                    Display.getDefault().asyncExec(new Runnable() {

                        @Override
                        public void run() {
                            if (!composite.isFocusControl()) {
                                composite.setFocus();
                            }
                        }
                    });

                    transfertMouseEvent(event);
                }
            };
            plot.addChartMouseListener(chartMouseListener);
        }
    }

    public JFreeChart getChart() {
        if (plot != null) {
            return plot.getChart();
        }
        else {
            return null;
        }
    }

    @Override
    public void setVisible(boolean visible) {
        if (plot != null) {
            plot.setVisible(visible);
        }
        super.setVisible(visible);
    }

    public void setDataset(Dataset dataset) {
        try {
            if (this.plot == null) {
                IPlot newPlot = null;
                if (dataset instanceof XYDataset) {
                    newPlot = PlotFactory.createPlot1DPanel((XYDataset) dataset);
                }
                setPlot(newPlot);
            }
            else {
                plot.cleanUp();
                plot.setDataset(dataset);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Dataset getDataset() {
        return getPlot().getDataset();
    }

    public IPlot getPlot() {
        return plot;
    }

    @Override
    public void redraw() {
        plot.repaint();
        super.redraw();
    }

    public void handleException(final Exception e) {
        Display display = Display.getCurrent();
        if (display == null) {
            display = Display.getDefault();
        }
        display.asyncExec(new Runnable() {
            @Override
            public void run() {
                if (getShell() != null) {
                    MessageBox messageBox = new MessageBox(getShell(), SWT.ICON_WARNING | SWT.OK);
                    messageBox.setText("Failed to Save");
                    messageBox.setMessage("failed : " + e.getMessage());
                    messageBox.open();
                    // MessageDialog.openError(getShell(), "Failed to Save", "failed to save " +
                    // "the image: " + e.getMessage());

                }
            }
        });
    }

    @Override
    public void dispose() {
        if (!isDisposed()) {
            if (mouseWheelListener != null) {
                removeMouseWheelListener(mouseWheelListener);
                mouseWheelListener = null;
            }
            if (keyListener != null) {
                removeKeyListener(keyListener);
                keyListener = null;
            }
            if (plot != null) {
                plot.cleanUp();
                if (chartMouseListener != null) {
                    plot.removeChartMouseListener(chartMouseListener);
                    chartMouseListener = null;
                }
                frame.remove((JPanel) plot);
                frame.dispose();
                plot = null;
            }
        }
        frame = null;
        super.dispose();
    }

    public void addChartViewerListener(IChartViewerListener listener) {
        if (listener != null) {
            synchronized (chartListeners) {
                boolean canAdd = true;
                List<WeakReference<IChartViewerListener>> toRemove = new ArrayList<WeakReference<IChartViewerListener>>();
                for (WeakReference<IChartViewerListener> ref : chartListeners) {
                    IChartViewerListener chartListener = ref.get();
                    if (chartListener == null) {
                        toRemove.add(ref);
                    }
                    else if (chartListener.equals(listener)) {
                        canAdd = false;
                    }
                }
                chartListeners.removeAll(toRemove);
                toRemove.clear();
                if (canAdd) {
                    chartListeners.add(new WeakReference<IChartViewerListener>(listener));
                }
            }
        }
    }

    public void removeChartViewerListener(IChartViewerListener listener) {
        if (listener != null) {
            synchronized (chartListeners) {
                List<WeakReference<IChartViewerListener>> toRemove = new ArrayList<WeakReference<IChartViewerListener>>();
                for (WeakReference<IChartViewerListener> ref : chartListeners) {
                    IChartViewerListener chartListener = ref.get();
                    if ((chartListener == null) || chartListener.equals(listener)) {
                        toRemove.add(ref);
                    }
                }
                chartListeners.removeAll(toRemove);
                toRemove.clear();
            }
        }
    }

    protected void fireChartViewerChanged(ChartViewerEvent event) {
        if (event != null) {
            synchronized (chartListeners) {
                List<WeakReference<IChartViewerListener>> toRemove = new ArrayList<WeakReference<IChartViewerListener>>();
                for (WeakReference<IChartViewerListener> ref : chartListeners) {
                    IChartViewerListener chartListener = ref.get();
                    if (chartListener == null) {
                        toRemove.add(ref);
                    }
                    else {
                        chartListener.chartViewerChanged(event);
                    }
                }
                chartListeners.removeAll(toRemove);
                toRemove.clear();
            }
        }
    }

    private void transfertMouseEvent(ChartMouseEvent event) {
        ChartEntity entity = event.getEntity();
        if ((entity != null) && (entity instanceof XYItemEntity)) {
            // XYItemEntity ent = (XYItemEntity) entity;

            // int sindex = ent.getSeriesIndex();
            // int iindex = ent.getItem();
            // XYDataset dataset = ent.getDataset();

            // double x = dataset.getXValue(sindex, iindex);
            // double y = dataset.getYValue(sindex, iindex);

            // IChartViewer source = this;
            // IChartViewer source = null;
            // double[] point = new double[] { x, y };
            // int index = -1; // ?
            // String viewId = null; // ?
            // Map<String, Object> concernedData = null;
            // Reason reason = Reason.SELECTION;
            //
            // System.out.println("(x= " + x + ", y= " + y + ")");

            // ChartViewerEvent chartEvent = new ChartViewerEvent(source, point, index, viewId,
            // concernedData, reason);
            // fireChartViewerChanged(chartEvent);
        }
    }

    /**
     * Remove all data views and reset the default axis and chart configuration
     */
    public void resetAll() {
        // TODO � tester
        setData(new HashMap<String, Object>());
        plot.cleanUp();
    }

    public void setAxisName(String name, int axis) {
        if (plot != null) {
            switch (axis) {
                case IChartViewer.Y1:
                    ((ChartPanel) plot).getChart().getXYPlot().getRangeAxis().setLabel(name);
                    break;
                default:
                    ((ChartPanel) plot).getChart().getXYPlot().getDomainAxis().setLabel(name);
                    break;
            }
        }
    }

    @Override
    public Map<String, Object> getData() {
        return data;
    }

    @Override
    public void setData(Map<String, Object> data) {
        this.data = data;
        XYDataset dataset = convertToDataset(data);
        setDataset(dataset);
    }

    @Override
    public void addData(Map<String, Object> dataToAdd) {
        if (data == null) {
            data = new LinkedHashMap<String, Object>();
        }
        if (dataToAdd != null) {
            data.putAll(dataToAdd);
        }
        // System.out.println("data size = " + data.size());
        setData(data);
    }

    @Override
    public void removeData(Collection<String> idsToRemove) {
        if (data != null) {
            // TODO � v�rifier, sinon utiliser un Iterator
            for (String id : idsToRemove) {
                data.remove(id);
            }
        }
        setData(data);

    }

    private XYDataset convertToDataset(Map<String, Object> data) {

        XYSeriesCollection dataset = new XYSeriesCollection();
        if (data != null) {
            for (Map.Entry<String, Object> entry : data.entrySet()) {
                String key = entry.getKey();
                Object value = entry.getValue();

                if (value != null) {
                    double[] xValue = null;
                    double[] yValue = null;

                    if (value instanceof double[]) {
                        double[] doubleValues = (double[]) value;
                        if ((doubleValues.length > 0) && (doubleValues.length % 2 == 0)) {
                            int halfLenght = doubleValues.length / 2;
                            xValue = new double[halfLenght];
                            yValue = new double[halfLenght];
                            for (int i = 0; i < halfLenght; i++) {
                                xValue[i] = doubleValues[2 * i];
                                yValue[i] = doubleValues[2 * i + 1];
                            }
                        }
                    }
                    else if (value instanceof double[][]) {
                        double[][] doubleValues = (double[][]) value;
                        if ((doubleValues.length == 2) && (doubleValues[0] != null)
                                && (doubleValues[1] != null)
                                && (doubleValues[0].length == doubleValues[1].length)) {
                            xValue = doubleValues[0];
                            yValue = doubleValues[1];
                        }
                    }

                    if ((xValue != null) && (yValue != null)) {
                        final XYSeries series = new XYSeries(key);
                        // System.out.println("xValue = " + Arrays.toString(xValue));
                        // System.out.println("yValue = " + Arrays.toString(yValue));
                        for (int i = 0; i < xValue.length; i++) {
                            series.add(xValue[i], yValue[i]);
                        }
                        dataset.addSeries(series);
                    }
                }
            }
        }

        return dataset;
    }

    @Override
    public void setCometeBackground(CometeColor color) {
        super.setCometeBackground(color);

        if (plot != null) {
            CometeColor cometeColor = ColorTool.getCometeColor(background);
            Color colorAwt = fr.soleil.comete.awt.util.ColorTool.getColor(cometeColor);
            // color around curves' area
            plot.setBackgroundColor(colorAwt);
            // color under curves
            plot.getXYPlot().setBackgroundPaint(colorAwt);
        }
    }

    @Override
    public void setHorizontalAlignment(int halign) {
        // TODO Auto-generated method stub

    }

    @Override
    public int getHorizontalAlignment() {
        // TODO Auto-generated method stub
        return 0;
    }

    /**
     * @param args
     */
    public static void main(String[] args) {
        final Display display = new Display();
        final Shell shell = new Shell(display);

        shell.setBounds(0, 0, 990, 510);
        shell.setLayout(new FillLayout());
        shell.setText("ChartViewer");

        Chart chartViewer = new Chart(shell, SWT.NONE);

        // mixed x and y values
        double[] t1 = new double[] { 1, 5, 2, 9, 3, 12, 4, 0, 5, 23, 6, 15, 7, 10, 8, 19 };
        double[] t2 = new double[] { 1, 6, 2, -3, 3, 0, 4, 7, 5, 54, 6, 12, 7, 34, 8, 32, 9, 31,
                10, 23 };
        Map<String, Object> data = new HashMap<String, Object>();
        data.put("first", t1);
        data.put("second", t2);
        chartViewer.setData(data);

        // shell.pack();
        shell.open();

        while (!shell.isDisposed()) {
            if (!display.readAndDispatch()) {
                display.sleep();
            }
        }

        display.dispose();
        System.exit(0);
    }


}
