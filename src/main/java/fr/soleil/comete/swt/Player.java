package fr.soleil.comete.swt;

import java.util.ArrayList;
import java.util.EventObject;
import java.util.List;
import java.util.ListIterator;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

import fr.soleil.comete.definition.event.PlayerEvent;
import fr.soleil.comete.definition.listener.IComboBoxListener;
import fr.soleil.comete.definition.listener.IPlayerListener;
import fr.soleil.comete.definition.listener.ISnapshotListener;
import fr.soleil.comete.definition.listener.ITextFieldListener;
import fr.soleil.comete.definition.widget.IPlayer;
import fr.soleil.comete.definition.widget.ISlider;
import fr.soleil.comete.definition.widget.util.IPlayerAnimationBehavior;
import fr.soleil.comete.swt.util.SWTUtils;

public class Player extends CometeComposite<Control> implements IPlayer {

    private final List<IPlayerListener> listeners = new ArrayList<IPlayerListener>();

    // GUI Elements
    private ComboBox layerCombo;
    private int selectedIndex = 0;
    private Label layerLabel;
    private Button playButton;
    private Button stopButton;
    private Button repeatButton;
    private Button generateAnimationButton;
    private IPlayerAnimationBehavior animationBehavior;
    private TextField periodText;
    private boolean play = false;

    private IComboBoxListener comboListener;
    private int periodInMs = 100;
    private boolean loop = false;

    public Player(Composite parent, int childStyle) {
        super(parent, childStyle);
    }

    @Override
    protected Control initControl(int childStyle) {
        setLayout(new RowLayout(SWT.HORIZONTAL));

        Button layerBackwardButton = new Button(this, SWT.PUSH);
        layerCombo = new ComboBox(this, SWT.NONE);
        layerLabel = new Label(this, SWT.NONE);
        layerLabel.setText(" / " + layerCombo.getItemCount());
        Button layerForewardButton = new Button(this, SWT.PUSH);
        playButton = new Button(this, SWT.PUSH);
        playButton.setText("Play");
        stopButton = new Button(this, SWT.PUSH);
        stopButton.setText("Stop");
        repeatButton = new Button(this, SWT.TOGGLE);
        repeatButton.setText("Loop");
        generateAnimationButton = new Button(this, SWT.PUSH);
        generateAnimationButton.setText("Generate Animation");
        generateAnimationButton.setVisible(false);
        periodText = new TextField(this, SWT.NONE);
        periodText.setText(" 100");
        periodText.setToolTipText("Set the period in ms");
        Label label = new Label(this, SWT.NONE);
        label.setText("ms");

        layerBackwardButton.setText("<");
        layerBackwardButton.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                previous();
            }
        });

        comboListener = new IComboBoxListener() {
            @Override
            public void selectedItemChanged(EventObject event) {
                selectedIndex = layerCombo.getSelectedIndex();
                fireIndexChanged(new PlayerEvent(Player.this));
            }
        };
        layerCombo.addComboBoxListener(comboListener);

        layerForewardButton.setText(">");
        layerForewardButton.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                next();
            }
        });

        playButton.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                play();
            }
        });

        stopButton.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                stop();
            }
        });

        final IPlayer player = this;
        generateAnimationButton.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                if (animationBehavior != null) {
                    animationBehavior.generateAnimation(player, null);
                }
            }
        });

        repeatButton.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                loop = repeatButton.getSelection();
            }
        });

        periodText.addTextFieldListener(new ITextFieldListener() {
            @Override
            public void textChanged(EventObject event) {
                setPeriod(periodText.getText());
            }

            @Override
            public void actionPerformed(EventObject event) {
                // TODO Auto-generated method stub

            }
        });

        return null;
    }

    @Override
    public void play() {
        if (!play) {
            play = true;
            stopButton.setEnabled(true);
            playButton.setEnabled(false);

            (new Thread() {
                @Override
                public void run() {
                    while (play) {
                        // System.out.println("play = " + play);
                        // System.out.println("loop = " + loop);
                        if (selectedIndex < getMaximum()) {
                            next();
                        }
                        else {
                            if (!loop) {
                                play = false;
                                SWTUtils.syncExec(new Runnable() {
                                    public void run() {
                                        playButton.setEnabled(true);
                                        stopButton.setEnabled(false);
                                    };
                                });
                            }
                            gotoFirst();
                        }
                        try {
                            Thread.sleep(periodInMs);
                        }
                        catch (InterruptedException e) {
                            play = false;
                            e.printStackTrace();
                        }
                    }
                }
            }).start();
        }
    }

    @Override
    public void stop() {
        play = false;
        playButton.setEnabled(true);
        stopButton.setEnabled(false);
    }

    /**
     * Setting period
     */
    public void setPeriod(String newValue) {
        try {
            int newPeriod = Integer.parseInt(newValue);
            setPeriodInMs(newPeriod);
        }
        catch (Exception e) {
            // nothing to do: ignore exception
        }
    }

    @Override
    public void addPlayerListener(IPlayerListener listener) {
        if (!listeners.contains(listener)) {
            listeners.add(listener);
        }
    }

    @Override
    public void removePlayerListener(IPlayerListener listener) {
        if (listeners.contains(listener)) {
            synchronized (listeners) {
                listeners.remove(listener);
            }
        }
    }

    @Override
    public void fireIndexChanged(PlayerEvent event) {
        ListIterator<IPlayerListener> iterator = listeners.listIterator();
        while (iterator.hasNext()) {
            iterator.next().indexChanged(event);
        }
    }

    @Override
    public int getIndex() {
        return selectedIndex;
    }

    @Override
    public void initSlider(int stackSize) {
        String[] values = new String[stackSize];
        for (int i = 0; i < stackSize; i++) {
            values[i] = String.valueOf(i);
        }
        layerCombo.setValueList(values);
        layerLabel.setText(" / " + (layerCombo.getItemCount() - 1));
        fireIndexChanged(new PlayerEvent(Player.this));
    }

    @Override
    public void setHorizontalAlignment(int halign) {
        // TODO Auto-generated method stub

    }

    @Override
    public int getHorizontalAlignment() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public ISlider getSlider() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setIndex(int index) {
        layerCombo.setSelectedIndex(index);
    }

    @Override
    public int getPeriodInMs() {
        return periodInMs;
    }

    @Override
    public void setPeriodInMs(int periodInMs) {
        this.periodInMs = periodInMs;
    }

    @Override
    public void gotoFirst() {
        selectedIndex = 0;
        if (layerCombo.getItemCount() > 0) {
            layerCombo.setSelectedIndex(selectedIndex);
        }
        fireIndexChanged(new PlayerEvent(Player.this));
    }

    @Override
    public void gotoLast() {
        selectedIndex = layerCombo.getItemCount() - 1;
        if (layerCombo.getItemCount() > 0) {
            layerCombo.setSelectedIndex(selectedIndex);
        }
        fireIndexChanged(new PlayerEvent(Player.this));
    }

    @Override
    public int getMaximum() {
        return layerCombo.getItemCount() - 1;
    }

    @Override
    public void setPlayerAnimationBehavior(IPlayerAnimationBehavior behavior) {
        animationBehavior = behavior;
    }

    @Override
    public IPlayerAnimationBehavior getPlayerAnimationBehavior() {
        return animationBehavior;
    }

    @Override
    public void setAnimationButtonVisible(boolean visible) {
        generateAnimationButton.setVisible(visible);
    }

    @Override
    public boolean isAnimationButtonVisible() {
        return generateAnimationButton.isVisible();
    }

    @Override
    public void previous() {
        if (selectedIndex == 0) {
            selectedIndex = layerCombo.getItemCount();
        }
        if (selectedIndex > 0) {
            selectedIndex = selectedIndex - 1;
            layerCombo.setSelectedIndex(selectedIndex);
            fireIndexChanged(new PlayerEvent(Player.this));
        }

    }

    @Override
    public void next() {
        if (selectedIndex > layerCombo.getItemCount()) {
            selectedIndex = -1;
        }
        selectedIndex = selectedIndex + 1;
        layerCombo.setSelectedIndex(selectedIndex);
        fireIndexChanged(new PlayerEvent(Player.this));
    }

    @Override
    public boolean isRepeatActivated() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void setRepeatActivated(boolean activated) {
        // TODO Auto-generated method stub
    }

    @Override
    public void dispose() {
        listeners.clear();
        super.dispose();
    }

    @Override
    public String getSnapshotDirectory() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setSnapshotDirectory(String snapshotDirectory) {
        // TODO Auto-generated method stub

    }

    @Override
    public String getSnapshotFile() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void addSnapshotListener(ISnapshotListener listener) {
        // TODO Auto-generated method stub

    }

    @Override
    public void removeSnapshotListener(ISnapshotListener listener) {
        // TODO Auto-generated method stub

    }

    public static void main(String[] args) {
        final Display display = new Display();
        final Shell shell = new Shell(display);

        shell.setBounds(0, 0, 400, 200);
        shell.setLayout(new FillLayout());
        shell.setText("Player");

        Player player = new Player(shell, SWT.NONE);
        player.initSlider(10);

        // shell.pack();
        shell.open();

        while (!shell.isDisposed()) {
            if (!display.readAndDispatch()) {
                display.sleep();
            }
        }

        display.dispose();
        System.exit(0);
    }

}
