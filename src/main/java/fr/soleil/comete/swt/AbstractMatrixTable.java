package fr.soleil.comete.swt;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.IllegalFormatException;
import java.util.List;
import java.util.ListIterator;
import java.util.Locale;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

import fr.soleil.comete.definition.data.target.scalar.IEditableComponent;
import fr.soleil.comete.definition.listener.ITableListener;
import fr.soleil.comete.definition.widget.ITable;
import fr.soleil.comete.swt.util.SWTUtils;
import fr.soleil.data.container.matrix.AbstractMatrix;
import fr.soleil.data.target.matrix.IMatrixTarget;
import fr.soleil.lib.project.Format;

public class AbstractMatrixTable<T> extends CometeComposite<org.eclipse.swt.widgets.Table> implements ITable<Object>,
IEditableComponent, IMatrixTarget {

    protected AbstractMatrix<T> matrix;

    protected String format = null;
    protected final List<ITableListener<Object>> tableListeners = new ArrayList<ITableListener<Object>>();

    public AbstractMatrixTable(Composite parent, int childStyle) {
        super(parent, childStyle);
    }

    @Override
    protected org.eclipse.swt.widgets.Table initControl(int childStyle) {
        org.eclipse.swt.widgets.Table table = new org.eclipse.swt.widgets.Table(this, SWT.BORDER | SWT.V_SCROLL
                | SWT.H_SCROLL | SWT.FULL_SELECTION | childStyle);
        table.setHeaderVisible(true);
        table.setLinesVisible(true);
        return table;
    }

    @Override
    public int getHorizontalAlignment() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public void setHorizontalAlignment(int arg0) {
        // TODO Auto-generated method stub
    }

    @Override
    public String getFormat() {
        return format;
    }

    @Override
    public void setFormat(final String sFormat) {
        if (getControl() != null) {
            SWTUtils.asyncExec(new Runnable() {
                @Override
                public void run() {

                    if (!getControl().isDisposed()) {
                        if (sFormat == null || Format.isNumberFormatOK(sFormat)) {
                            format = sFormat;

                            getControl().setRedraw(false);
                            setDisplayedValues(false);
                            getControl().setRedraw(true);
                        }
                    }
                }
            });
        }
    }

    private String formatData(Number data) {
        String result = null;
        if (data != null) {
            if ((format == null) || (format.trim().isEmpty())) {
                result = data.toString();
            }
            else {
                try {
                    result = Format.format(Locale.US, format, data);
                }
                catch (IllegalFormatException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }
        return result;
    }

    @Override
    public void addTableListener(ITableListener<Object> listener) {
        if (!tableListeners.contains(listener)) {
            tableListeners.add(listener);
        }
    }

    @Override
    public void fireValueChanged(Object amatrix) {
        ListIterator<ITableListener<Object>> iterator = tableListeners.listIterator();
        while (iterator.hasNext()) {
            ITableListener<Object> listener = iterator.next();
            listener.valueChanged(amatrix);
        }
    }

    @Override
    public String[] getCustomColumnNames() {
        // not todo
        return null;
    }

    @Override
    public String[] getCustomRowNames() {
        // not todo
        return null;
    }

    @Override
    public boolean isEditable() {
        return false;
    }

    @Override
    public void refresh() {
        // not todo

    }

    @Override
    public void removeTableListener(ITableListener<Object> listener) {
        if (tableListeners.contains(listener)) {
            synchronized (tableListeners) {
                tableListeners.remove(listener);
            }
        }

    }

    @Override
    public void setCustomColumnNames(String[] arg0) {
        // not todo

    }

    @Override
    public void setCustomRowNames(String[] arg0) {
        // not todo

    }

    @Override
    public Object getData() {
        return matrix;
    }

    @Override
    public void setData(final Object amatrix) {
        if (getControl() != null) {
            SWTUtils.asyncExec(new Runnable() {
                @Override
                public void run() {
                    if (!getControl().isDisposed()) {
                        getControl().setRedraw(false);
                        // Clean the table
                        getControl().removeAll();
                        for (TableColumn column : getControl().getColumns()) {
                            column.dispose();
                        }
                        if (amatrix != null && amatrix instanceof AbstractMatrix<?>) {
                            // Set matrix data
                            AbstractMatrixTable.this.matrix = (AbstractMatrix<T>) amatrix;
                            // Column name
                            for (int i = 0; i < matrix.getWidth(); i++) {
                                TableColumn column = new TableColumn(getControl(), SWT.NONE);
                                column.setText(String.valueOf(i));
                                column.setMoveable(true);
                            }
                            setDisplayedValues(true);
                        }
                        getControl().setRedraw(true);
                        fireValueChanged(matrix);
                    }
                }
            });
        }
    }

    private void setDisplayedValues(final boolean create) {

        if (getControl() != null) {
            SWTUtils.asyncExec(new Runnable() {

                @Override
                public void run() {
                    if (!getControl().isDisposed()) {
                        if (!create) {
                            getControl().clearAll();
                        }
                        TableItem item = null;
                        for (int j = 0; j < matrix.getHeight(); j++) {
                            if (create) {
                                item = new TableItem(getControl(), SWT.NONE);
                            }
                            else {
                                item = getControl().getItem(j);
                            }
                            for (int i = 0; i < matrix.getWidth(); i++) {
                                T valueAt = matrix.getValueAt(j, i);
                                if (valueAt instanceof Number) {
                                    String formatedData = formatData((Number) valueAt);
                                    item.setText(i, (formatedData == null ? "" : formatedData));
                                }
                                else {
                                    item.setText(i, valueAt.toString());
                                }
                            }
                        }
                        // pack all columns
                        for (int i = 0; i < matrix.getWidth(); i++) {
                            TableColumn column = getControl().getColumn(i);
                            column.pack();
                        }
                    }
                }

            });
        }
    }

    @Override
    public boolean isPreferFlatValues(Class<?> concernedDataClass) {
        return false;
    }

    @Override
    public int getMatrixDataWidth(Class<?> concernedDataClass) {
        int width = 0;
        if (matrix.getType().isAssignableFrom(concernedDataClass)) {
            Object[] value = matrix.getValue();
            if ((value != null) && (value.length > 0)) {
                width = Array.getLength(value[0]);
            }
        }
        return width;
    }

    @Override
    public int getMatrixDataHeight(Class<?> concernedDataClass) {
        int height = 0;
        if (matrix.getType().isAssignableFrom(concernedDataClass)) {
            Object[] value = matrix.getValue();
            if (value != null) {
                height = value.length;
            }
        }
        return height;
    }

    @Override
    public void setEditable(boolean editable) {
        // table is not editable
    }

}
