//+============================================================================
//Source: package fr.soleil.comete.widget.swing;/Label.java
//
//project :     Comete
//
//Description: This class hides
//
//Author: SAINTIN
//
//Revision: 1.1
//
//Log:
//
//copyleft :Synchrotron SOLEIL
//			L'Orme des Merisiers
//			Saint-Aubin - BP 48
//			91192 GIF-sur-YVETTE CEDEX
//			FRANCE
//
//+============================================================================
package fr.soleil.comete.swt;

import java.util.ArrayList;
import java.util.EventObject;
import java.util.List;
import java.util.ListIterator;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import fr.soleil.comete.definition.data.information.TextInformation;
import fr.soleil.comete.definition.listener.ITextFieldListener;
import fr.soleil.comete.definition.widget.IComponent;
import fr.soleil.comete.definition.widget.ITextField;
import fr.soleil.comete.swt.util.SWTUtils;
import fr.soleil.data.target.IUnitTarget;

public class TextField extends CometeComposite<Text> implements ITextField, IUnitTarget, KeyListener {

    private String unit;
    protected final List<ITextFieldListener> textFieldListeners = new ArrayList<ITextFieldListener>();
    protected boolean actionPerformedOnTextChanged = false;

    public TextField(Composite parent, int childStyle) {
        super(parent, childStyle);
    }

    @Override
    protected Text initControl(int childStyle) {
        Text text = new Text(this, SWT.LEFT | childStyle);
        text.addKeyListener(this);
        return text;
    }


    @Override
    public void setPreferredSize(int width, int height) {
        super.setSize(width, height);
    }

    @Override
    public void setSize(int width, int height) {
        super.setSize(width, height);
    }

    @Override
    public String getUnit() {
        return this.unit;
    }

    @Override
    public void setUnit(String unit) {
        this.unit = unit;
    }

    /**
     * Returns the text string that the label displays with its unit
     *
     * @return a String
     * @see #setText
     */
    @Override
    public String getText() {
        final String[] result = { null };
        if (getControl() != null) {
            SWTUtils.syncExec(new Runnable() {
                @Override
                public void run() {
                    if (!getControl().isDisposed()) {
                        result[0] = getControl().getText();
                    }
                }
            });
            if (unit != null) {
                result[0] = result[0] + " " + unit;
            }
        }
        return result[0];
    }

    @Override
    public void setText(final String text) {
        if (getControl() != null) {
            final String theText = (text == null ? "" : text);
            SWTUtils.asyncExec(new Runnable() {
                @Override
                public void run() {
                    if (!getControl().isDisposed()) {
                        getControl().setText(theText);

                        EventObject event = new EventObject(TextField.this);
                        fireTextChanged(event);
                    }
                }
            });
        }
    }

    @Override
    public boolean isEditable() {
        final boolean[] editable = { false };
        if (getControl() != null) {
            SWTUtils.syncExec(new Runnable() {
                @Override
                public void run() {
                    if (!getControl().isDisposed()) {
                        editable[0] = getControl().getEditable();
                    }
                }
            });
        }
        return editable[0];
    }

    @Override
    public void setEditable(final boolean editable) {
        if (getControl() != null) {
            SWTUtils.asyncExec(new Runnable() {
                @Override
                public void run() {
                    if (!getControl().isDisposed()) {
                        getControl().setEditable(editable);
                    }
                }
            });
        }
    }

    @Override
    public boolean isEditingData() {
        final boolean[] editing = { false };
        if (getControl() != null) {
            SWTUtils.syncExec(new Runnable() {
                @Override
                public void run() {
                    if (!getControl().isDisposed()) {
                        editing[0] = getControl().getEditable() && getControl().isFocusControl();
                    }
                }
            });
        }
        return editing[0];
    }

    @Override
    public int getHorizontalAlignment() {
        return IComponent.LEFT;
    }

    @Override
    public void setHorizontalAlignment(int arg0) {
        // nop
    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (e.character == SWT.CR) {
            fireActionPerformed(new EventObject(this));
        }
        else if (e.character != '\0') {
            EventObject event = new EventObject(this);
            fireTextChanged(event);
            if (actionPerformedOnTextChanged) {
                fireActionPerformed(event);
            }
        }

    }

    @Override
    public void keyReleased(KeyEvent e) {
    }

    @Override
    public boolean isActionPerformedOnTextChanged() {
        return actionPerformedOnTextChanged;
    }

    @Override
    public void setActionPerformedOnTextChanged(boolean actionPerformedOnTextChanged) {
        this.actionPerformedOnTextChanged = actionPerformedOnTextChanged;

    }

    @Override
    public void addTextFieldListener(final ITextFieldListener listener) {
        if (!textFieldListeners.contains(listener)) {
            textFieldListeners.add(listener);
        }
    }

    @Override
    public void removeTextFieldListener(final ITextFieldListener listener) {
        if (textFieldListeners.contains(listener)) {
            synchronized (textFieldListeners) {
                textFieldListeners.remove(listener);
            }
        }
    }

    @Override
    public void fireTextChanged(final EventObject event) {
        ListIterator<ITextFieldListener> iterator = textFieldListeners.listIterator();
        while (iterator.hasNext()) {
            ITextFieldListener listener = iterator.next();
            listener.textChanged(event);
        }
    }

    @Override
    public void fireActionPerformed(EventObject event) {
        warnMediators(new TextInformation(this, getText()));
        ListIterator<ITextFieldListener> iterator = textFieldListeners.listIterator();
        while (iterator.hasNext()) {
            ITextFieldListener listener = iterator.next();
            listener.textChanged(event);
            listener.actionPerformed(event);
        }

    }

    @Override
    public int getColumns() {
        int columns = 0;
        if (getControl() != null) {
            columns = getControl().getTextLimit();
        }
        return columns;
    }

    @Override
    public void setColumns(final int columns) {
        if (getControl() != null) {
            SWTUtils.asyncExec(new Runnable() {
                @Override
                public void run() {
                    if (!getControl().isDisposed()) {
                        getControl().setTextLimit(columns);
                    }
                }
            });
        }
    }

    public static void main(String[] args) {
        final Display display = new Display();
        final Shell shell = new Shell(display);

        shell.setBounds(0, 0, 400, 200);
        shell.setLayout(new FillLayout());
        shell.setText("TextField");

        TextField textfield = new TextField(shell, SWT.NONE);
        textfield.setText("TextField SWT Test");
        textfield.setToolTipText("Tooltype test");

        // shell.pack();
        shell.open();

        while (!shell.isDisposed()) {
            if (!display.readAndDispatch()) {
                display.sleep();
            }
        }

        display.dispose();
        System.exit(0);
    }

}
