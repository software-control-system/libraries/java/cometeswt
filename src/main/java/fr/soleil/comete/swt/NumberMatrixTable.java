package fr.soleil.comete.swt;

import javax.activation.UnsupportedDataTypeException;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

import fr.soleil.data.container.matrix.AbstractNumberMatrix;
import fr.soleil.data.container.matrix.BasicNumberMatrix;
import fr.soleil.data.container.matrix.util.NumberMatrixGenerator;
import fr.soleil.data.target.matrix.INumberMatrixTarget;

public class NumberMatrixTable<T extends Number> extends AbstractMatrixTable<T> implements INumberMatrixTarget {


    public NumberMatrixTable(Composite parent, int childStyle) {
        super(parent, childStyle);
    }

    @Override
    public Object[] getNumberMatrix() {
        Object[] result = null;
        if (matrix != null) {
            result = matrix.getValue();
        }
        return result;
    }

    @Override
    public void setNumberMatrix(Object[] value) {

        AbstractNumberMatrix<?> result = NumberMatrixGenerator.getNewMatrixFor(value);
        if (result != null) {
            try {
                result.setValue(value);
            }
            catch (UnsupportedDataTypeException e) {
                // Should not happen
                e.printStackTrace();
            }
        }
        setData(result);
    }

    @Override
    public Object getFlatNumberMatrix() {
        Object result = null;
        if (matrix != null) {
            result = matrix.getFlatValue();
        }
        return result;
    }

    @Override
    public void setFlatNumberMatrix(Object value, int width, int height) {
        BasicNumberMatrix result = new BasicNumberMatrix();
        try {
            result.setFlatValue(value, height, width);
        }
        catch (UnsupportedDataTypeException e) {
            // Should not happen
            e.printStackTrace();
        }
        setData(result);
    }

    /**
     * @param args
     */
    public static void main(String[] args) {
        final Display display = new Display();
        final Shell shell = new Shell(display);

        shell.setBounds(0, 0, 990, 510);
        shell.setLayout(new FillLayout());
        shell.setText("NumberMatrixTable");

        NumberMatrixTable<Double> numberTable = new NumberMatrixTable<Double>(shell, SWT.NONE);

        double[][] value = new double[][] { { 0.0, 1.2, 3.6, 4 }, { 5, 6, 7.8, 10 }, { 11, 0.1, 0.5, 10 } };
        numberTable.setNumberMatrix(value);

        // shell.pack();
        shell.open();

        while (!shell.isDisposed()) {
            if (!display.readAndDispatch()) {
                display.sleep();
            }
        }

        display.dispose();
        System.exit(0);
    }

}
