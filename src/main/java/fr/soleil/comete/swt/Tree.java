//+============================================================================
//Source: package fr.soleil.comete.widget.swing;/Tree.java
//
//project :     Comete
//
//Description: This class hides
//
//Author: SAINTIN
//
//Revision: 1.1
//
//Log:
//
//copyleft :Synchrotron SOLEIL
//          L'Orme des Merisiers
//          Saint-Aubin - BP 48
//          91192 GIF-sur-YVETTE CEDEX
//          FRANCE
//
//+============================================================================
package fr.soleil.comete.swt;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TreeItem;

import fr.soleil.comete.definition.event.TreeNodeSelectionEvent;
import fr.soleil.comete.definition.listener.ITreeNodeSelectionListener;
import fr.soleil.comete.definition.widget.ITree;
import fr.soleil.comete.definition.widget.util.BasicTreeNode;
import fr.soleil.comete.definition.widget.util.ITreeNode;
import fr.soleil.comete.swt.util.SWTUtils;
import fr.soleil.comete.swt.util.TreeNodeTool;

/**
 * CometeSWT implementation of {@link ITree}
 *
 * @author saintin
 */
public class Tree extends CometeComposite<org.eclipse.swt.widgets.Tree> implements ITree, SelectionListener {

    protected final List<WeakReference<ITreeNodeSelectionListener>> selectionListeners;

    protected ITreeNode rootNode;

    protected int selectionMode = ITree.MULTI_SELECTION;

    /**
     * Default constructor
     */
    public Tree(Composite parent, int childStyle) {
        super(parent, ITree.MULTI_SELECTION, childStyle);
        selectionListeners = new ArrayList<WeakReference<ITreeNodeSelectionListener>>();
    }

    public Tree(Composite parent, int selectionType, int childStyle) {
        super(parent, selectionType | childStyle);
        setSelectionMode(selectionType);
        selectionListeners = new ArrayList<WeakReference<ITreeNodeSelectionListener>>();
    }

    @Override
    protected org.eclipse.swt.widgets.Tree initControl(int childStyle) {
        final org.eclipse.swt.widgets.Tree tree = new org.eclipse.swt.widgets.Tree(this, childStyle);
        tree.addSelectionListener(this);
        tree.addListener(SWT.MouseMove, new Listener() {
            public void handleEvent(Event event) {
                ITreeNode treeNode = getTreeNode(event.x, event.y);
                if (treeNode != null) {
                    tree.setToolTipText(treeNode.getToolTip());
                } else {
                    tree.setToolTipText(null);
                }
            }
        });
        return tree;
    }

    private ITreeNode getTreeNode(int x, int y) {
        ITreeNode treeNode = null;
        if ((getControl() != null) && !getControl().isDisposed()) {
            org.eclipse.swt.widgets.Tree tree = getControl();
            Point point = new Point(x, y);
            TreeItem item = tree.getItem(point);
            if (item != null) {
                treeNode = TreeNodeTool.findTreeNodeForTreeItem(item, rootNode);
            }
        }
        return treeNode;
    }

    @Override
    public ITreeNode getRootNode() {
        return rootNode;
    }

    @Override
    public void setRootNode(final ITreeNode aRootNode) {
        if (getControl() != null) {
            SWTUtils.asyncExec(new Runnable() {

                @Override
                public void run() {
                    if (!getControl().isDisposed()) {
                        // remove all nodes to keep a single root, as swt allows multiple roots
                        getControl().removeAll();
                        TreeNodeTool.createRootTreeNode(getControl(), aRootNode);
                        rootNode = aRootNode;
                    }
                }

            });
        }
    }

    public void repaint() {
        if (getControl() != null) {
            SWTUtils.asyncExec(new Runnable() {

                @Override
                public void run() {
                    if (!getControl().isDisposed()) {
                        getControl().redraw();
                    }
                }

            });
        }
    }

    public void expendAll(final boolean expend) {
        if (getControl() != null) {
            SWTUtils.asyncExec(new Runnable() {

                @Override
                public void run() {
                    if (!getControl().isDisposed()) {
                        TreeItem topItem = getControl().getTopItem();
                        if (topItem != null) {
                            topItem.setExpanded(expend);
                        }
                    }
                }
            });
        }
    }

    @Override
    public ITreeNode getNodeAt(final int x, final int y) {
        ITreeNode result = null;
        if (getControl() != null) {
            final ITreeNode[] threadResult = { null };
            SWTUtils.syncExec(new Runnable() {

                @Override
                public void run() {
                    TreeItem item = getControl().getItem(new Point(x, y));
                    threadResult[0] =  TreeNodeTool.findTreeNodeForTreeItem(item, rootNode);
                }

            });
            result = threadResult[0];
        }
        return result;
    }

    @Override
    public void selectNodes(final boolean keepPreviousSelection, final ITreeNode... nodes) {
        if (getControl() != null) {
            SWTUtils.asyncExec(new Runnable() {

                @Override
                public void run() {
                    if (!getControl().isDisposed()) {
                        if (!keepPreviousSelection) {
                            clearSelection();
                        }
                        // get the single root we allow
                        TreeItem rootItem = getControl().getItem(0);
                        // find new items to select
                        Set<TreeItem> itemsToSelect = new HashSet<TreeItem>();
                        for (ITreeNode node : nodes) {
                            TreeItem item = TreeNodeTool.findTreeItemFromTreeNode(node, rootItem);
                            if (item != null) {
                                itemsToSelect.add(item);
                            }
                        }
                        // select only new items
                        for (TreeItem treeItem : itemsToSelect) {
                            getControl().select(treeItem);
                        }
                        TreeNodeSelectionEvent treeNodeSelectionEvent = new TreeNodeSelectionEvent(Tree.this, true,
                                nodes);
                        warnSelectionListeners(treeNodeSelectionEvent);
                    }
                }

            });
        }

    }

    @Override
    public void deselectNodes(final ITreeNode... nodes) {

        if (getControl() != null) {
            SWTUtils.asyncExec(new Runnable() {

                @Override
                public void run() {
                    if (!getControl().isDisposed()) {
                        // get the single root we allow
                        TreeItem rootItem = getControl().getItem(0);
                        // find corresponding items to deselect
                        Set<TreeItem> itemsToDeselect = new HashSet<TreeItem>();
                        for (ITreeNode node : nodes) {
                            TreeItem item = TreeNodeTool.findTreeItemFromTreeNode(node, rootItem);
                            if (item != null) {
                                itemsToDeselect.add(item);
                            }
                        }
                        for (TreeItem treeItem : itemsToDeselect) {
                            getControl().deselect(treeItem);
                        }
                        TreeNodeSelectionEvent treeNodeSelectionEvent = new TreeNodeSelectionEvent(Tree.this, false, nodes);
                        warnSelectionListeners(treeNodeSelectionEvent);
                    }
                }

            });
        }
    }

    @Override
    public void clearSelection() {
        if (getControl() != null) {
            SWTUtils.asyncExec(new Runnable() {

                @Override
                public void run() {
                    if (!getControl().isDisposed()) {
                        // get the currently selected nodes
                        ITreeNode[] selectedNodes = getSelectedNodes();
                        TreeNodeSelectionEvent treeNodeSelectionEvent = new TreeNodeSelectionEvent(Tree.this, false,
                                selectedNodes);
                        getControl().deselectAll();
                        warnSelectionListeners(treeNodeSelectionEvent);
                    }
                }
            });
        }
    }

    @Override
    public ITreeNode[] getSelectedNodes() {

        ITreeNode[] result = null;
        if (getControl() != null) {
            final ITreeNode[][] threadResult = { null };
            SWTUtils.syncExec(new Runnable() {

                @Override
                public void run() {
                    // get the current selection
                    TreeItem[] currentSelection = getControl().getSelection();

                    // find corresponding ITreeNodes
                    List<ITreeNode> selectedNodes = new ArrayList<ITreeNode>(currentSelection.length);
                    for (TreeItem selectedItem : currentSelection) {
                        ITreeNode treeNode = TreeNodeTool.findTreeNodeForTreeItem(selectedItem, rootNode);
                        if (treeNode != null) {
                            selectedNodes.add(treeNode);
                        }
                    }
                    threadResult[0] = selectedNodes.toArray(new ITreeNode[selectedNodes.size()]);
                }

            });
            result = threadResult[0];
        }
        return result;
    }

    @Override
    public void addTreeNodeSelectionListener(ITreeNodeSelectionListener listener) {
        if (listener != null) {
            synchronized (selectionListeners) {
                boolean canAdd = true;
                List<WeakReference<ITreeNodeSelectionListener>> toRemove = new ArrayList<WeakReference<ITreeNodeSelectionListener>>();
                for (WeakReference<ITreeNodeSelectionListener> ref : selectionListeners) {
                    ITreeNodeSelectionListener selectionListener = ref.get();
                    if (selectionListener == null) {
                        toRemove.add(ref);
                    }
                    else if (selectionListener.equals(listener)) {
                        canAdd = false;
                    }
                }
                selectionListeners.removeAll(toRemove);
                toRemove.clear();
                if (canAdd) {
                    selectionListeners.add(new WeakReference<ITreeNodeSelectionListener>(listener));
                }
            }
        }
    }

    @Override
    public boolean hasTreeNodeSelectionListener(ITreeNodeSelectionListener listener) {
        boolean contains = false;
        if (listener != null) {
            synchronized (selectionListeners) {
                List<WeakReference<ITreeNodeSelectionListener>> toRemove = new ArrayList<WeakReference<ITreeNodeSelectionListener>>();
                for (WeakReference<ITreeNodeSelectionListener> ref : selectionListeners) {
                    ITreeNodeSelectionListener selectionListener = ref.get();
                    if (selectionListener == null) {
                        toRemove.add(ref);
                    }
                    else if (selectionListener.equals(listener)) {
                        contains = true;
                    }
                }
                selectionListeners.removeAll(toRemove);
                toRemove.clear();
            }
        }
        return contains;
    }

    @Override
    public void removeTreeNodeSelectionListener(ITreeNodeSelectionListener listener) {
        if (listener != null) {
            synchronized (selectionListeners) {
                List<WeakReference<ITreeNodeSelectionListener>> toRemove = new ArrayList<WeakReference<ITreeNodeSelectionListener>>();
                for (WeakReference<ITreeNodeSelectionListener> ref : selectionListeners) {
                    ITreeNodeSelectionListener selectionListener = ref.get();
                    if ((selectionListener == null) || selectionListener.equals(listener)) {
                        toRemove.add(ref);
                    }
                }
                selectionListeners.removeAll(toRemove);
                toRemove.clear();
            }
        }

    }

    @Override
    public void removeAllTreeNodeSelectionListeners() {
        synchronized (selectionListeners) {
            selectionListeners.clear();
        }
    }

    protected void warnSelectionListeners(TreeNodeSelectionEvent event) {
        if (event != null) {
            synchronized (selectionListeners) {
                List<WeakReference<ITreeNodeSelectionListener>> toRemove = new ArrayList<WeakReference<ITreeNodeSelectionListener>>();
                for (WeakReference<ITreeNodeSelectionListener> ref : selectionListeners) {
                    ITreeNodeSelectionListener selectionListener = ref.get();
                    if (selectionListener == null) {
                        toRemove.add(ref);
                    }
                    else {
                        selectionListener.selectionChanged(event);
                    }
                }
                selectionListeners.removeAll(toRemove);
                toRemove.clear();
            }
        }
    }

    @Override
    public void widgetDefaultSelected(SelectionEvent e) {
        // System.out.println("selection1 " + e.item.toString());

        // TODO warnSelectionListeners
    }

    @Override
    public void widgetSelected(final SelectionEvent e) {
        if (getControl() != null) {
            SWTUtils.asyncExec(new Runnable() {

                @Override
                public void run() {
                    if (!getControl().isDisposed()) {
                        warnSelectionListeners();
                    }
                }

            });
        }
    }

    private void warnSelectionListeners() {
        new TreeNodeSelectionEvent(this, true, getSelectedNodes());
    }

    @Override
    public void setHorizontalAlignment(int halign) {
        // nop

    }

    @Override
    public int getHorizontalAlignment() {
        // nop
        return 0;
    }

    @Override
    public void setSelectionMode(int selectionMode) {
        // In SWT the selection mode is set at construction
        this.selectionMode = selectionMode;
    }

    @Override
    public int getSelectionMode() {
        // In SWT the selection mode is set at construction
        return selectionMode;
    }

    public static void main(final String[] args) {
        final Display display = new Display();
        final Shell shell = new Shell(display);

        shell.setBounds(new Rectangle(0, 0, 300, 400));
        shell.setLayout(new FillLayout(SWT.VERTICAL));

        Tree tree = new Tree(shell, SWT.NONE);

        // ITreeNode root = new BasicTreeNode();
        // root.setName("ROOT");
        // File[] roots = File.listRoots();
        // System.out.println("root length " + roots.length);
        // for (int i = 0; i < roots.length; i++) {
        // ITreeNode child = new BasicTreeNode();
        // child.setName(roots[i].toString());
        // child.setData(roots[i]);
        // root.addNodes(child);
        // }
        //
        // tree.setRootNode(root);

        ITreeNode root2 = new BasicTreeNode();
        root2.setName("ROOT2");

        ITreeNode n1 = new BasicTreeNode();
        n1.setName("n1");
        root2.addNodes(n1);
        ITreeNode n2 = new BasicTreeNode();
        n2.setName("n2");
        root2.addNodes(n2);
        ITreeNode n3 = new BasicTreeNode();
        n3.setName("n3");
        root2.addNodes(n3);
        ITreeNode n31 = new BasicTreeNode();
        n31.setName("n31");
        n3.addNodes(n31);

        tree.setRootNode(root2);

        ITreeNode[] selectedNodes = tree.getSelectedNodes();
        System.out.println("selection =");
        for (ITreeNode iTreeNode : selectedNodes) {
            System.out.println(iTreeNode.getName());
        }
        System.out.println("---- select n1 n2 n31");
        tree.selectNodes(false, n1, n2, n31);
        tree.selectNodes(true, n1, n2, n3);
        selectedNodes = tree.getSelectedNodes();
        System.out.println("selection =");
        for (ITreeNode iTreeNode : selectedNodes) {
            System.out.println(iTreeNode.getName());
        }

        shell.open();

        while (!shell.isDisposed()) {
            if (!display.readAndDispatch()) {
                display.sleep();
            }
        }

        display.dispose();
        System.exit(0);
    }


}
