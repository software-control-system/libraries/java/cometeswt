package fr.soleil.comete.swt;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.EventObject;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseWheelListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

import fr.soleil.comete.definition.data.information.TextInformation;
import fr.soleil.comete.definition.listener.IComboBoxListener;
import fr.soleil.comete.definition.widget.IComboBox;
import fr.soleil.comete.definition.widget.IComponent;
import fr.soleil.comete.swt.util.SWTUtils;

public class ComboBox extends CometeComposite<Combo> implements IComboBox, SelectionListener,
MouseWheelListener {

    private final List<WeakReference<IComboBoxListener>> comboBoxListeners = new ArrayList<WeakReference<IComboBoxListener>>();
    private String[] valueList = null;

    public ComboBox(Composite parent, int childStyle) {
        super(parent, childStyle);
    }

    @Override
    public void mouseScrolled(MouseEvent event) {
        final int count = event.count;
        final int direction = count > 0 ? -1 : 1;
        final int newSelection_ = getControl().getSelectionIndex() + direction;
        final int newSelection;
        if (newSelection_ == -1) {
            newSelection = getControl().getItemCount() - 1;
        }
        else if (newSelection_ == getControl().getItemCount()) {
            newSelection = 0;
        }
        else {
            newSelection = newSelection_;
        }
        getControl().select(newSelection);
    }

    @Override
    protected Combo initControl(int childStyle) {
        Combo combo = new Combo(this, SWT.DROP_DOWN | SWT.READ_ONLY | childStyle);
        combo.addSelectionListener(this);
        // Uncomment to add mouseWheelListener
        // combo.addMouseWheelListener(this);
        return combo;
    }

    @Override
    public void widgetDefaultSelected(SelectionEvent arg0) {
        fireSelectedItemChanged(new EventObject(this));
    }

    @Override
    public void widgetSelected(SelectionEvent arg0) {
        fireSelectedItemChanged(new EventObject(this));
    }

    @Override
    public String getText() {
        // should return the content of the textfield in case of an editable combo
        // here we return the currently displayed item
        final String[] result = { null };
        if (getControl() != null) {
            SWTUtils.syncExec(new Runnable() {
                @Override
                public void run() {
                    if (!getControl().isDisposed()) {
                        int selectionIndex = getControl().getSelectionIndex();
                        if (selectionIndex != -1) {
                            result[0] = getControl().getItem(selectionIndex);
                        }
                    }
                }
            });
        }
        return result[0];
    }

    @Override
    public void setText(final String text) {
        if (getControl() != null) {
            // sets the currently displayed item to text
            SWTUtils.asyncExec(new Runnable() {
                @Override
                public void run() {
                    if (!getControl().isDisposed()) {
                        getControl().deselectAll();
                        if (text != null) {
                            int index = getControl().indexOf(text);
                            if (index != -1) {
                                getControl().select(index);
                            }
                        }
                        fireSelectedItemChanged(new EventObject(this));
                    }
                }
            });
        }
    }

    @Override
    public void setHorizontalAlignment(int halign) {
        // nothing to do
    }

    @Override
    public int getHorizontalAlignment() {
        return IComponent.LEFT;
    }

    @Override
    public void setEditable(boolean b) {
        // nothing to do
    }

    @Override
    public boolean isEditable() {
        return false;
    }

    @Override
    public String[] getStringArray() {
        return getValueList();
    }

    @Override
    public void setStringArray(String[] value) {
        setValueList(value);
    }

    @Override
    public void addComboBoxListener(IComboBoxListener listener) {
        // add listener and clean useless references
        if (listener != null) {
            ArrayList<WeakReference<IComboBoxListener>> toRemove = new ArrayList<WeakReference<IComboBoxListener>>();
            boolean canAdd = true;
            synchronized (comboBoxListeners) {
                for (WeakReference<IComboBoxListener> ref : comboBoxListeners) {
                    IComboBoxListener temp = ref.get();
                    if (temp == null) {
                        toRemove.add(ref);
                    }
                    else if (temp.equals(listener)) {
                        canAdd = false;
                    }
                    temp = null;
                }
                comboBoxListeners.removeAll(toRemove);
                if (canAdd) {
                    comboBoxListeners.add(new WeakReference<IComboBoxListener>(listener));
                }
            }
            toRemove.clear();
        }
    }

    @Override
    public void removeComboBoxListener(IComboBoxListener listener) {
        // remove listener and clean useless references
        if (listener != null) {
            ArrayList<WeakReference<IComboBoxListener>> toRemove = new ArrayList<WeakReference<IComboBoxListener>>();
            synchronized (comboBoxListeners) {
                for (WeakReference<IComboBoxListener> ref : comboBoxListeners) {
                    IComboBoxListener temp = ref.get();
                    if ((temp == null) || temp.equals(listener)) {
                        toRemove.add(ref);
                    }
                }
                comboBoxListeners.removeAll(toRemove);
            }
            toRemove.clear();
        }
    }

    @Override
    public void fireSelectedItemChanged(EventObject event) {
        warnMediators(new TextInformation(this, getText()));
        // fire event and clean useless references
        ArrayList<WeakReference<IComboBoxListener>> toRemove = new ArrayList<WeakReference<IComboBoxListener>>();
        synchronized (comboBoxListeners) {
            for (WeakReference<IComboBoxListener> ref : comboBoxListeners) {
                IComboBoxListener temp = ref.get();
                if (temp == null) {
                    toRemove.add(ref);
                }
                else {
                    temp.selectedItemChanged(event);
                }
            }
            comboBoxListeners.removeAll(toRemove);
        }
        toRemove.clear();
    }

    @Override
    public String[] getDisplayedList() {
        final String[][] result = { null };
        if (getControl() != null) {
            SWTUtils.syncExec(new Runnable() {
                @Override
                public void run() {
                    if (!getControl().isDisposed()) {
                        result[0] = getControl().getItems();
                    }
                }
            });
        }
        return result[0];
    }

    @Override
    public void setDisplayedList(final String[] displayedList) {
        final String selectedValue = getSelectedValue();

        if (getControl() != null) {
            SWTUtils.asyncExec(new Runnable() {
                @Override
                public void run() {
                    if (!getControl().isDisposed()) {
                        if (displayedList == null) {
                            setValueList(valueList);
                        }
                        else {
                            // TODO ensure there are no null items
                            getControl().setItems(displayedList);
                            setSelectedValue(selectedValue);
                        }
                    }
                }
            });
        }
    }

    @Override
    public String[] getValueList() {
        // TODO should clone the returned array
        return valueList;
    }

    public void setValueList(final String[] valueList) {
        if (getControl() != null) {
            SWTUtils.asyncExec(new Runnable() {
                @Override
                public void run() {
                    if (!getControl().isDisposed()) {
                        getControl().removeAll();
                        if (valueList != null) {
                            for (String value : valueList) {
                                getControl().add(value);
                            }
                        }
                        ComboBox.this.valueList = valueList;
                        fireSelectedItemChanged(new EventObject(this));
                    }
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        final int[] result = { 0 };
        if (getControl() != null) {
            SWTUtils.syncExec(new Runnable() {
                @Override
                public void run() {
                    if (!getControl().isDisposed()) {
                        result[0] = getControl().getItemCount();
                    }
                }
            });
        }
        return result[0];
    }

    public Object getSelectedItem() {
        // not to do
        return getSelectedValue();
    }

    public void setSelectedItem(Object item) {
        // not to do
        String value = item.toString();
        if (item instanceof String) {
            value = (String) item;
        }
        setSelectedValue(value);
    }

    @Override
    public String getSelectedValue() {
        final String[] selectedValue = { null };
        if (getControl() != null) {
            SWTUtils.syncExec(new Runnable() {
                @Override
                public void run() {
                    if (!getControl().isDisposed()) {
                        if (valueList != null) {
                            synchronized (valueList) {
                                int selectionIndex = getControl().getSelectionIndex();
                                // ensure we have enough values
                                if ((selectionIndex != -1) && (selectionIndex < valueList.length)) {
                                    selectedValue[0] = valueList[selectionIndex];
                                }
                            }
                        }
                    }
                }
            });
        }
        return selectedValue[0];
    }

    public void setSelectedValue(final String value) {
        if (getControl() != null) {
            SWTUtils.asyncExec(new Runnable() {
                @Override
                public void run() {
                    if (!getControl().isDisposed()) {
                        getControl().deselectAll();
                        if (valueList != null) {
                            synchronized (valueList) {
                                for (int i = 0; i < valueList.length; i++) {
                                    if (valueList[i].equals(value)) {
                                        getControl().select(i);
                                        break;
                                    }
                                }
                            }
                            fireSelectedItemChanged(new EventObject(this));
                        }
                    }
                }
            });
        }
    }

    @Override
    public int getSelectedIndex() {
        final int[] result = { -1 };
        if (getControl() != null) {
            SWTUtils.syncExec(new Runnable() {
                @Override
                public void run() {
                    if (!getControl().isDisposed()) {
                        result[0] = getControl().getSelectionIndex();
                    }
                }
            });
        }
        return result[0];
    }

    @Override
    public void setSelectedIndex(final int index) {
        if (getControl() != null) {
            SWTUtils.syncExec(new Runnable() {
                @Override
                public void run() {
                    if (!getControl().isDisposed()) {
                        getControl().select(index);
                        fireSelectedItemChanged(new EventObject(this));
                    }
                }
            });
        }
    }

    @Override
    public Object[] getObjectArray() {
        String[] stringList = getDisplayedList();
        Object[] objectList = null;
        if (stringList != null) {
            objectList = new Object[stringList.length];
            for (int i = 0; i < stringList.length; i++) {
                objectList[i] = stringList[i];
            }
        }
        return objectList;
    }

    @Override
    public void setObjectArray(Object[] value) {
        if (value != null) {
            String[] stringList = new String[value.length];
            for (int i = 0; i < stringList.length; i++) {
                stringList[i] = value[i].toString();
            }
            setDisplayedList(stringList);
        }
    }

    @Override
    public void setSelectedValue(Object arg0) {
        if (arg0 != null) {
            setSelectedValue(arg0.toString());
        }

    }

    @Override
    public void setValueList(Object... arg0) {
        String[] stringList = null;
        if (arg0 != null) {
            stringList = new String[arg0.length];
            for (int i = 0; i < stringList.length; i++) {
                stringList[i] = arg0[i].toString();
            }
            setValueList(stringList);
        }
    }

    public static void main(String[] args) {
        final Display display = new Display();
        final Shell shell = new Shell(display);

        shell.setBounds(0, 0, 400, 200);
        shell.setLayout(new FillLayout());
        shell.setText("ComboBox");

        ComboBox combo = new ComboBox(shell, SWT.NONE);
        combo.setValueList(new String[] { "option1", "option2", "option3" });

        // shell.pack();
        shell.open();

        while (!shell.isDisposed()) {
            if (!display.readAndDispatch()) {
                display.sleep();
            }
        }

        display.dispose();
        System.exit(0);
    }

}
