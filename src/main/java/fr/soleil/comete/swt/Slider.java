package fr.soleil.comete.swt;

import java.util.EventObject;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

import fr.soleil.comete.definition.listener.ISliderListener;
import fr.soleil.comete.definition.widget.ISlider;
import fr.soleil.comete.swt.util.SWTUtils;

public class Slider extends AbstractNumberRangeControl<org.eclipse.swt.widgets.Slider> implements ISlider, SelectionListener {

    public Slider(Composite parent, int childStyle) {
        super(parent, childStyle);
    }

    @Override
    protected org.eclipse.swt.widgets.Slider initControl(int childStyle) {
        org.eclipse.swt.widgets.Slider slider = new org.eclipse.swt.widgets.Slider(this, childStyle);
        slider.addSelectionListener(this);
        return slider;
    }

    @Override
    protected Number getControlValue() {
        Number value = null;
        if (getControl() != null) {
            value = getControl().getSelection();
        }
        return value;
    }

    @Override
    protected void setControlValue(Number value) {
        if ((getControl() != null) && (value != null)) {
            getControl().setSelection(value.intValue());
        }
    }

    @Override
    protected Number getControlMinimum() {
        Number min = null;
        if (getControl() != null) {
            min = getControl().getMinimum();
        }
        return min;
    }

    @Override
    protected void setControlMinimum(Number min) {
        if ((min != null) && (getControl() != null)) {
            getControl().setMinimum(min.intValue());
        }
    }

    @Override
    protected Number getControlMaximum() {
        Number max = null;
        if (getControl() != null) {
            max = getControl().getMaximum();
        }
        return max;
    }

    @Override
    protected void setControlMaximum(Number max) {
        if ((max != null) && (getControl() != null)) {
            getControl().setMaximum(max.intValue());
        }
    }

    @Override
    public int getMinimum() {
        return getMinimumInteger();
    }

    @Override
    public int getMaximum() {
        return getMaximumInteger();
    }

    @Override
    public void setEditable(boolean b) {
        setEnabled(b);
    }

    @Override
    public boolean isEditable() {
        return isEnabled();
    }

    @Override
    public void addSliderListener(ISliderListener listener) {
        addNumberListener(listener);
    }

    @Override
    public void removeSliderListener(ISliderListener listener) {
        removeNumberListener(listener);
    }

    @Override
    public void setMinorTickSpacing(int space) {
        // TODO Auto-generated method stub
    }

    @Override
    public void setMajorTickSpacing(int space) {
        // TODO Auto-generated method stub
    }

    @Override
    public int getMinorTickSpacing() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public int getMajorTickSpacing() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public double getStep() {
        int step = 0;
        if ((getControl() != null) && !getControl().isDisposed()) {
            step = getControl().getIncrement();
        }
        return step;
    }

    @Override
    public void setStep(final double step) {
        if (getControl() != null) {
            SWTUtils.asyncExec(new Runnable() {
                @Override
                public void run() {
                    if (!getControl().isDisposed()) {
                        getControl().setIncrement(Double.valueOf(step).intValue());

                    }
                }
            });
        }
    }

    @Override
    public void setLabelTable(String[] labels, int[] values) {
        // TODO Auto-generated method stub
    }

    @Override
    public void setPaintTicks(boolean paint) {
        // TODO Auto-generated method stub
    }

    @Override
    public void setPaintLabels(boolean paint) {
        // TODO Auto-generated method stub
    }

    @Override
    public boolean isPaintTicks() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean isPaintLabels() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void widgetDefaultSelected(SelectionEvent arg0) {
    }

    @Override
    public void widgetSelected(SelectionEvent arg0) {
        EventObject event = new EventObject(this);
        fireValueChanged(event);
    }

    public static void main(String[] args) {
        final Display display = new Display();
        final Shell shell = new Shell(display);

        shell.setBounds(0, 0, 400, 200);
        shell.setLayout(new FillLayout());
        shell.setText("Slider");

        final Slider slider = new Slider(shell, SWT.NONE);
        slider.setMaximum(1000);
        slider.setStep(10);
        slider.setToolTipText("Tooltype test");

        slider.addSliderListener(new ISliderListener() {
            @Override
            public void valueChanged(EventObject event) {
                System.out.println("selected=" + slider.getValue());
            }
        });


        // shell.pack();
        shell.open();

        while (!shell.isDisposed()) {
            if (!display.readAndDispatch()) {
                display.sleep();
            }
        }

        display.dispose();
        System.exit(0);
    }



}
