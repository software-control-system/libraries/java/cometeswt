package fr.soleil.comete.swt;

import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

import fr.soleil.comete.definition.listener.IMouseListener;
import fr.soleil.comete.definition.widget.IFrame;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.comete.definition.widget.util.CometeFont;
import fr.soleil.data.mediator.Mediator;

public class Frame extends Display implements IFrame {

    private int closeOperation = IFrame.HIDE_ON_CLOSE;
    private final Shell shell;

    public Frame() {
        shell = new Shell(this);
    }

    @Override
    public boolean hasFocus() {
        return false;
    }

    @Override
    public void setOpaque(boolean opaque) {
        // Nothing to do
    }

    @Override
    public boolean isOpaque() {
        return false;
    }

    @Override
    public void setCometeBackground(CometeColor color) {
        // Nothing to do
    }

    @Override
    public CometeColor getCometeBackground() {
        return null;
    }

    @Override
    public void setCometeForeground(CometeColor color) {
        // Nothing to do

    }

    @Override
    public CometeColor getCometeForeground() {
        return null;
    }

    @Override
    public void setEnabled(boolean enabled) {
        // Nothing to do

    }

    @Override
    public boolean isEnabled() {
        return false;
    }

    @Override
    public void setCometeFont(CometeFont font) {
        // Nothing to do
    }

    @Override
    public CometeFont getCometeFont() {
        return null;
    }

    @Override
    public void setToolTipText(String text) {
        // Nothing to do
    }

    @Override
    public String getToolTipText() {
        return null;
    }

    @Override
    public void setVisible(boolean visible) {
        if (visible) {
            // shell.pack();
            shell.open();
            while (!shell.isDisposed()) {
                if (!this.readAndDispatch()) {
                    this.sleep();
                }
            }
        }

        if (!this.isDisposed()) {
            dispose();
            if (closeOperation == IFrame.EXIT_ON_CLOSE) {
                System.exit(0);
            }
        }

    }

    @Override
    public boolean isVisible() {
        return !this.isDisposed();
    }

    @Override
    public void setHorizontalAlignment(int halign) {
        // Nothing to do

    }

    @Override
    public int getHorizontalAlignment() {
        return 0;
    }

    @Override
    public void setSize(int width, int height) {
        if ((shell != null) && !shell.isDisposed()) {
            shell.setSize(width, height);
        }
    }

    @Override
    public void setPreferredSize(int width, int height) {
        setSize(width, height);
    }

    @Override
    public int getWidth() {
        int width = 0;
        if ((shell != null) && !shell.isDisposed()) {
            Rectangle bounds = shell.getBounds();
            width = bounds.width;
        }
        return width;
    }

    @Override
    public int getHeight() {
        int height = 0;
        if ((shell != null) && !shell.isDisposed()) {
            Rectangle bounds = shell.getBounds();
            height = bounds.height;
        }
        return height;
    }

    @Override
    public void setLocation(int x, int y) {
        if ((shell != null) && !shell.isDisposed()) {
            shell.setLocation(x, y);
        }
    }

    @Override
    public int getX() {
        int x = 0;
        if ((shell != null) && !shell.isDisposed()) {
            Point location = shell.getLocation();
            x = location.x;
        }
        return x;
    }

    @Override
    public int getY() {
        int y = 0;
        if ((shell != null) && !shell.isDisposed()) {
            Point location = shell.getLocation();
            y = location.y;
        }
        return y;
    }

    @Override
    public boolean isEditingData() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void setTitledBorder(String title) {
        // TODO Auto-generated method stub

    }

    @Override
    public void addMouseListener(IMouseListener listener) {
        // TODO Auto-generated method stub

    }

    @Override
    public void removeMouseListener(IMouseListener listener) {
        // TODO Auto-generated method stub

    }

    @Override
    public void removeAllMouseListeners() {
        // TODO Auto-generated method stub

    }

    @Override
    public void addMediator(Mediator<?> mediator) {
        // TODO Auto-generated method stub

    }

    @Override
    public void removeMediator(Mediator<?> mediator) {
        // TODO Auto-generated method stub

    }

    @Override
    public void setTitle(String title) {
        shell.setText(title);
    }

    @Override
    public String getTitle() {
        return shell.getText();
    }

    @Override
    public void setDefaultCloseOperation(int operation) {
        closeOperation = operation;
    }

    @Override
    public void setContentPane(Object panel) {
        // Nothing to do in SWT composite constructor always need parent
    }


}
