//+============================================================================
//Source: package fr.soleil.comete.widget.swing;/Label.java
//
//project :     Comete
//
//Description: This class hides
//
//Author: SAINTIN
//
//Revision: 1.1
//
//Log:
//
//copyleft :Synchrotron SOLEIL
//			L'Orme des Merisiers
//			Saint-Aubin - BP 48
//			91192 GIF-sur-YVETTE CEDEX
//			FRANCE
//
//+============================================================================
package fr.soleil.comete.swt;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

import fr.soleil.comete.definition.data.target.scalar.IEditableComponent;
import fr.soleil.comete.definition.widget.IComponent;
import fr.soleil.comete.definition.widget.ILabel;
import fr.soleil.comete.definition.widget.util.CometeImage;
import fr.soleil.comete.swt.util.ImageTool;
import fr.soleil.comete.swt.util.SWTUtils;
import fr.soleil.comete.swt.util.SwtImage;
import fr.soleil.data.target.IUnitTarget;

public class Label extends CometeComposite<org.eclipse.swt.widgets.Label> implements ILabel, IUnitTarget,
IEditableComponent {

    private String unit = "";

    public Label(Composite parent, int childStyle) {
        super(parent, childStyle);
    }

    @Override
    protected org.eclipse.swt.widgets.Label initControl(int childStyle) {
        return new org.eclipse.swt.widgets.Label(this, childStyle);
    }

    @Override
    public String getUnit() {
        return this.unit;
    }

    @Override
    public void setUnit(final String unit) {
        this.unit = unit;
    }

    /**
     * Returns the text string that the label displays with its unit
     *
     * @return a String
     * @see #setText
     */
    @Override
    public String getText() {
        String result = null;
        if (getControl() != null) {
            final String[] threadResult = { null };
            SWTUtils.syncExec(new Runnable() {
                @Override
                public void run() {
                    if (!getControl().isDisposed()) {
                        threadResult[0] = getControl().getText();
                    }
                }
            });
            result = threadResult[0];
            if (unit != null) {
                result = result + " " + unit;
            }
        }
        return result;
    }

    @Override
    public void setText(final String text) {
        if (getControl() != null) {
            final String theText = (text == null ? "" : text);
            SWTUtils.asyncExec(new Runnable() {
                @Override
                public void run() {
                    if (!getControl().isDisposed()) {
                        getControl().setText(theText);
                    }
                }
            });
        }
    }

    @Override
    public void setCometeImage(CometeImage image) {
        if (getControl() != null) {
            final SwtImage swtImage = ImageTool.getImage(image);
            if ((swtImage != null) && (swtImage.getImage() != null)) {
                SWTUtils.asyncExec(new Runnable() {
                    @Override
                    public void run() {
                        if (!getControl().isDisposed()) {
                            getControl().setImage(swtImage.getImage());
                        }
                    }
                });
            }
        }
    }

    @Override
    public CometeImage getCometeImage() {
        // TODO convert SWT Image into CometeImage
        CometeImage result = null;
        getControl().getImage();
        return result;
    }

    @Override
    public boolean isEditable() {
        return false;
    }

    @Override
    public void setEditable(boolean editable) {
        // Nothing to do: a label is not editable
    }

    @Override
    public int getHorizontalAlignment() {
        int cometeAlign = IComponent.CENTER;
        if (getControl() != null) {
            final int[] threadResult = { SWT.LEFT };
            SWTUtils.syncExec(new Runnable() {
                @Override
                public void run() {
                    if (!getControl().isDisposed()) {
                        threadResult[0] = getControl().getAlignment();
                    }
                }
            });
            int swtAlign = threadResult[0];

            switch (swtAlign) {
                case SWT.CENTER:
                    cometeAlign = IComponent.CENTER;
                    break;
                case SWT.LEFT:
                    cometeAlign = IComponent.LEFT;
                    break;
                case SWT.RIGHT:
                    cometeAlign = IComponent.RIGHT;
                    break;
                default:
                    break;
            }
        }
        return cometeAlign;
    }

    @Override
    public void setHorizontalAlignment(final int align) {
        if (getControl() != null) {
            SWTUtils.asyncExec(new Runnable() {
                @Override
                public void run() {
                    if (!getControl().isDisposed()) {
                        switch (align) {
                            case IComponent.CENTER:
                                getControl().setAlignment(SWT.CENTER);
                                break;
                            case IComponent.LEFT:
                                getControl().setAlignment(SWT.LEFT);
                                break;
                            case IComponent.RIGHT:
                                getControl().setAlignment(SWT.RIGHT);
                                break;
                            default:
                                break;
                        }
                    }
                }
            });
        }
    }

    public static void main(String[] args) {
        final Display display = new Display();
        final Shell shell = new Shell(display);

        shell.setBounds(0, 0, 400, 200);
        shell.setLayout(new FillLayout());
        shell.setText("Label");

        Label label = new Label(shell, SWT.NONE);
        label.setText("Label SWT Test");
        label.setToolTipText("Tooltype test");

        // shell.pack();
        shell.open();

        while (!shell.isDisposed()) {
            if (!display.readAndDispatch()) {
                display.sleep();
            }
        }

        display.dispose();
        System.exit(0);
    }


}
