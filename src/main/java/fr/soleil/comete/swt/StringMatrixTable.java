package fr.soleil.comete.swt;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

import fr.soleil.data.container.matrix.StringMatrix;
import fr.soleil.data.target.matrix.ITextMatrixTarget;

public class StringMatrixTable extends AbstractMatrixTable<String> implements ITextMatrixTarget {


    public StringMatrixTable(Composite parent, int childStyle) {
        super(parent, childStyle);
    }

    @Override
    public String[][] getStringMatrix() {
        String[][] result = null;
        if (matrix != null) {
            result = (String[][]) matrix.getValue();
        }
        return result;
    }

    @Override
    public void setStringMatrix(String[][] value) {
        StringMatrix matrix = new StringMatrix();
        matrix.setValue(value);
        setData(matrix);
    }

    @Override
    public String[] getFlatStringMatrix() {
        String[] result = null;
        if (matrix != null) {
            result = (String[]) matrix.getFlatValue();
        }
        return result;
    }

    @Override
    public void setFlatStringMatrix(String[] value, int width, int height) {
        StringMatrix matrix = new StringMatrix();
        matrix.setFlatValue(value, height, width);
        setData(matrix);
    }

    /**
     * @param args
     */
    public static void main(String[] args) {
        final Display display = new Display();
        final Shell shell = new Shell(display);

        shell.setBounds(0, 0, 990, 510);
        shell.setLayout(new FillLayout());
        shell.setText("StringMatrixTable");

        StringMatrixTable stringTable = new StringMatrixTable(shell, SWT.NONE);

        String[][] value = new String[][] { { "cell11", "cell12", "cell13" }, { "cell21", "cell22", "cell23" },
                { "cell31", "cell32", "cell34" } };
        stringTable.setStringMatrix(value);

        // shell.pack();
        shell.open();

        while (!shell.isDisposed()) {
            if (!display.readAndDispatch()) {
                display.sleep();
            }
        }

        display.dispose();
        System.exit(0);
    }

}
