package fr.soleil.comete.swt;

import java.awt.Color;
import java.awt.Frame;
import java.awt.event.MouseWheelEvent;
import java.io.IOException;
import java.lang.reflect.Array;

import javax.activation.UnsupportedDataTypeException;
import javax.swing.JPanel;

import org.eclipse.swt.SWT;
import org.eclipse.swt.awt.SWT_AWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseWheelListener;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.jfree.chart.ChartMouseEvent;
import org.jfree.chart.ChartMouseListener;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.title.TextTitle;
import org.jfree.data.general.Dataset;
import org.jfree.data.xy.XYZDataset;

import fr.soleil.comete.definition.widget.IComponent;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.comete.swt.util.ColorTool;
import fr.soleil.comete.swt.viewers.utils.CometeDefaultXYZDataset;
import fr.soleil.comete.swt.viewers.utils.Hist2DPanel;
import fr.soleil.comete.swt.viewers.utils.IPlot;
import fr.soleil.comete.swt.viewers.utils.PlotFactory;
import fr.soleil.data.container.matrix.AbstractNumberMatrix;
import fr.soleil.data.container.matrix.ByteMatrix;
import fr.soleil.data.container.matrix.DoubleMatrix;
import fr.soleil.data.container.matrix.FloatMatrix;
import fr.soleil.data.container.matrix.IntMatrix;
import fr.soleil.data.container.matrix.LongMatrix;
import fr.soleil.data.container.matrix.ShortMatrix;
import fr.soleil.data.target.matrix.INumberMatrixTarget;
import fr.soleil.data.target.scalar.ITextTarget;

public class ImageViewer extends CometeComposite<Control> implements INumberMatrixTarget,
ITextTarget {

    private String title = null;
    private AbstractNumberMatrix<? extends Number> abstractmatrix = null;

    private IPlot plot;
    private Frame frame;
    private MouseWheelListener mouseWheelListener;
    private KeyListener keyListener;
    private ChartMouseListener chartMouseListener;

    public ImageViewer(Composite parent, int childStyle) {
        super(parent, SWT.EMBEDDED, childStyle);
        frame = SWT_AWT.new_Frame(this);
    }

    @Override
    protected Control initControl(int childStyle) {
        return null;
    }

    protected void embedPlot(IPlot plot) {
        if (plot instanceof JPanel) {
            if (frame != null) {
                frame.add((JPanel) plot);
            }
        }
        else {
            throw new IllegalArgumentException("must be a chart plot panel");
        }
    }

    public void setPlot(final IPlot plot) {
        final IPlot oldPlot = this.plot;
        // if (oldPlot != null) {
        // oldPlot.cleanUp();
        // frame.remove((JPanel) oldPlot);
        // }
        this.plot = plot;
        title = getText();
        final Composite composite = this;
        Display display = Display.getCurrent();
        if (display == null) {
            display = Display.getDefault();
        }

        if (!isDisposed()) {
            embedPlot(plot);
            removeListeners(oldPlot);
            addListeners();
            composite.pack();
            composite.getParent().layout(true, true);
        }

    }

    private void removeListeners(IPlot plot) {
        if (plot != null) {
            removeMouseWheelListener(mouseWheelListener);
            removeKeyListener(keyListener);
            plot.removeChartMouseListener(chartMouseListener);
        }
    }

    private void addListeners() {
        if (plot != null) {
            mouseWheelListener = new MouseWheelListener() {

                @Override
                public void mouseScrolled(MouseEvent event) {
                    JPanel panel = null;
                    if (plot instanceof JPanel) {
                        panel = (JPanel) plot;
                    }
                    MouseWheelEvent awtEvent = org.gumtree.vis.listener.SWT_AWT.toMouseWheelEvent(
                            event, panel);
                    plot.processMouseWheelEvent(awtEvent);
                }
            };
            addMouseWheelListener(mouseWheelListener);

            keyListener = new KeyListener() {

                boolean keyPressed = false;

                @Override
                public void keyReleased(KeyEvent event) {
                    switch (event.keyCode) {
                        case SWT.DEL:
                            plot.removeSelectedMask();
                            break;
                        default:
                            break;
                    }
                    switch (event.character) {
                        default:
                            break;
                    }
                    keyPressed = false;
                }

                @Override
                public void keyPressed(KeyEvent event) {
                    switch (event.keyCode) {
                        case SWT.ARROW_UP:
                            plot.moveSelectedMask(event.keyCode);
                            break;
                        case SWT.ARROW_LEFT:
                            plot.moveSelectedMask(event.keyCode);
                            break;
                        case SWT.ARROW_RIGHT:
                            plot.moveSelectedMask(event.keyCode);
                            break;
                        case SWT.ARROW_DOWN:
                            plot.moveSelectedMask(event.keyCode);
                            break;
                        default:
                            break;
                    }
                    switch (event.stateMask) {
                        case SWT.CTRL:
                            if ((event.keyCode == 'c') || (event.keyCode == 'C')) {
                                if (!keyPressed) {
                                    plot.doCopy();
                                }
                            }
                            else if ((event.keyCode == 'z') || (event.keyCode == 'Z')
                                    || (event.keyCode == 'r') || (event.keyCode == 'R')) {
                                if (!keyPressed) {
                                    plot.restoreAutoBounds();
                                }
                            }
                            else if ((event.keyCode == 'p') || (event.keyCode == 'P')) {
                                System.out.println("p pressed");
                                if (!keyPressed) {
                                    Thread newThread = new Thread(new Runnable() {

                                        @Override
                                        public void run() {
                                            plot.createChartPrintJob();
                                        }
                                    });
                                    newThread.start();
                                }
                            }
                            else if ((event.keyCode == 'e') || (event.keyCode == 'E')) {
                                System.out.println("s pressed");
                                if (!keyPressed) {
                                    Thread newThread = new Thread(new Runnable() {

                                        @Override
                                        public void run() {
                                            try {
                                                plot.doSaveAs();
                                            }
                                            catch (IOException e) {
                                                handleException(e);
                                            }
                                        }
                                    });
                                    newThread.start();
                                }
                            }
                            keyPressed = true;
                            break;
                        default:
                            break;
                    }
                }
            };
            addKeyListener(keyListener);

            final Composite composite = this;

            chartMouseListener = new ChartMouseListener() {

                @Override
                public void chartMouseMoved(ChartMouseEvent event) {

                }

                @Override
                public void chartMouseClicked(ChartMouseEvent event) {
                    Display.getDefault().asyncExec(new Runnable() {

                        @Override
                        public void run() {
                            if (!composite.isFocusControl()) {
                                composite.setFocus();
                            }
                        }
                    });
                }
            };
            plot.addChartMouseListener(chartMouseListener);
        }
    }

    public JFreeChart getChart() {
        if (plot != null) {
            return plot.getChart();
        }
        else {
            return null;
        }
    }

    @Override
    public void setVisible(boolean visible) {
        if (plot != null) {
            plot.setVisible(visible);
        }
        super.setVisible(visible);
    }

    public void setDataset(Dataset dataset) {
        try {
            if (plot == null) {
                if (dataset instanceof XYZDataset) {
                    IPlot newPlot = PlotFactory.createHist2DPanel(((XYZDataset) dataset), title);
                    setPlot(newPlot);
                }
            }
            if (plot != null) {
                plot.setDataset(dataset);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Dataset getDataset() {
        return getPlot().getDataset();
    }

    public IPlot getPlot() {
        return plot;
    }

    @Override
    public void redraw() {
        plot.repaint();
        super.redraw();
    }

    public void handleException(final Exception e) {
        Display display = Display.getCurrent();
        if (display == null) {
            display = Display.getDefault();
        }
        display.asyncExec(new Runnable() {
            @Override
            public void run() {
                if (getShell() != null) {
                    MessageBox messageBox = new MessageBox(getShell(), SWT.ICON_WARNING | SWT.OK);
                    messageBox.setText("Failed to Save");
                    messageBox.setMessage("failed : " + e.getMessage());
                    messageBox.open();
                    // MessageDialog.openError(getShell(), "Failed to Save", "failed to save " +
                    // "the image: " + e.getMessage());

                }
            }
        });
    }

    @Override
    public void dispose() {
        if (!isDisposed()) {
            if (mouseWheelListener != null) {
                removeMouseWheelListener(mouseWheelListener);
                mouseWheelListener = null;
            }
            if (keyListener != null) {
                removeKeyListener(keyListener);
                keyListener = null;
            }
            if (plot != null) {
                plot.cleanUp();
                if (chartMouseListener != null) {
                    plot.removeChartMouseListener(chartMouseListener);
                    chartMouseListener = null;
                }
                frame.remove((JPanel) plot);
                frame.dispose();
                plot = null;
            }
        }
        frame = null;
        super.dispose();
    }

    @Override
    public String getText() {
        String result = null;
        if (plot != null) {
            JFreeChart chart = plot.getChart();
            if (chart != null) {
                TextTitle title = chart.getTitle();
                if (title != null) {
                    result = title.getText();
                }
            }
        }
        return result;
    }

    @Override
    public void setText(String value) {
        if (plot != null) {
            JFreeChart chart = plot.getChart();
            if (chart != null) {
                chart.setTitle(value);
                title = value;
            }
        }
    }

    @Override
    public Object getFlatNumberMatrix() {
        if (abstractmatrix != null) {
            return abstractmatrix.getFlatValue();
        }
        return null;
    }

    @Override
    public Object[] getNumberMatrix() {
        if (abstractmatrix != null) {
            return abstractmatrix.getValue();
        }
        return null;
    }

    @Override
    public void setFlatNumberMatrix(Object value, int width, int height) {
        try {
            if ((width > 0) && (height > 0)) {
                Object firstValue = Array.get(value, 0);
                initMatrix(firstValue);
            }
            if (abstractmatrix != null) {
                abstractmatrix.setFlatValue(value, height, width);
                // XYZDataset dataset = convertToDataset(value, width, height);
                XYZDataset dataset = convertToDataset();
                setDataset(dataset);
                if ((plot != null) && (plot instanceof Hist2DPanel)) {
                    ((Hist2DPanel) plot).setFlatNumberMatrix(value, width, height);
                }
            }
        }
        catch (UnsupportedDataTypeException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private void initMatrix(Object firstValue) {
        if (firstValue != null) {
            // System.out.println(firstValue.getClass().getName());
            if (firstValue instanceof Double) {
                abstractmatrix = new DoubleMatrix();
            }
            else if (firstValue instanceof Short) {
                abstractmatrix = new ShortMatrix();
            }
            else if (firstValue instanceof Integer) {
                abstractmatrix = new IntMatrix();
            }
            else if (firstValue instanceof Float) {
                abstractmatrix = new FloatMatrix();
            }
            else if (firstValue instanceof Long) {
                abstractmatrix = new LongMatrix();
            }
            else if (firstValue instanceof Byte) {
                abstractmatrix = new ByteMatrix();
            }
        }
    }

    @Override
    public void setNumberMatrix(Object[] value) {
        if ((value != null) && (value.length > 0)) {
            Object firstValue = value[0];
            if (Array.getLength(firstValue) > 0) {
                firstValue = Array.get(firstValue, 0);
            }
            initMatrix(firstValue);
            try {
                abstractmatrix.setValue(value);
                // XYZDataset dataset = convertToDataset(abstractmatrix.getFlatValue(),
                // abstractmatrix.getWidth(), abstractmatrix.getHeight());
                XYZDataset dataset = convertToDataset();
                setDataset(dataset);
                if ((plot != null) && (plot instanceof Hist2DPanel)) {
                    ((Hist2DPanel) plot).setNumberMatrix(value);
                }
            } catch (UnsupportedDataTypeException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

    }

    private XYZDataset convertToDataset() {
        CometeDefaultXYZDataset dataset = new CometeDefaultXYZDataset();
        if (abstractmatrix != null) {
            // Object[] object = abstractmatrix.getValueAt(row, col)
            int height = abstractmatrix.getHeight();
            int width = abstractmatrix.getWidth();

            double[] x = new double[height * width];
            double[] y = new double[height * width];
            double[] z = new double[height * width];
            for (int l = 0; l < height; l++) {
                for (int c = 0; c < width; c++) {
                    x[l * width + c] = c;
                    y[l * width + c] = l;
                    z[l * width + c] = parseDouble(abstractmatrix.getValueAt(l, c));
                }
            }
            dataset.addSeries("image", new double[][] { x, y, z });

        }
        return dataset;
    }

    private double parseDouble(Object object) {
        double doubleValue = Double.NaN;
        if (object != null) {
            doubleValue = ((Number) object).doubleValue();
            ;
        }
        return doubleValue;

    }

    @Override
    public int getMatrixDataHeight(Class<?> concernedDataClass) {
        return abstractmatrix.getHeight();
    }

    @Override
    public int getMatrixDataWidth(Class<?> concernedDataClass) {
        return abstractmatrix.getWidth();
    }

    @Override
    public boolean isPreferFlatValues(Class<?> arg0) {
        return false;
    }

    @Override
    public void setCometeBackground(CometeColor color) {
        super.setCometeBackground(color);

        if (plot != null) {
            CometeColor cometeColor = ColorTool.getCometeColor(background);
            Color colorAwt = fr.soleil.comete.awt.util.ColorTool.getColor(cometeColor);
            plot.setBackgroundColor(colorAwt);
        }
    }

    @Override
    public void setHorizontalAlignment(int halign) {
        // nop
    }

    @Override
    public int getHorizontalAlignment() {
        return IComponent.CENTER;
    }

    public static void main(String[] args) {
        final Display display = new Display();
        final Shell shell = new Shell(display);

        shell.setBounds(0, 0, 990, 510);
        shell.setLayout(new FillLayout());
        shell.setText("ImageViewer");

        ImageViewer imageViewer = new ImageViewer(shell, SWT.NONE);

        // TODO voir pourquoi l'�chelle ne se met pas � jour lors du changement de dataset
        // le min et le max ne sont pas mis � jour
        double[][] t2 = new double[][] { { 2., 4., 6. }, { 8., 10., 12. }, { 14., 16., 18. },
                { 20., 22., 24. } };
        imageViewer.setNumberMatrix(t2);

        // shell.pack();
        shell.open();

        while (!shell.isDisposed()) {
            if (!display.readAndDispatch()) {
                display.sleep();
            }
        }

        display.dispose();
        System.exit(0);
    }

}
