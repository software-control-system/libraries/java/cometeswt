//+============================================================================
//Source: package fr.soleil.comete.widget.awt.util;/AwtImageTool.java
//
//project :     Comete
//
//Description: This class hides
//
//Author: SAINTIN
//
//Revision: 1.1
//
//Log:
//
//copyleft :Synchrotron SOLEIL
//			L'Orme des Merisiers
//			Saint-Aubin - BP 48
//			91192 GIF-sur-YVETTE CEDEX
//			FRANCE
//
//+============================================================================
package fr.soleil.comete.swt.util;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

import fr.soleil.comete.definition.widget.util.CometeImage;

public class ImageTool {

    public static SwtImage getImage(CometeImage image) {
        SwtImage result = null;
        if (image != null) {
            result = new SwtImage(image.getImagePath());
        }
        return result;
    }

    public static Image getImage(String imagePath) {
        Image result = null;
        try {
            result = new Image(Display.getCurrent(), imagePath);
        }
        catch (Exception e) {
            result = null;
        }

        return result;
    }

    public static CometeImage getCometeImage(SwtImage image) {
        CometeImage result = null;
        if (image != null) {
            result = new CometeImage(image.getImagePath());
        }
        return result;
    }

    public static void main(String[] args) {
        final Display display = new Display();
        final Shell shell = new Shell(display);
        shell.setLayout(new FillLayout(SWT.VERTICAL));
        Button button = new Button(shell, SWT.NONE);
        Image image = ImageTool.getImage("D:\\MesDocuments\\Soleil\\Salsa\\SalsaSoleilHQ.gif");
        System.out.println(image);
        button.setImage(image);
        shell.open();

        while (!shell.isDisposed()) {

            if (!display.readAndDispatch()) {

                display.sleep();
            }
        }

        display.dispose();
        System.exit(0);

    }
}
