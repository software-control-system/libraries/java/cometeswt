package fr.soleil.comete.swt.util;

import org.eclipse.swt.widgets.Display;

public class SWTUtils {

    public static void asyncExec(Runnable r) {
        if (Display.getCurrent() != null) {
            r.run();
        } else {
            Display.getDefault().asyncExec(r);
        }
    }

    public static void syncExec(Runnable r) {
        if (Display.getCurrent() != null) {
            r.run();
        } else {
            Display.getDefault().syncExec(r);
        }
    }

}
