package fr.soleil.comete.swt.util;

import java.util.List;
import java.util.ListIterator;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;

import fr.soleil.comete.definition.widget.util.CometeImage;
import fr.soleil.comete.definition.widget.util.ITreeNode;

public class TreeNodeTool {

    public static TreeItem createRootTreeNode(Tree parent, ITreeNode cometeTreeNode) {
        TreeItem treeItem = null;
        if (cometeTreeNode != null) {
            treeItem = new TreeItem(parent, SWT.NONE);
            fillTreeNode(treeItem, cometeTreeNode);
        }
        return treeItem;
    }

    public static TreeItem createChildTreeNode(TreeItem parent, ITreeNode cometeTreeNode) {
        TreeItem treeItem = null;
        if (cometeTreeNode != null) {
            treeItem = new TreeItem(parent, SWT.NONE);
            fillTreeNode(treeItem, cometeTreeNode);
        }
        return treeItem;
    }

    public static TreeItem createTreeNode(TreeItem parent, ITreeNode cometeTreeNode, int index) {
        TreeItem treeItem = null;
        if (cometeTreeNode != null) {
            treeItem = new TreeItem(parent, SWT.NONE, index);
            fillTreeNode(treeItem, cometeTreeNode);
        }
        return treeItem;
    }

    private static void fillTreeNode(TreeItem treeItem, ITreeNode cometeTreeNode) {
        String treeName = cometeTreeNode.getName();
        Object data = cometeTreeNode.getData();
        treeItem.setText(treeName);
        treeItem.setData(data);
        CometeImage image = cometeTreeNode.getImage();
        SwtImage swtImage = ImageTool.getImage(image);
        if ((swtImage != null) && (swtImage.getImage() != null)) {
            treeItem.setImage(swtImage.getImage());
        }
        createChildTreeNodes(treeItem, cometeTreeNode);
    }

    private static void createChildTreeNodes(TreeItem parent, ITreeNode cometeTreeNode) {
        List<ITreeNode> childNode = cometeTreeNode.getChildren();
        ListIterator<ITreeNode> listIterator = childNode.listIterator();
        int index = 0;
        while (listIterator.hasNext()) {
            ITreeNode iTreeNode = listIterator.next();
            createTreeNode(parent, iTreeNode, index);
            index++;
        }
    }

    public static TreeItem findTreeItemFromTreeNode(ITreeNode rootNode, TreeItem treeItem) {
        TreeItem result = null;
        if ((treeItem != null) && (rootNode != null)) {
            result = findTreeItem_aux(rootNode, treeItem);
        }
        return result;
    }

    private static TreeItem findTreeItem_aux(ITreeNode node, TreeItem treeItem) {
        TreeItem result = null;

        if (sameDatas(treeItem, node)) {
            result = treeItem;
        }
        else {
            for (TreeItem child : treeItem.getItems()) {
                result = findTreeItem_aux(node, child);
                if (result != null) {
                    break;
                }
            }
        }

        return result;
    }

    public static ITreeNode findTreeNodeForTreeItem(TreeItem treeItem, ITreeNode rootNode) {
        ITreeNode result = null;
        if ((treeItem != null) && (rootNode != null)) {
            result = findTreeNode_aux(treeItem, rootNode);
        }
        return result;
    }

    private static ITreeNode findTreeNode_aux(TreeItem treeItem, ITreeNode node) {
        ITreeNode result = null;

        if (sameDatas(treeItem, node)) {
            result = node;
        }
        else {
            for (ITreeNode child : node.getChildren()) {
                result = findTreeNode_aux(treeItem, child);
                if (result != null) {
                    break;
                }
            }
        }

        return result;
    }

    private static boolean sameDatas(TreeItem treeItem, ITreeNode node) {
        return treeItem.getText().equals(node.getName()) && // text can't be null
        (treeItem.getData() == node.getData());// == or equals
    }

    // -------------------------

    public static TreeItem createRootTreeNode2(Tree parent, ITreeNode cometeTreeNode) {
        TreeItem treeItem = null;
        if (cometeTreeNode != null) {
            String treeName = cometeTreeNode.getName();
            Object data = cometeTreeNode.getData();
            treeItem = new TreeItem(parent, SWT.NONE);
            treeItem.setText(treeName);
            treeItem.setData(data);
            createChildTreeNodes2(treeItem, cometeTreeNode);
        }
        return treeItem;
    }

    public static TreeItem createChildTreeNode2(TreeItem parent, ITreeNode cometeTreeNode, int index) {
        TreeItem treeItem = null;
        if (cometeTreeNode != null) {
            String treeName = cometeTreeNode.getName();
            Object data = cometeTreeNode.getData();
            treeItem = new TreeItem(parent, SWT.NONE, index);
            treeItem.setText(treeName);
            treeItem.setData(data);
            createChildTreeNodes2(treeItem, cometeTreeNode);
        }
        return treeItem;
    }

    private static void createChildTreeNodes2(TreeItem parent, ITreeNode cometeTreeNode) {
        List<ITreeNode> childNode = cometeTreeNode.getChildren();
        ListIterator<ITreeNode> listIterator = childNode.listIterator();
        int index = 0;
        while (listIterator.hasNext()) {
            ITreeNode iTreeNode = listIterator.next();
            createChildTreeNode2(parent, iTreeNode, index);
            index++;
        }
    }

}
