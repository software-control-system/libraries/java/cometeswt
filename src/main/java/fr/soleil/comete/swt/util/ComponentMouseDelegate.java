package fr.soleil.comete.swt.util;

import java.awt.Component;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.MouseMoveListener;
import org.eclipse.swt.events.MouseTrackListener;
import org.eclipse.swt.events.MouseWheelListener;
import org.eclipse.swt.widgets.Control;

import fr.soleil.comete.definition.event.CometeMouseEvent;
import fr.soleil.comete.definition.event.CometeMouseEvent.MouseReason;
import fr.soleil.comete.definition.listener.IMouseListener;
import fr.soleil.comete.definition.widget.IComponent;
import fr.soleil.comete.swt.CometeComposite;

/**
 * This class handles Comete mouse event forwarding for {@link IComponent}s that also are
 * {@link Component}s
 *
 * @author saintin
 */
public class ComponentMouseDelegate implements MouseListener, MouseWheelListener,
MouseMoveListener, MouseTrackListener {

    private static final ComponentMouseDelegate instance = new ComponentMouseDelegate();

    private final Map<IComponent, List<WeakReference<IMouseListener>>> listenerMap;

    private ComponentMouseDelegate() {
        listenerMap = new HashMap<IComponent, List<WeakReference<IMouseListener>>>();
    }

    /**
     * Adds an {@link IMouseListener} to an {@link IComponent}, if this {@link IComponent} is a
     * {@link Component}
     *
     * @param comp The concerned {@link IComponent}
     * @param listener The {@link IMouseListener} to add
     */
    public static void addMouseListenerToComponent(IComponent comp, IMouseListener listener) {
        instance.registerMouseListenerToComponent(comp, listener);
    }

    /**
     * Removes an {@link IMouseListener} from an {@link IComponent}, if this {@link IComponent} is a
     * {@link Component}
     *
     * @param comp The concerned {@link IComponent}
     * @param listener The {@link IMouseListener} to remove
     */
    public static void removeMouseListenerFromComponent(IComponent comp, IMouseListener listener) {
        instance.unregisterMouseListenerFromComponent(comp, listener);
    }

    /**
     * Removes all {@link IMouseListener}s from an {@link IComponent}, if this {@link IComponent} is
     * a {@link Component}
     *
     * @param comp The concerned {@link IComponent}
     */
    public static void removeAllMouseListenersFromComponent(IComponent comp) {
        instance.unregisterAllMouseListenersFromComponent(comp);
    }

    /**
     * Adds an {@link IMouseListener} to an {@link IComponent}, if this {@link IComponent} is a
     * {@link Component}
     *
     * @param comp The concerned {@link IComponent}
     * @param listener The {@link IMouseListener} to add
     */
    private void registerMouseListenerToComponent(IComponent comp, IMouseListener listener) {
        if ((listener != null) && (comp instanceof CometeComposite)) {
            List<WeakReference<IMouseListener>> listeners;
            CometeComposite<?> cometeComposite = (CometeComposite<?>) comp;
            Control component = cometeComposite.getControl();
            if (component == null) {
                component = cometeComposite;
            }
            synchronized (listenerMap) {
                listeners = listenerMap.get(comp);
                if (listeners == null) {
                    component.removeMouseListener(this);
                    component.removeMouseWheelListener(this);
                    component.removeMouseMoveListener(this);
                    component.removeMouseTrackListener(this);
                    listeners = new ArrayList<WeakReference<IMouseListener>>();
                    listenerMap.put(comp, listeners);
                    component.addMouseListener(this);
                    component.addMouseWheelListener(this);
                    component.addMouseMoveListener(this);
                    component.addMouseTrackListener(this);
                }
            }
            synchronized (listeners) {
                List<WeakReference<IMouseListener>> toRemove = new ArrayList<WeakReference<IMouseListener>>();
                boolean canAdd = true;
                for (WeakReference<IMouseListener> ref : listeners) {
                    IMouseListener temp = ref.get();
                    if (temp == null) {
                        toRemove.add(ref);
                    }
                    else if (temp.equals(listener)) {
                        canAdd = false;
                    }
                }
                listeners.removeAll(toRemove);
                toRemove.clear();
                if (canAdd) {
                    listeners.add(new WeakReference<IMouseListener>(listener));
                }
            }
        }
    }

    /**
     * Removes an {@link IMouseListener} from an {@link IComponent}, if this {@link IComponent} is a
     * {@link Component}
     *
     * @param comp The concerned {@link IComponent}
     * @param listener The {@link IMouseListener} to remove
     */
    private void unregisterMouseListenerFromComponent(IComponent comp, IMouseListener listener) {
        if ((listener != null) && (comp instanceof CometeComposite)) {
            List<WeakReference<IMouseListener>> listeners;
            CometeComposite<?> cometeComposite = (CometeComposite<?>) comp;
            Control component = cometeComposite.getControl();
            if (component == null) {
                component = cometeComposite;
            }
            synchronized (listenerMap) {
                listeners = listenerMap.get(comp);
                if (listeners == null) {
                    component.removeMouseListener(this);
                    component.removeMouseWheelListener(this);
                    component.removeMouseMoveListener(this);
                    component.removeMouseTrackListener(this);
                }
            }
            if (listeners != null) {
                synchronized (listeners) {
                    List<WeakReference<IMouseListener>> toRemove = new ArrayList<WeakReference<IMouseListener>>();
                    for (WeakReference<IMouseListener> ref : listeners) {
                        IMouseListener temp = ref.get();
                        if ((temp == null) || temp.equals(listener)) {
                            toRemove.add(ref);
                        }
                    }
                    listeners.removeAll(toRemove);
                    toRemove.clear();
                    if (listeners.isEmpty()) {
                        synchronized (listenerMap) {
                            listenerMap.remove(comp);
                            component.removeMouseListener(this);
                            component.removeMouseWheelListener(this);
                            component.removeMouseMoveListener(this);
                            component.removeMouseTrackListener(this);
                        }
                    }
                }
            }
        }
    }

    /**
     * Removes all {@link IMouseListener}s from an {@link IComponent}, if this {@link IComponent} is
     * a {@link Component}
     *
     * @param comp The concerned {@link IComponent}
     */
    private void unregisterAllMouseListenersFromComponent(IComponent comp) {
        if ((comp != null) && (comp instanceof CometeComposite)) {
            CometeComposite<?> cometeComposite = (CometeComposite<?>) comp;
            Control component = cometeComposite.getControl();
            if (component == null) {
                component = cometeComposite;
            }
            synchronized (listenerMap) {
                component.removeMouseListener(this);
                component.removeMouseWheelListener(this);
                component.removeMouseMoveListener(this);
                component.removeMouseTrackListener(this);
                List<WeakReference<IMouseListener>> listeners = listenerMap.get(comp);
                if (listeners != null) {
                    synchronized (listeners) {
                        listeners.clear();
                    }
                }
                listenerMap.remove(comp);
            }
        }
    }

    /**
     * Warns all mouse listeners of an {@link IComponent} about a mouse event
     *
     * @param comp The concerned {@link IComponent}
     * @param event The mouse event
     */
    protected void warnMouseListeners(IComponent comp, CometeMouseEvent event) {
        if ((comp != null) && (event != null)) {
            List<WeakReference<IMouseListener>> listeners;
            synchronized (listenerMap) {
                listeners = listenerMap.get(comp);
            }
            if (listeners != null) {
                synchronized (listeners) {
                    List<WeakReference<IMouseListener>> toRemove = new ArrayList<WeakReference<IMouseListener>>();
                    for (WeakReference<IMouseListener> ref : listeners) {
                        IMouseListener listener = ref.get();
                        if (listener == null) {
                            toRemove.add(ref);
                        }
                        else if (!event.isCanceled()) {
                            listener.mouseChanged(event);
                        }
                    }
                    listeners.removeAll(toRemove);
                    toRemove.clear();
                }
            }
        }
    }

    @Override
    public void mouseScrolled(MouseEvent e) {
        doWarn(e, MouseReason.WHEEL, 1);
    }

    @Override
    public void mouseDoubleClick(MouseEvent e) {
        doWarn(e, MouseReason.CLICKED, 0);
    }

    @Override
    public void mouseDown(MouseEvent e) {
        doWarn(e, MouseReason.PRESSED, 0);
    }

    @Override
    public void mouseUp(MouseEvent e) {
        doWarn(e, MouseReason.RELEASED, 0);
    }

    @Override
    public void mouseEnter(MouseEvent e) {
        doWarn(e, MouseReason.ENTERED, 0);
    }

    @Override
    public void mouseExit(MouseEvent e) {
        doWarn(e, MouseReason.EXITED, 0);
    }

    @Override
    public void mouseHover(MouseEvent e) {
        // TODO not yet implemented
        // extracted(e, MouseReason.HOVER, 0);
    }

    @Override
    public void mouseMove(MouseEvent e) {
        doWarn(e, MouseReason.MOVED, 0);
    }

    private void doWarn(MouseEvent e, MouseReason reason, int scroll) {
        if (e != null) {
            if ((e.getSource() != null) && (e.getSource() instanceof Control)) {
                Control source = (Control) e.getSource();
                if (source.getParent() instanceof IComponent) {
                    IComponent comp = (IComponent) source.getParent();
                    warnMouseListeners(comp, new CometeMouseEvent(comp, reason, e.x, e.y, e.button - 1, e.count,
                            scroll, e.stateMask, e.time, (e.stateMask & SWT.ALT) != 0,
                            (e.stateMask & (SWT.CTRL | SWT.ALT)) != 0, (e.stateMask & SWT.CTRL) != 0,
                            (e.stateMask & SWT.SHIFT) != 0, false));
                }
            }
        }
    }

}