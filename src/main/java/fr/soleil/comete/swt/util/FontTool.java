//+============================================================================
//Source: package fr.soleil.comete.widget.swing;/AwtCometeColor.java
//
//project :     Comete
//
//Description: This class hides
//
//Author: SAINTIN
//
//Revision: 1.1
//
//Log:
//
//copyleft :Synchrotron SOLEIL
//			L'Orme des Merisiers
//			Saint-Aubin - BP 48
//			91192 GIF-sur-YVETTE CEDEX
//			FRANCE
//
//+============================================================================
package fr.soleil.comete.swt.util;

import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.widgets.Display;

import fr.soleil.comete.definition.widget.util.CometeFont;

/**
 * Common Font utilities
 *
 * @author saintin
 */
public class FontTool {

    public static Font getFont(CometeFont cometeFont) {
        Font swtFont = null;
        if (cometeFont != null) {
            swtFont = getFont(cometeFont.getName(), cometeFont.getStyle(), cometeFont.getSize());
        }
        return swtFont;
    }

    public static Font getFont(String name, int style, int size) {
        return new Font(Display.getCurrent(), name, size, style);
    }

    public static CometeFont getCometeFont(Font srcFont) {
        CometeFont cometeFont = null;
        if (srcFont != null) {
            FontData[] fontDataArray = srcFont.getFontData();
            if (fontDataArray != null && fontDataArray.length > 0) {
                FontData fontData = fontDataArray[0];
                cometeFont = new CometeFont(fontData.getName(), fontData.getStyle(), fontData.getHeight());
            }
        }
        return cometeFont;
    }

}
