package fr.soleil.comete.swt.util;

import org.eclipse.swt.graphics.Image;

public class SwtImage {

    private Image image = null;
    private String fileName = null;

    public SwtImage(String fileName) {
        this.image = ImageTool.getImage(fileName);
        this.fileName = fileName;
    }

    public Image getImage() {
        return image;
    }

    public String getImagePath() {
        return fileName;
    }

}
