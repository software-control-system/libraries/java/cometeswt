package fr.soleil.comete.swt;

import java.util.EventObject;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

import fr.soleil.comete.definition.listener.IProgressBarListener;
import fr.soleil.comete.definition.listener.ISliderListener;
import fr.soleil.comete.definition.widget.IProgressBar;

public class ProgressBar extends AbstractNumberRangeControl<org.eclipse.swt.widgets.ProgressBar> implements
IProgressBar {

    public ProgressBar(Composite parent, int childStyle) {
        super(parent, childStyle);
    }

    @Override
    protected org.eclipse.swt.widgets.ProgressBar initControl(int childStyle) {
        org.eclipse.swt.widgets.ProgressBar progressBar = new org.eclipse.swt.widgets.ProgressBar(this, childStyle);
        return progressBar;
    }

    @Override
    protected Number getControlValue() {
        Number value = null;
        if (getControl() != null) {
            value = getControl().getSelection();
        }
        return value;
    }

    @Override
    protected void setControlValue(Number value) {
        if ((getControl() != null) && (value != null)) {
            getControl().setSelection(value.intValue());
        }
    }

    @Override
    protected Number getControlMinimum() {
        Number min = null;
        if (getControl() != null) {
            min = getControl().getMinimum();
        }
        return min;
    }

    @Override
    protected void setControlMinimum(Number min) {
        if ((min != null) && (getControl() != null)) {
            getControl().setMinimum(min.intValue());
        }
    }

    @Override
    protected Number getControlMaximum() {
        Number max = null;
        if (getControl() != null) {
            getControl().getMaximum();
        }
        return max;
    }

    @Override
    protected void setControlMaximum(Number max) {
        if ((max != null) && (getControl() != null)) {
            getControl().setMaximum(max.intValue());
        }
    }

    @Override
    public void addProgressBarListener(IProgressBarListener listener) {
        addNumberListener(listener);
    }

    @Override
    public void removeProgressBarListener(IProgressBarListener listener) {
        removeNumberListener(listener);
    }

    public static void main(String[] args) {
        final Display display = new Display();
        final Shell shell = new Shell(display);

        shell.setBounds(0, 0, 400, 200);
        shell.setLayout(new FillLayout());
        shell.setText("ProgressBar");

        final ProgressBar bar = new ProgressBar(shell, SWT.NONE);
        bar.setMaximum(1000);
        bar.setToolTipText("Tooltype test");

        bar.addProgressBarListener(new IProgressBarListener() {
            @Override
            public void valueChanged(EventObject event) {
                System.out.println("selected=" + bar.getValue());
            }
        });

        final Slider slider = new Slider(shell, SWT.NONE);
        slider.setMaximum(1000);

        slider.addSliderListener(new ISliderListener() {
            @Override
            public void valueChanged(EventObject event) {
                bar.setValue(slider.getValue());
            }
        });

        // shell.pack();
        shell.open();

        while (!shell.isDisposed()) {
            if (!display.readAndDispatch()) {
                display.sleep();
            }
        }

        display.dispose();
        System.exit(0);
    }


}
