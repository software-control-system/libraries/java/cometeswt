package fr.soleil.comete.swt;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.EventObject;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import fr.soleil.comete.definition.data.information.TextInformation;
import fr.soleil.comete.definition.listener.ITextAreaListener;
import fr.soleil.comete.definition.widget.IComponent;
import fr.soleil.comete.definition.widget.ITextArea;
import fr.soleil.comete.swt.util.SWTUtils;
import fr.soleil.data.target.IUnitTarget;

public class TextArea extends CometeComposite<Text> implements ITextArea, IUnitTarget, KeyListener {

    private String unit;
    private final List<WeakReference<ITextAreaListener>> textAreaListeners = new ArrayList<WeakReference<ITextAreaListener>>();

    public TextArea(Composite parent, int childStyle) {
        super(parent, childStyle);
    }

    @Override
    protected Text initControl(int childStyle) {
        Text text = new Text(this, SWT.MULTI | SWT.LEFT | childStyle);
        text.addKeyListener(this);
        return text;
    }

    @Override
    public boolean isEditable() {
        final boolean[] editable = { false };
        if (getControl() != null) {
            SWTUtils.syncExec(new Runnable() {
                @Override
                public void run() {
                    if (!getControl().isDisposed()) {
                        editable[0] = getControl().getEditable();
                    }
                }
            });
        }
        return editable[0];
    }

    @Override
    public void setEditable(final boolean editable) {
        if (getControl() != null) {
            SWTUtils.asyncExec(new Runnable() {
                @Override
                public void run() {
                    if (!getControl().isDisposed()) {
                        getControl().setEditable(editable);
                    }
                }
            });
        }
    }

    @Override
    public boolean isEditingData() {
        final boolean[] editing = { false };
        if (getControl() != null) {
            SWTUtils.syncExec(new Runnable() {
                @Override
                public void run() {
                    if (!getControl().isDisposed()) {
                        editing[0] = getControl().getEditable() && getControl().isFocusControl();
                    }
                }
            });
        }
        return editing[0];
    }

    @Override
    public String getText() {
        final String[] result = { null };
        if (getControl() != null) {
            SWTUtils.syncExec(new Runnable() {
                @Override
                public void run() {
                    if (!getControl().isDisposed()) {
                        result[0] = getControl().getText();
                    }
                }
            });
            if (unit != null) {
                result[0] = result[0] + " " + unit;
            }
        }
        return result[0];
    }

    @Override
    public void setText(final String text) {
        if (getControl() != null) {
            final String theText = (text == null ? "" : text);
            SWTUtils.asyncExec(new Runnable() {
                @Override
                public void run() {
                    if (!getControl().isDisposed()) {
                        getControl().setText(theText);

                        EventObject event = new EventObject(TextArea.this);
                        fireTextChanged(event);
                    }
                }
            });
        }
    }

    @Override
    public int getHorizontalAlignment() {
        return IComponent.LEFT;
    }

    @Override
    public void setHorizontalAlignment(int hAlign) {
        // nothing to do
    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (e.character == SWT.CR) {
            fireActionPerformed(new EventObject(this));
        }
        else if (e.character != '\0') {
            fireTextChanged(new EventObject(this));
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
    }

    @Override
    public String getUnit() {
        return this.unit;
    }

    @Override
    public void setUnit(String unit) {
        this.unit = unit;
    }

    @Override
    public void addTextAreaListener(final ITextAreaListener listener) {
        // add listener and clean useless references
        if (listener != null) {
            ArrayList<WeakReference<ITextAreaListener>> toRemove = new ArrayList<WeakReference<ITextAreaListener>>();
            boolean canAdd = true;
            synchronized (textAreaListeners) {
                for (WeakReference<ITextAreaListener> ref : textAreaListeners) {
                    ITextAreaListener temp = ref.get();
                    if (temp == null) {
                        toRemove.add(ref);
                    }
                    else if (temp.equals(listener)) {
                        canAdd = false;
                    }
                    temp = null;
                }
                textAreaListeners.removeAll(toRemove);
                if (canAdd) {
                    textAreaListeners.add(new WeakReference<ITextAreaListener>(listener));
                }
            }
            toRemove.clear();
        }
    }

    @Override
    public void fireActionPerformed(EventObject event) {
        // TODO here getText does not contain the last character (call is on keyPressed, not keyRelease...)
        warnMediators(new TextInformation(this, getText()));
        // fire event and clean useless references
        ArrayList<WeakReference<ITextAreaListener>> toRemove = new ArrayList<WeakReference<ITextAreaListener>>();
        synchronized (textAreaListeners) {
            for (WeakReference<ITextAreaListener> ref : textAreaListeners) {
                ITextAreaListener temp = ref.get();
                if (temp == null) {
                    toRemove.add(ref);
                }
                else {
                    temp.textChanged(event);
                    temp.actionPerformed(event);
                }
            }
            textAreaListeners.removeAll(toRemove);
        }
        toRemove.clear();
    }

    @Override
    public void fireTextChanged(EventObject event) {
        // fire event and clean useless references
        ArrayList<WeakReference<ITextAreaListener>> toRemove = new ArrayList<WeakReference<ITextAreaListener>>();
        synchronized (textAreaListeners) {
            for (WeakReference<ITextAreaListener> ref : textAreaListeners) {
                ITextAreaListener temp = ref.get();
                if (temp == null) {
                    toRemove.add(ref);
                }
                else {
                    temp.textChanged(event);
                }
            }
            textAreaListeners.removeAll(toRemove);
        }
        toRemove.clear();
    }

    @Override
    public void removeTextAreaListener(ITextAreaListener listener) {
        // remove listener and clean useless references
        if (listener != null) {
            ArrayList<WeakReference<ITextAreaListener>> toRemove = new ArrayList<WeakReference<ITextAreaListener>>();
            synchronized (textAreaListeners) {
                for (WeakReference<ITextAreaListener> ref : textAreaListeners) {
                    ITextAreaListener temp = ref.get();
                    if ((temp == null) || temp.equals(listener)) {
                        toRemove.add(ref);
                    }
                }
                textAreaListeners.removeAll(toRemove);
            }
            toRemove.clear();
        }
    }

    @Override
    public void setColumns(final int columns) {
        if (getControl() != null) {
            SWTUtils.asyncExec(new Runnable() {
                @Override
                public void run() {
                    if (!getControl().isDisposed()) {
                        getControl().setTextLimit(columns);
                    }
                }
            });
        }
    }

    @Override
    public void setRows(int rows) {
        // Not to be implemented
    }

    public static void main(String[] args) {
        final Display display = new Display();
        final Shell shell = new Shell(display);

        shell.setBounds(0, 0, 400, 200);
        shell.setLayout(new FillLayout());
        shell.setText("TextArea");

        TextArea textarea = new TextArea(shell, SWT.NONE);
        textarea.setText("TextArea SWT Test\nTextArea SWT Test\nTextArea SWT Test");
        textarea.setToolTipText("Tooltype test");

        // shell.pack();
        shell.open();

        while (!shell.isDisposed()) {
            if (!display.readAndDispatch()) {
                display.sleep();
            }
        }

        display.dispose();
        System.exit(0);
    }

}
