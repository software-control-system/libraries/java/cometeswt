package fr.soleil.comete.swt;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;

import fr.soleil.comete.definition.listener.IMouseListener;
import fr.soleil.comete.definition.widget.IComponent;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.comete.definition.widget.util.CometeFont;
import fr.soleil.comete.swt.util.ColorTool;
import fr.soleil.comete.swt.util.ComponentMouseDelegate;
import fr.soleil.comete.swt.util.FontTool;
import fr.soleil.comete.swt.util.SWTUtils;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.target.TargetDelegate;
import fr.soleil.data.target.information.TargetInformation;

public abstract class CometeComposite<T extends Control> extends Composite implements IComponent {

    private final TargetDelegate delegate = new TargetDelegate();
    protected T internalControl = null;

    protected Font currentFont = null;
    protected Color background = null;
    protected Color foreground = null;

    public CometeComposite(Composite parent, int childStyle) {
        this(parent, SWT.NONE, childStyle);
    }

    protected CometeComposite(Composite parent, int style, int childStyle) {
        super(parent, style);

        FillLayout layout = new FillLayout();
        layout.marginHeight = 0;
        layout.marginWidth = 0;
        layout.spacing = 0;
        setLayout(layout);

        internalControl = initControl(childStyle);
    }

    protected abstract T initControl(int childStyle);

    public T getControl() {
        return internalControl;
    }

    @Override
    public void addMediator(Mediator<?> mediator) {
        delegate.addMediator(mediator);
    }

    @Override
    public void removeMediator(Mediator<?> mediator) {
        delegate.removeMediator(mediator);
    }

    /**
     * Transmits a {@link TargetInformation} to the linked {@link Mediator}s
     * 
     * @param <I> The type of Data the {@link TargetInformation} is able to manipulate
     * @param <V> The type of {@link TargetInformation} to transmit
     * @param targetInformation The {@link TargetInformation} that should be transmitted to
     *            {@link Mediator}s
     */
    protected <I, V extends TargetInformation<I>> void warnMediators(V targetInformation) {
        delegate.warnMediators(targetInformation);
    }

    @Override
    public boolean hasFocus() {
        // not to be implemented
        return false;
    }

    @Override
    public void setOpaque(boolean opaque) {
        // not to be implemented
    }

    @Override
    public boolean isOpaque() {
        // not to be implemented
        return true;
    }

    @Override
    public void setCometeBackground(final CometeColor color) {
        SWTUtils.asyncExec(new Runnable() {
            @Override
            public void run() {
                if ((background != null) && !background.isDisposed()) {
                    background.dispose();
                }

                background = ColorTool.getColor(color);
                setSuperBackground(background);
                if ((internalControl != null) && !internalControl.isDisposed() && !internalControl.equals(this)) {
                    internalControl.setBackground(background);
                }
            }
        });
    }

    private void setSuperBackground(final Color background) {
        super.setBackground(background);
    }

    @Override
    public CometeColor getCometeBackground() {
        Color swtColor = super.getBackground();
        if (internalControl != null) {
            swtColor = internalControl.getBackground();
        }
        return ColorTool.getCometeColor(swtColor);
    }

    @Override
    public void setCometeForeground(final CometeColor color) {
        SWTUtils.asyncExec(new Runnable() {
            @Override
            public void run() {
                if ((foreground != null) && !foreground.isDisposed()) {
                    foreground.dispose();
                }
                foreground = ColorTool.getColor(color);
                setSuperForeground(foreground);

                if ((internalControl != null) && !internalControl.isDisposed() && !internalControl.equals(this)) {
                    internalControl.setBackground(foreground);
                }
            }
        });
    }

    private void setSuperForeground(final Color foreground) {
        super.setForeground(background);
    }


    @Override
    public CometeColor getCometeForeground() {
        Color swtColor = super.getForeground();
        if (internalControl != null) {
            swtColor = internalControl.getForeground();
        }
        return ColorTool.getCometeColor(swtColor);
    }

    @Override
    public void setEnabled(final boolean enabled) {
        SWTUtils.asyncExec(new Runnable() {
            @Override
            public void run() {
                setSuperEnabled(enabled);
                if ((internalControl != null) && !internalControl.isDisposed() && !internalControl.equals(this)) {
                    if (!internalControl.equals(this)) {
                        internalControl.setEnabled(enabled);
                    }
                }
            }
        });
    }

    private void setSuperEnabled(boolean enabled) {
        if (!isDisposed()) {
            super.setEnabled(enabled);
        }
    }

    @Override
    public boolean isEnabled() {
        boolean result = super.getEnabled();
        if (internalControl != null) {
            result &= internalControl.getEnabled();
        }
        return result;
    }

    @Override
    public void setCometeFont(final CometeFont font) {
        SWTUtils.asyncExec(new Runnable() {
            @Override
            public void run() {
                if ((currentFont != null) && !currentFont.isDisposed()) {
                    currentFont.dispose();
                }

                currentFont = FontTool.getFont(font);
                setSuperFont(currentFont);
                if ((internalControl != null) && !internalControl.isDisposed() && !internalControl.equals(this)) {
                    internalControl.setFont(currentFont);
                }
            }
        });
    }

    private void setSuperFont(final Font font) {
        super.setFont(currentFont);
    }

    @Override
    public CometeFont getCometeFont() {
        Font swtFont = super.getFont();
        if (internalControl != null) {
            swtFont = internalControl.getFont();
        }
        return FontTool.getCometeFont(swtFont);
    }

    @Override
    public void setToolTipText(final String string) {
        SWTUtils.asyncExec(new Runnable() {
            @Override
            public void run() {
                superSetToolTipText(string);
                if ((internalControl != null) && !internalControl.isDisposed() && !internalControl.equals(this)) {
                    internalControl.setToolTipText(string);
                }
            }
        });

    }

    private void superSetToolTipText(String string) {
        super.setToolTipText(string);
    }

    @Override
    public String getToolTipText() {
        String result = null;
        if (internalControl != null) {
            result = internalControl.getToolTipText();
        }
        return result;
    }

    @Override
    public void setVisible(final boolean visible) {
        SWTUtils.asyncExec(new Runnable() {
            @Override
            public void run() {
                setSuperVisible(visible);
                if ((internalControl != null) && !internalControl.isDisposed() && !internalControl.equals(this)) {
                    internalControl.setVisible(visible);
                }
            }
        });
    }

    private void setSuperVisible(boolean visible) {
        super.setVisible(visible);
    }

    @Override
    public boolean isVisible() {
        boolean visible = getVisible();
        if (internalControl != null) {
            visible = internalControl.getVisible();
        }
        return visible;
    }

    @Override
    public void setSize(final int width, final int height) {
        SWTUtils.asyncExec(new Runnable() {
            @Override
            public void run() {
                setSuperSize(width, height);
            }
        });
    }

    private void setSuperSize(int width, int height) {
        super.setSize(width, height);
    }

    @Override
    public void setPreferredSize(int width, int height) {
        // not to be implemented
        setSize(width, height);
    }

    @Override
    public int getWidth() {
        int width = 0;
        if (!isDisposed()) {
            Rectangle bounds = this.getBounds();
            width = bounds.width;
        }
        return width;
    }

    @Override
    public int getHeight() {
        int height = 0;
        if (!isDisposed()) {
            Rectangle bounds = this.getBounds();
            height = bounds.height;
        }
        return height;
    }

    @Override
    public void setLocation(final int x, final int y) {
        SWTUtils.asyncExec(new Runnable() {
            @Override
            public void run() {
                setSuperLocation(x, y);
            }
        });
    }

    private void setSuperLocation(int x, int y) {
        super.setLocation(x, y);
    }

    @Override
    public int getX() {
        int x = 0;
        if (!isDisposed()) {
            Point location = this.getLocation();
            x = location.x;
        }
        return x;
    }

    @Override
    public int getY() {
        int y = 0;
        if (!isDisposed()) {
            Point location = this.getLocation();
            y = location.y;
        }
        return y;
    }

    @Override
    public boolean isEditingData() {
        return false;
    }

    @Override
    public void setTitledBorder(String title) {
        // not to be implemented
    }

    @Override
    public void addMouseListener(IMouseListener listener) {
        ComponentMouseDelegate.addMouseListenerToComponent(this, listener);
    }

    @Override
    public void removeMouseListener(IMouseListener listener) {
        ComponentMouseDelegate.removeMouseListenerFromComponent(this, listener);
    }

    @Override
    public void removeAllMouseListeners() {
        ComponentMouseDelegate.removeAllMouseListenersFromComponent(this);
    }

}
