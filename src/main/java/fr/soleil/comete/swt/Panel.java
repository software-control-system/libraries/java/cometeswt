package fr.soleil.comete.swt;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Layout;

import fr.soleil.comete.definition.widget.IComponent;
import fr.soleil.comete.definition.widget.IPanel;

public class Panel extends CometeComposite<Composite> implements IPanel {

    public Panel(Composite parent, int childStyle) {
        super(parent, childStyle);
    }

    @Override
    protected Composite initControl(int childStyle) {
        return this;
    }

    @Override
    public void setHorizontalAlignment(int halign) {
        // Nothing to do

    }

    @Override
    public int getHorizontalAlignment() {
        return 0;
    }

    @Override
    public void add(IComponent component) {
        // Nothing to do in SWT composite constructor always need parent
    }

    @Override
    public void addCenter(IComponent component) {
        // Nothing to do in SWT composite constructor always need parent
    }

    @Override
    public void add(IComponent component, Object constraints) {
        // Nothing to do in SWT composite constructor always need parent
    }

    @Override
    public void setLayout(Object layout) {
        if (layout instanceof Layout) {
            super.setLayout((Layout) layout);
        }
    }

}
